﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventHandlerBase.cs" company="">
//   
// </copyright>
// <summary>
//   The event handler base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DenormalizerServer.EventHandler
{
    using Cloud.Infrastructure.Logging.Contracts;
    using Cloud.Infrastructure.Repository.Contracts;
    using Cloud.Repository;
    using Cloud.Repository.Models;

    using Microsoft.Practices.Unity;

    /// <summary>
    /// The event handler base.
    /// </summary>
    public class EventHandlerBase
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        [Dependency]
        public ILogger Logger { get; set; }

        /// <summary>
        /// Gets or sets the repository.
        /// </summary>
        [Dependency]
        public IBankingRepository Repository { get; set; }

        #endregion
    }
}
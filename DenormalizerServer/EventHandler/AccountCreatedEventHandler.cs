﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountCreatedEventHandler.cs" company="">
//   
// </copyright>
// <summary>
//   The account created event handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DenormalizerServer.EventHandler
{
    using Banking.Dtos;

    using Cloud.Events;

    using NServiceBus;

    /// <summary>
    /// The account created event handler.
    /// </summary>
    public class AccountCreatedEventHandler : EventHandlerBase, IHandleMessages<IAccountCreatedEvent>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The handle.
        /// </summary>
        /// <param name="event">
        /// The event.
        /// </param>
        public void Handle(IAccountCreatedEvent @event)
        {
            this.Logger.Debug(() => "############Inside AccountCreatedEventHandler################");
            var account = new Account
                              {
                                  AccountId = @event.AccountId, 
                                  AccountName = @event.AccountName, 
                                  AccountNumber = @event.AccountNumber, 
                                  Balance = @event.Balance, 
                                  IsActive = @event.IsActive
                              };

            this.Repository.Insert(account);
            this.Repository.SaveChanges();
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="">
//   
// </copyright>
// <summary>
//   The web api application.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DenormalizerServer
{
    using System.Configuration;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;

    using Cloud.Infrastructure.Host;
    using Cloud.Infrastructure.Host.Bus;
    using Cloud.Infrastructure.Host.Contracts;

    /// <summary>
    /// The web api application.
    /// </summary>
    public class WebApiApplication : HttpApplication
    {
        #region Methods

        /// <summary>
        /// The application_ start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            IBootstrapperConfig bootstrapper =
                new BootstrapperBus().Initialize()
                    .EnableLogging()
                    .EnableGlobalExceptionHandler()
                    .EnableJsonTypeNameHandling()
                    .SetSerializePrivateMembers()
                    .ConfigureNsbBus(ConfigurationManager.AppSettings["endpointname"], true)
                    .StartNsbBus(doInstall: true);
        }

        #endregion
    }
}
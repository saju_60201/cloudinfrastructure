﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValuesController.cs" company="">
//   
// </copyright>
// <summary>
//   The values controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DenormalizerServer.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using Banking.Dtos;

    using Cloud.Infrastructure.Logging.Contracts;
    using Cloud.Infrastructure.Repository.Contracts;
    using Cloud.Repository;

    /// <summary>
    /// The values controller.
    /// </summary>
    public class ValuesController : ApiController
    {
        // GET api/values
        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void Delete(int id)
        {
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<string> Get(IBankingRepository repository)
        {
            List<Account> account = repository.Get<Account>().ToList();
            return new[] { "value1", "value2" };
        }

        // GET api/values/5
        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        /// <summary>
        /// The put.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void Put(int id, [FromBody] string value)
        {
        }

        #endregion

        // DELETE api/values/5
    }
}
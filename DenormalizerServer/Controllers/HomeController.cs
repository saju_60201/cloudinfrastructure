﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.UI.WebControls;

namespace DenormalizerServer.Controllers
{
    public class HomeController : ApiController
    {
        [System.Web.Http.HttpGet]
        public string Get()
        {
            return "Denormalizer Initiated";
        }
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommandEventCommon.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The ComandEventCommon interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    ///     The ComandEventCommon interface.
    /// </summary>
    public interface ICommandEventCommon : ITrackingId, IClientUtcTime
    {
    }
}
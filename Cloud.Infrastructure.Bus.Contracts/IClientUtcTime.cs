﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClientUtcTime.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    ///     The ClientUtcTime interface.
    /// </summary>
    public interface IClientUtcTime
    {
        /// <summary>
        ///     Gets or sets the client utc time.
        /// </summary>
        DateTime ClientUtcTime { get; set; }
    }
}
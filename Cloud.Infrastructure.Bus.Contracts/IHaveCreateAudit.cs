﻿//--------------------------------------------------------------------------------
// <copyright file="IHaveCreateAudit.cs" company="Ruf Informatik AG"> 
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
//--------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    ///     When we need audit for creation of object.
    /// </summary>
    public interface IHaveCreateAudit
    {
        /// <summary>
        ///     Gets or sets the created by user.
        /// </summary>
        /// <value>
        ///     The created by user.
        /// </value>
        Guid CreatedByUser { get; set; }

        /// <summary>
        ///     Gets or sets the created date.
        /// </summary>
        /// <value>
        ///     The created date.
        /// </value>
        DateTime CreatedDate { get; set; }
    }
}
﻿//--------------------------------------------------------------------------------
// <copyright file="IHaveAudit.cs" company="Ruf Informatik AG"> 
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
//--------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    /// When we need audit for the object.
    /// </summary>
    public interface IHaveAudit : IHaveCreateAudit, IHaveModifyAudit
    {
    }
}
﻿//--------------------------------------------------------------------------------
// <copyright file="IHaveModifyAudit.cs" company="Ruf Informatik AG"> 
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
//--------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    ///     When we need audit for last modification of object.
    /// </summary>
    public interface IHaveModifyAudit
    {
        /// <summary>
        ///     Gets or sets the last modified by user.
        /// </summary>
        /// <value>
        ///     The last modified by user.
        /// </value>
        Guid LastModifiedByUser { get; set; }

        /// <summary>
        ///     Gets or sets the last modified date.
        /// </summary>
        /// <value>
        ///     The last modified date.
        /// </value>
        DateTime LastModifiedDate { get; set; }
    }
}
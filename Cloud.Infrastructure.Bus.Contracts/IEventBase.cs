﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventBase.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The event base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NServiceBus;

namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    ///     The event base.
    /// </summary>
    public interface IEventBase : IEvent, ICommandEventCommon
    {
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBusLicenseProvider.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The BusLicenseProvider interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    ///     The BusLicenseProvider interface.
    /// </summary>
    public interface IBusLicenseProvider
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get license.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        string GetLicense();

        #endregion
    }
}
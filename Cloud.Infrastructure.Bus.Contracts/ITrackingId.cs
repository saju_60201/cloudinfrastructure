﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITrackingId.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The TrackingId interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Bus.Contracts
{
    /// <summary>
    ///     The TrackingId interface.
    /// </summary>
    public interface ITrackingId
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the tracking id.
        /// </summary>
        Guid TrackingId { get; set; }

        #endregion
    }
}
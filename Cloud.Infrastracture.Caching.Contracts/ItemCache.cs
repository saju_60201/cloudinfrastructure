﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemCache.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    /// <summary>
    ///     The item cache.
    /// </summary>
    public class ItemCache
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemCache"/> class.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        public ItemCache(string key, object value, string regionName = null)
        {
            this.Key = key;
            this.Value = value;
            this.RegionName = regionName;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the key.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        ///     Gets the region name.
        /// </summary>
        public string RegionName { get; private set; }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        public object Value { get; private set; }

        #endregion
    }
}
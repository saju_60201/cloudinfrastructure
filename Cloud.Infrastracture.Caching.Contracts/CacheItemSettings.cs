﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheItemSettings.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    using System;

    /// <summary>
    ///     The cache item settings.
    /// </summary>
    public class CacheItemSettings
    {
        #region Fields

        /// <summary>
        /// The abs expiry.
        /// </summary>
        private DateTimeOffset absExpiry;

        /// <summary>
        /// The sld expiry.
        /// </summary>
        private TimeSpan sldExpiry;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="CacheItemSettings" /> class.
        /// </summary>
        public CacheItemSettings()
        {
            this.absExpiry = DateTimeOffset.MaxValue;
            this.sldExpiry = TimeSpan.Zero;
        }

        #endregion

        #region Delegates

        /// <summary>
        ///     The cache entry item removed call back.
        /// </summary>
        /// <param name="arguments">
        ///     The arguments.
        /// </param>
        public delegate void CacheEntryItemRemovedCallBack(CacheEntryItemRemovedEventArgs arguments);

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the absolute expiration.
        /// </summary>
        public DateTimeOffset AbsoluteExpiration
        {
            get
            {
                return this.absExpiry;
            }

            set
            {
                this.absExpiry = value;
            }
        }

        /// <summary>
        ///     Gets or sets the removed callback.
        /// </summary>
        public CacheEntryItemRemovedCallBack RemovedCallback { get; set; }

        /// <summary>
        ///     Gets or sets the sliding expiration.
        /// </summary>
        public TimeSpan SlidingExpiration
        {
            get
            {
                return this.sldExpiry;
            }

            set
            {
                this.sldExpiry = value;
            }
        }

        #endregion
    }
}
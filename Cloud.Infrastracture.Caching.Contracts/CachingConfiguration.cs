﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CachingConfiguration.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Caching.Contracts
{
    using Cloud.Infrastructure.Configuration;

    /// <summary>
    ///     The caching configuration.
    /// </summary>
    public class CachingConfiguration : ConfigurationBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CachingConfiguration"/> class.
        /// </summary>
        /// <param name="configurationProvider">
        /// The configuration provider.
        /// </param>
        public CachingConfiguration(ConfigurationProvider configurationProvider)
            : base(configurationProvider)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether is caching enabled.
        /// </summary>
        public bool IsCachingEnabled
        {
            get
            {
                return this.Settings.Get(() => this.IsCachingEnabled);
            }

            set
            {
                this.Settings.Set(() => this.IsCachingEnabled, value);
            }
        }

        #endregion
    }
}
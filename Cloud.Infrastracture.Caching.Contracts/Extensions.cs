﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    using System;

    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="cache">
        /// The cache.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <typeparam name="T">
        /// Type of the item to add
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Add<T>(this ICache cache, string key, T value, string regionName = null)
        {
            return cache.Add(key, value, regionName);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="cache">
        /// The cache.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <typeparam name="T">
        /// Type of the Item to Get
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T Get<T>(this ICache cache, string key, string regionName = null)
        {
            var item = cache.Get(key, regionName);
            if (item != null)
            {
                return (T)item;
            }

            return default(T);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="cache">
        /// The cache.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <typeparam name="T">
        /// Type of the item to set
        /// </typeparam>
        public static void Set<T>(this ICache cache, string key, T value, CacheItemSettings setting, string regionName = null)
        {
            cache.Set(key, value, setting, regionName);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="cache">
        /// The cache.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absoluteExpiration">
        /// The absolute expiration.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <typeparam name="T">
        /// Type of the item to set
        /// </typeparam>
        public static void Set<T>(this ICache cache, string key, T value, DateTimeOffset absoluteExpiration, string regionName = null)
        {
            cache.Set(key, value, absoluteExpiration, regionName);
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICache.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     The Cache interface.
    /// </summary>
    public interface ICache
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Add(string key, object value, string regionName = null);

        /// <summary>
        /// The add or get existing.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absoluteExpiration">
        /// The absolute expiration.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object AddOrGetExisting(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null);

        /// <summary>
        /// The add or get existing.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object AddOrGetExisting(string key, object value, CacheItemSettings setting, string regionName = null);

        /// <summary>
        /// The contains.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Contains(string key, string regionName = null);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Get(string key, string regionName = null);

        /// <summary>
        /// The get count.
        /// </summary>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        long GetCount(string regionName = null);

        /// <summary>
        /// The get values.
        /// </summary>
        /// <param name="keys">
        /// The keys.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="IDictionary"/>.
        /// </returns>
        IDictionary<string, object> GetValues(IEnumerable<string> keys, string regionName = null);

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Remove(string key, string regionName = null);

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absoluteExpiration">
        /// The absolute expiration.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        void Set(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null);

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        void Set(string key, object value, CacheItemSettings setting, string regionName = null);

        #endregion
    }
}
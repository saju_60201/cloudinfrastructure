﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICachingFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    using System;

    /// <summary>
    ///     The CachingFactory interface.
    /// </summary>
    public interface ICachingFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// The create cache.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="isGetExisting">
        /// The is get existing.
        /// </param>
        /// <returns>
        /// The <see cref="ICache"/>.
        /// </returns>
        ICache CreateCache(string name, bool isGetExisting = true);

        /// <summary>
        /// The create cache.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="maxItemOffset">
        /// The max item offset.
        /// </param>
        /// <param name="isGetExisting">
        /// The is get existing.
        /// </param>
        /// <returns>
        /// The <see cref="ICache"/>.
        /// </returns>
        ICache CreateCache(string name, DateTimeOffset maxItemOffset, bool isGetExisting = true);

        #endregion
    }
}
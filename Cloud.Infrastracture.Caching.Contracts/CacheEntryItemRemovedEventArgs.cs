﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheEntryItemRemovedEventArgs.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    using System;

    /// <summary>
    ///     The cache entry item removed event args.
    /// </summary>
    public class CacheEntryItemRemovedEventArgs : EventArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheEntryItemRemovedEventArgs"/> class.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="itemCache">
        /// The item cache.
        /// </param>
        /// <param name="reason">
        /// CacheItemRemovedReason 
        /// </param>
        public CacheEntryItemRemovedEventArgs(object source, ItemCache itemCache, CustomCacheEntryRemovedReason reason)
        {
            this.Source = source;
            this.ItemCache = itemCache;
            this.RemovedReason = reason;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the item cache.
        /// </summary>
        public ItemCache ItemCache { get; private set; }

        /// <summary>
        ///     Gets the reason.
        /// </summary>
        public CustomCacheEntryRemovedReason RemovedReason { get; private set; }

        /// <summary>
        ///     Gets the source.
        /// </summary>
        public object Source { get; private set; }

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheEntryRemovedReasonExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    using System.Runtime.Caching;

    /// <summary>
    ///     The cache item removed reason extensions.
    /// </summary>
    public static class CacheEntryRemovedReasonExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Converts to CacheItemRemovedReason. 
        /// </summary>
        /// <param name="value">
        /// The value. 
        /// </param>
        /// <returns>
        /// The <see cref="CacheItemRemovedReason"/>. 
        /// </returns>
        public static CacheEntryRemovedReason ToCacheEntryRemovedReason(this CustomCacheEntryRemovedReason value)
        {
            switch (value)
            {
                case CustomCacheEntryRemovedReason.Removed:
                    return CacheEntryRemovedReason.Removed;
                case CustomCacheEntryRemovedReason.Expired:
                    return CacheEntryRemovedReason.Expired;
                case CustomCacheEntryRemovedReason.Evicted:
                    return CacheEntryRemovedReason.Evicted;
                case CustomCacheEntryRemovedReason.ChangeMonitorChanged:
                    return CacheEntryRemovedReason.ChangeMonitorChanged;
                case CustomCacheEntryRemovedReason.CacheSpecificEviction:
                    return CacheEntryRemovedReason.CacheSpecificEviction;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Converts to customCacheItemRemovedReason. 
        /// </summary>
        /// <param name="value">
        /// The value. 
        /// </param>
        /// <returns>
        /// The <see cref="CustomCacheEntryRemovedReason"/>. 
        /// </returns>
        public static CustomCacheEntryRemovedReason ToCustomCacheEntryRemovedReason(this CacheEntryRemovedReason value)
        {
            switch (value)
            {
                case CacheEntryRemovedReason.Removed:
                    return CustomCacheEntryRemovedReason.Removed;
                case CacheEntryRemovedReason.Expired:
                    return CustomCacheEntryRemovedReason.Expired;
                case CacheEntryRemovedReason.Evicted:
                    return CustomCacheEntryRemovedReason.Evicted;
                case CacheEntryRemovedReason.ChangeMonitorChanged:
                    return CustomCacheEntryRemovedReason.ChangeMonitorChanged;
                case CacheEntryRemovedReason.CacheSpecificEviction:
                    return CustomCacheEntryRemovedReason.CacheSpecificEviction;
                default:
                    return 0;
            }
        }

        #endregion
    }
}
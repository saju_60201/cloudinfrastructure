// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomCacheEntryRemovedReason.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching.Contracts
{
    /// <summary>
    ///     The cache item removed reason.
    /// </summary>
    public enum CustomCacheEntryRemovedReason
    {
        /// <summary>
        ///     The removed.
        /// </summary>
        Removed, 

        /// <summary>
        ///     The expired.
        /// </summary>
        Expired, 

        /// <summary>
        ///     The evicted.
        /// </summary>
        Evicted, 

        /// <summary>
        ///     The change monitor changed.
        /// </summary>
        ChangeMonitorChanged, 

        /// <summary>
        ///     The cache specific eviction.
        /// </summary>
        CacheSpecificEviction
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MultilanguageAttribute.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.ODataAttributes
{
    using System;

    /// <summary>
    ///     The multilanguage attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class MultilanguageAttribute : Attribute
    {
        #region Fields

        /// <summary>
        /// The property name.
        /// </summary>
        private readonly string propertyName;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MultilanguageAttribute"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        public MultilanguageAttribute(string propertyName)
        {
            this.propertyName = propertyName;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the max length.
        /// </summary>
        public int MaxLength { get; set; }

        /// <summary>
        ///     Gets or sets the max length error message.
        /// </summary>
        public string MaxLengthErrorMessage { get; set; }

        /// <summary>
        ///     Gets or sets the min length.
        /// </summary>
        public int MinLength { get; set; }

        /// <summary>
        ///     Gets or sets the min length error message.
        /// </summary>
        public string MinLengthErrorMessage { get; set; }

        /// <summary>
        ///     Gets the property name.
        /// </summary>
        public string PropertyName
        {
            get
            {
                return this.propertyName;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether required.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        ///     Gets or sets the string length.
        /// </summary>
        public int StringLength { get; set; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        public Type Type { get; set; }

        #endregion
    }
}
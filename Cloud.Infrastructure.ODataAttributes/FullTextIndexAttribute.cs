﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FullTextIndexAttribute.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.ODataAttributes
{
    using System;

    /// <summary>
    ///     The language attribute indicates if a property is designed to show lanquage specific text.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class FullTextIndexAttribute : Attribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FullTextIndexAttribute"/> class.
        /// </summary>
        /// <param name="language">
        /// full text index language
        /// </param>
        public FullTextIndexAttribute(FullTextIndexLanguage language)
        {
            this.Language = language;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the FullTextIndexLanguage.
        /// </summary>
        public FullTextIndexLanguage Language { get; set; }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FullTextIndexLanguage.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.ODataAttributes
{
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     The language.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:EnumerationItemsMustBeDocumented", 
        Justification = "Reviewed. Suppression is OK here.")]
    public enum FullTextIndexLanguage
    {
        /// <summary>
        /// The english.
        /// </summary>
        English = 1033, 

        /// <summary>
        /// The german.
        /// </summary>
        German = 1031, 

        /// <summary>
        /// The french.
        /// </summary>
        French = 1036, 

        /// <summary>
        /// The japanese.
        /// </summary>
        Japanese = 1041, 

        /// <summary>
        /// The danish.
        /// </summary>
        Danish = 1030, 

        /// <summary>
        /// The spanish.
        /// </summary>
        Spanish = 3082, 

        /// <summary>
        /// The italian.
        /// </summary>
        Italian = 1040, 

        /// <summary>
        /// The dutch.
        /// </summary>
        Dutch = 1043, 

        /// <summary>
        /// The norwegian.
        /// </summary>
        Norwegian = 2068, 

        /// <summary>
        /// The portuguese.
        /// </summary>
        Portuguese = 2070, 

        /// <summary>
        /// The finnish.
        /// </summary>
        Finnish = 1035, 

        /// <summary>
        /// The swedish.
        /// </summary>
        Swedish = 1053, 

        /// <summary>
        /// The czech.
        /// </summary>
        Czech = 1029, 

        /// <summary>
        /// The hungarian.
        /// </summary>
        Hungarian = 1038, 

        /// <summary>
        /// The polish.
        /// </summary>
        Polish = 1045, 

        /// <summary>
        /// The romanian.
        /// </summary>
        Romanian = 1048, 

        /// <summary>
        /// The croatian.
        /// </summary>
        Croatian = 1050, 

        /// <summary>
        /// The slovak.
        /// </summary>
        Slovak = 1051, 

        /// <summary>
        /// The slovenian.
        /// </summary>
        Slovenian = 1060, 

        /// <summary>
        /// The greek.
        /// </summary>
        Greek = 1032, 

        /// <summary>
        /// The bulgarian.
        /// </summary>
        Bulgarian = 1026, 

        /// <summary>
        /// The russian.
        /// </summary>
        Russian = 1049, 

        /// <summary>
        /// The turkish.
        /// </summary>
        Turkish = 1055, 

        /// <summary>
        /// The british english.
        /// </summary>
        [Description("British English")]
        BritishEnglish = 2057, 

        /// <summary>
        /// The estonian.
        /// </summary>
        Estonian = 1061, 

        /// <summary>
        /// The latvian.
        /// </summary>
        Latvian = 1062, 

        /// <summary>
        /// The lithuanian.
        /// </summary>
        Lithuanian = 1063, 

        /// <summary>
        /// The brazilian.
        /// </summary>
        Brazilian = 1046, 

        /// <summary>
        /// The traditional chinese.
        /// </summary>
        [Description("Traditional Chinese")]
        TraditionalChinese = 1028, 

        /// <summary>
        /// The korean.
        /// </summary>
        Korean = 1042, 

        /// <summary>
        /// The simplified chinese.
        /// </summary>
        [Description(" Simplified Chinese")]
        SimplifiedChinese = 2052, 

        /// <summary>
        /// The arabic.
        /// </summary>
        Arabic = 1025, 

        /// <summary>
        /// The thai.
        /// </summary>
        Thai = 1054
    }
}
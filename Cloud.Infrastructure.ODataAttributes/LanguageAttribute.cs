﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguageAttribute.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.ODataAttributes
{
    using System;

    /// <summary>
    ///     The language attribute indicates if a property is designed to show lanquage specific text.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class LanguageAttribute : Attribute
    {
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceLocator.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The ServiceLocator interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The ServiceLocator interface.
    /// </summary>
    public interface IServiceLocator
    {
        #region Public Properties

        /// <summary>
        ///     Gets the current.
        /// </summary>
        IUnityContainer Current { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get root container.
        /// </summary>
        /// <returns>
        ///     The <see cref="IUnityContainer" />.
        /// </returns>
        IUnityContainer GetRootContainer();

        /// <summary>
        ///     The get unity of work container.
        /// </summary>
        /// <returns>
        ///     The <see cref="IUnityContainer" />.
        /// </returns>
        IUnityContainer GetUnityOfWorkContainer();

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpContextLifeTimeManager.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The per call or thread life time manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Web;
using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The per call time manager.
    /// </summary>
    public class HttpContextLifeTimeManager : LifetimeManager
    {
        #region Fields

        private readonly string key = Guid.NewGuid().ToString();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get value.
        /// </summary>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public override object GetValue()
        {
            return HttpContext.Current != null ? HttpContext.Current.Items[this.key] : null;
        }

        /// <summary>
        ///     The remove value.
        /// </summary>
        public override void RemoveValue()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items.Remove(this.key);
            }
        }

        /// <summary>
        /// The set value.
        /// </summary>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        public override void SetValue(object newValue)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items[this.key] = newValue;
            }
        }

        #endregion
    }
}
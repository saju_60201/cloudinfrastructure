﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDependencyResolver.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The instance creater.
    /// </summary>
    public interface IDependencyResolver
    {
        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        void BuildUp(object instance);

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="typeToCreate">
        /// The type to create.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Resolve(Type typeToCreate);

        /// <summary>
        ///     The create.
        /// </summary>
        /// <typeparam name="T">
        ///     type to create. normally it's an interface
        /// </typeparam>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        T Resolve<T>();

        #endregion
    }
}
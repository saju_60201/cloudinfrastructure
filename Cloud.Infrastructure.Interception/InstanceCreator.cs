﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstanceCreator.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The instance creater.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The instance creater.
    /// </summary>
    [Obsolete("Please use the IDependencyResolver Interface")]
    public static class InstanceCreator
    {
        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        public static void Build(object instance)
        {
            ServiceLocator.Instance.Current.BuildUp(instance.GetType(), instance, null);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="typeToCreate">
        /// The type to create.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object Create(Type typeToCreate)
        {
            return ServiceLocator.Instance.Current.ResolveOrThrow(typeToCreate);
        }

        /// <summary>
        ///     The create.
        /// </summary>
        /// <typeparam name="T">
        ///     type to create. normally it's an interface
        /// </typeparam>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public static T Create<T>()
        {
            return (T)ServiceLocator.Instance.Current.ResolveOrThrow(typeof(T));
        }

        /// <summary>
        /// The create using unit of work container.
        /// </summary>
        /// <param name="typeToCreate">
        /// The type to create.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object CreateUsingUnitOfWorkContainer(Type typeToCreate)
        {
            return ServiceLocator.Instance.GetUnityOfWorkContainer().ResolveOrThrow(typeToCreate);
        }

        /// <summary>
        /// The create using unit of work container.
        /// </summary>
        /// <typeparam name="T">
        /// type to create
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T CreateUsingUnitOfWorkContainer<T>()
        {
            return (T)ServiceLocator.Instance.GetUnityOfWorkContainer().ResolveOrThrow(typeof(T));
        }

        #endregion
    }
}
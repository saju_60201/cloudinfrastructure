﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUnitOfWorkContainerSetter.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The UnitOfWorkContainerSetter interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The UnitOfWorkContainerSetter interface.
    /// </summary>
    public interface IUnitOfWorkContainerSetter
    {
        #region Public Methods and Operators

        /// <summary>
        /// The set unit of work container.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        void SetUnitOfWorkContainerThreadIndepended(IUnityContainer container);

        /// <summary>
        /// Set The unit of work thread static
        /// </summary>
        /// <param name="container">The container</param>
        void SetUnitOfWorkContainerThreadStatic(IUnityContainer container);

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUnityRegistrar.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The UnityRegistrar interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The UnityRegistrar interface.
    /// </summary>
    public interface IUnityRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        void Register(IUnityContainer container);

        #endregion
    }
}
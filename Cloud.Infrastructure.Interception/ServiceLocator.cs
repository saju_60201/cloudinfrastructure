﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceLocator.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The service locator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Interception
{
    using System;

    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The service locator.
    /// </summary>
    public class ServiceLocator : IServiceLocator, IUnitOfWorkContainerSetter, IServiceLocatorInitializer
    {
        #region Static Fields

        private static IServiceLocator serviceLocatorInternal;

        #endregion

        #region Fields

        [ThreadStatic]
        private static IUnityContainer unityOfWorkContainerThreadStatic;

        private IHostUnityContainer hostContainerInternal;

        private IUnityContainer unityOfWorkContainerThreadIndependend;

        #endregion

        #region Constructors and Destructors

        private ServiceLocator()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static IServiceLocator Instance
        {
            get
            {
                return serviceLocatorInternal ?? (serviceLocatorInternal = new ServiceLocator());
            }
        }

        /// <summary>
        ///     Gets the current.
        /// </summary>
        public IUnityContainer Current
        {
            get
            {
                return this.hostContainerInternal;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get root container.
        /// </summary>
        /// <returns>
        ///     The <see cref="IUnityContainer" />.
        /// </returns>
        public IUnityContainer GetRootContainer()
        {
            return this.hostContainerInternal.GetRootContainer();
        }

        /// <summary>
        ///     The get unity of work container.
        /// </summary>
        /// <returns>
        ///     The <see cref="IUnityContainer" />.
        /// </returns>
        public IUnityContainer GetUnityOfWorkContainer()
        {
            return unityOfWorkContainerThreadStatic ?? this.unityOfWorkContainerThreadIndependend;
        }

        #endregion

        #region Explicit Interface Methods

        void IServiceLocatorInitializer.Initialize(IHostUnityContainer hostUnityContainer)
        {
            if (hostUnityContainer == null)
            {
                throw new ArgumentNullException("hostUnityContainer");
            }

            if (this.hostContainerInternal != null)
            {
                throw new Exception("ServiceLocator is already initialized");
            }

            this.hostContainerInternal = hostUnityContainer;
        }

        void IUnitOfWorkContainerSetter.SetUnitOfWorkContainerThreadIndepended(IUnityContainer container)
        {
            this.unityOfWorkContainerThreadIndependend = container;
        }

        #endregion

        void IUnitOfWorkContainerSetter.SetUnitOfWorkContainerThreadStatic(IUnityContainer container)
        {
            unityOfWorkContainerThreadStatic = container;
        }
    }
}
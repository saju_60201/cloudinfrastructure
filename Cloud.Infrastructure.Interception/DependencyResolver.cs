﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DependencyResolver.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The instance creater.
    /// </summary>
    public class DependencyResolver : IDependencyResolver
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyResolver"/> class.
        /// </summary>
        /// <param name="unityContainer">
        /// The unity container.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Container musst be not null
        /// </exception>
        public DependencyResolver(IUnityContainer unityContainer)
        {
            if (unityContainer == null)
            {
                throw new ArgumentNullException("unityContainer");
            }

            this.ContainerWeakReference = new WeakReference<IUnityContainer>(unityContainer);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the i unity container.
        /// </summary>
        private IUnityContainer Container
        {
            get
            {
                IUnityContainer container;
                if (this.ContainerWeakReference.TryGetTarget(out container))
                {
                    return container;
                }

                return null;
            }
        }

        private WeakReference<IUnityContainer> ContainerWeakReference { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        public void BuildUp(object instance)
        {
            this.Container.BuildUp(instance.GetType(), instance, null);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="typeToCreate">
        /// The type to create.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Resolve(Type typeToCreate)
        {
            return this.Container.Resolve(typeToCreate);
        }

        /// <summary>
        ///     The create.
        /// </summary>
        /// <typeparam name="T">
        ///     type to create. normally it's an interface
        /// </typeparam>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public T Resolve<T>()
        {
            return this.Container.Resolve<T>();
        }

        #endregion
    }
}
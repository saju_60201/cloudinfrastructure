﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRequestUnitOfWorkUnityRegistrar.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The RequestUnitOfWorkRegistrar interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The RequestUnitOfWorkRegistrar interface.
    /// </summary>
    public interface IRequestUnitOfWorkUnityRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get types to register.
        /// </summary>
        /// <returns>
        ///     The <see cref="Dictionary" />.
        /// </returns>
        Dictionary<Type, Type> GetTypesToRegister();

        #endregion
    }
}
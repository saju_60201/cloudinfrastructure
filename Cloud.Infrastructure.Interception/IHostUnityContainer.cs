// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHostUnityContainer.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The HostUnityContainer interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Interception
{
    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The HostUnityContainer interface.
    /// </summary>
    public interface IHostUnityContainer : IUnityContainer
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get root container.
        /// </summary>
        /// <returns>
        ///     The <see cref="IUnityContainer" />.
        /// </returns>
        IUnityContainer GetRootContainer();

        #endregion
    }
}
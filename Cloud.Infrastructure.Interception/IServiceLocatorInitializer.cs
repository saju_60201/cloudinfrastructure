﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceLocatorInitializer.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The ServiceLocatorInitializer interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The ServiceLocatorInitializer interface.
    /// </summary>
    public interface IServiceLocatorInitializer
    {
        #region Public Methods and Operators

        /// <summary>
        /// The initializ.
        /// </summary>
        /// <param name="hostUnityContainer">
        /// The host unity container.
        /// </param>
        void Initialize(IHostUnityContainer hostUnityContainer);

        #endregion
    }
}
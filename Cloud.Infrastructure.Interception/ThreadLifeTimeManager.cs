﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ThreadLifeTimeManager.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The per call or thread life time manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Interception
{
    using System;
    using System.Runtime.Remoting.Messaging;

    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The per thread life time manager.
    /// </summary>
    public class ThreadLifeTimeManager : LifetimeManager
    {
        #region Fields

        private readonly string key = Guid.NewGuid().ToString();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get value.
        /// </summary>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public override object GetValue()
        {
            return CallContext.GetData(this.key);
        }

        /// <summary>
        ///     The remove value.
        /// </summary>
        public override void RemoveValue()
        {
            CallContext.FreeNamedDataSlot(this.key);
        }

        /// <summary>
        /// The set value.
        /// </summary>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        public override void SetValue(object newValue)
        {
            CallContext.SetData(this.key, newValue);
        }

        #endregion
    }
}
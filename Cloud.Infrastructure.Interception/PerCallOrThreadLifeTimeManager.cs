﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerCallOrThreadLifeTimeManager.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The per call or thread life time manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Interception
{
    using System;
    using System.Runtime.Remoting.Messaging;
    using System.Web;

    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The per call or thread life time manager.
    /// </summary>
    [Obsolete("There are two more specific classes for Thread and HttpContext")]
    public class PerCallOrThreadLifeTimeManager : LifetimeManager
    {
        #region Fields

        private readonly string key = Guid.NewGuid().ToString();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get value.
        /// </summary>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public override object GetValue()
        {
            return HttpContext.Current != null ? HttpContext.Current.Items[this.key] : CallContext.GetData(this.key);
        }

        /// <summary>
        ///     The remove value.
        /// </summary>
        public override void RemoveValue()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items.Remove(this.key);
                return;
            }

            CallContext.FreeNamedDataSlot(this.key);
        }

        /// <summary>
        /// The set value.
        /// </summary>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        public override void SetValue(object newValue)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items[this.key] = newValue;
                return;
            }

            CallContext.SetData(this.key, newValue);
        }

        #endregion
    }
}
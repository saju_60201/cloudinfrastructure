﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IChildContainer.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The ChildContainer interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Interception
{
    /// <summary>
    ///     The ChildContainer interface.
    /// </summary>
    public interface IChildContainer
    {
        #region Public Properties

        /// <summary>
        ///     Gets the container.
        /// </summary>
        IUnityContainer Container { get; }

        #endregion
    }
}
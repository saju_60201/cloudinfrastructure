﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateAccountCommand.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Commands
{
    using System;

    using Cloud.Infrastructure.Bus;

    using NServiceBus;

    /// <summary>
    ///     The create account command.
    /// </summary>
    public class CreateAccountCommand : ICommand
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateAccountCommand"/> class.
        /// </summary>
        /// <param name="accountName">
        /// The account name.
        /// </param>
        public CreateAccountCommand(string accountName)
        {
            this.AccountName = accountName;
            this.AccountId = Guid.NewGuid();
            this.AccountNumber = DateTime.Now.Ticks.ToString();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the account id.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        ///     Gets or sets the account name.
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        ///     Gets or sets the account number.
        /// </summary>
        public string AccountNumber { get; set; }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountBasicViewModel.cs" company="">
//   
// </copyright>
// <summary>
//   The account basic view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace TestApp.Models
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    using Banking.Dtos;

    /// <summary>
    ///     The account basic view model.
    /// </summary>
    public class AccountBasicViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the account id.
        /// </summary>       
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        ///     Gets or sets the account name.
        /// </summary>
        [Required]
        public string AccountName { get; set; }

        /// <summary>
        ///     Gets or sets the account number.
        /// </summary>
        [Required]
        public string AccountNumber { get; set; }

        /// <summary>
        ///     Gets or sets the balance.
        /// </summary>
        [Required]        
        public decimal Balance { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is active.
        /// </summary>
        public bool IsActive { get; set; }
       
        #endregion
    }
}
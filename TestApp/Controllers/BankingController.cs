﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BankingController.cs" company="">
//   
// </copyright>
// <summary>
//   The banking controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace TestApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Banking.Dtos;

    using Cloud.Infrastructure.Utility;
    using Cloud.Repository;

    using TestApp.Models;

    /// <summary>
    ///     The banking controller.
    /// </summary>
    public class BankingController : Controller
    {
        #region Fields

        /// <summary>
        ///     The banking repository.
        /// </summary>
        private readonly IBankingRepository bankingRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BankingController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        public BankingController(IBankingRepository repository)
        {
            this.bankingRepository = repository;
        }

        #endregion

        // GET: /Banking/

        // GET: /Banking/Create
        #region Public Methods and Operators

        /// <summary>
        ///     The create.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Create()
        {

            return this.View(new AccountBasicViewModel()
                                 {
                                     AccountId = Guid.NewGuid(),Balance = 0
                                 });
        }

        // POST: /Banking/Create
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="accountInfo">
        /// The account Info.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(AccountBasicViewModel accountInfo)
        {
            try
            {
                if (!this.ModelState.IsValid)
                {
                    return View(accountInfo);
                }

                var account = (Account)Mapper.Map(accountInfo, new Account());
                this.bankingRepository.Insert(account);
                  this.bankingRepository.SaveChanges();
                
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: /Banking/Edit/5

        // GET: /Banking/Delete/5
        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            return this.View();
        }

        // POST: /Banking/Delete/5
        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            return this.View();
        }

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            return this.View();
        }

        // POST: /Banking/Edit/5
        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        /// <summary>
        ///     The index.
        /// </summary>
        /// <returns>
        ///     The <see cref="ActionResult" />.
        /// </returns>
        public ActionResult Index()
        {
            List<Account> accountList = this.bankingRepository.Get<Account>().ToList();

            var accountModelList = new List<AccountBasicViewModel>();
            foreach (Account account in accountList)
            {
                accountModelList.Add((AccountBasicViewModel)Mapper.Map(account, new AccountBasicViewModel()));
            }

            return this.View(accountModelList);
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityEventSourceFactory.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The unity event source factory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host.Bus.EventStore
{
    using System;

    using Cloud.Infrastructure.Interception;

    using NES.Contracts;

    /// <summary>
    ///     The unity event source factory.
    /// </summary>
    public class UnityEventSourceFactory : IEventSourceFactory
    {
        #region Fields

        private readonly IServiceLocator serviceLocator;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityEventSourceFactory"/> class.
        /// </summary>
        /// <param name="serviceLocator">
        /// The service locator.
        /// </param>
        public UnityEventSourceFactory(IServiceLocator serviceLocator)
        {
            this.serviceLocator = serviceLocator;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The create.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of the eventsource
        /// </typeparam>
        /// <returns>
        ///     The <see cref="T" />.
        /// </returns>
        public T Create<T>() where T : IEventSourceBase
        {
            var instance = (T)Activator.CreateInstance(typeof(T), true);
            this.serviceLocator.GetRootContainer().BuildUp(typeof(T), instance, null);
            return instance;
        }

        #endregion
    }
}
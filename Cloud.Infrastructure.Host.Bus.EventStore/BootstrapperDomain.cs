﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BootstrapperDomain.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host.Bus.EventStore
{
    using Cloud.Infrastructure.Logging.Contracts;

    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The bootstrapper.
    /// </summary>
    public class BootstrapperDomain : BootstrapperBus
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BootstrapperDomain"/> class.
        /// </summary>
        /// <param name="enableTenantAwareness">
        /// if set to <c>true</c> [enable tenant awareness].
        /// </param>
        public BootstrapperDomain(bool enableTenantAwareness = false)
            : base(enableTenantAwareness)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The register types.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        protected override void RegisterTypes(IUnityContainer container, ILogger logger)
        {
            base.RegisterTypes(container, logger);
        }

        #endregion
    }
}
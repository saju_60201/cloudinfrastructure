﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantConfigurationConnectionFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host.Bus.EventStore
{
    using System;
    using System.Collections.Concurrent;
    using System.Configuration;
    using System.Data;
    using System.Transactions;

    using Cloud.Infrastructure.Interception;
    using Cloud.Infrastructure.Logging.Contracts;

    //using Microsoft.Practices.ServiceLocation;

    using NEventStore.Persistence.Sql;

    /// <summary>
    ///     The tenant configuration connection factory.
    /// </summary>
    public class TenantConfigurationConnectionFactory :NEventStore.Persistence.Sql.ConfigurationConnectionFactory
    {
        #region Fields

        /// <summary>
        /// The connection name prefix.
        /// </summary>
        private readonly string connectionNamePrefix;

        /// <summary>
        /// The initialized engines.
        /// </summary>
        private readonly ConcurrentDictionary<string, bool> initializedEngines = new ConcurrentDictionary<string, bool>();

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// The service locator.
        /// </summary>
        private readonly IServiceLocator serviceLocator;

        /// <summary>
        /// The sql dialect.
        /// </summary>
        private readonly ISqlDialect sqlDialect;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TenantConfigurationConnectionFactory"/> class.
        /// </summary>
        /// <param name="connectionName">
        /// The connection name.
        /// </param>
        /// <param name="serviceLocator">
        /// The service locator.
        /// </param>
        /// <param name="sqlDialect">
        /// The sql dialect.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// all arguments must not be null
        /// </exception>
        public  TenantConfigurationConnectionFactory(
            string connectionName, 
            IServiceLocator serviceLocator, 
            ISqlDialect sqlDialect, 
            ILogger logger)
            : base(connectionName)
        {
            if (serviceLocator == null)
            {
                throw new ArgumentNullException("serviceLocator");
            }

            if (sqlDialect == null)
            {
                throw new ArgumentNullException("sqlDialect");
            }

            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.serviceLocator = serviceLocator;
            this.sqlDialect = sqlDialect;
            this.logger = logger;
            this.connectionNamePrefix = connectionName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get setting.
        /// </summary>
        /// <param name="connectionName">
        /// The connection name.
        /// </param>
        /// <returns>
        /// The <see cref="ConnectionStringSettings"/>.
        /// </returns>
        protected override ConnectionStringSettings GetSetting(string connectionName)
        {
            return base.GetSetting(this.GetClassicConnectionName());
        }

        /// <summary>
        /// The open.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <returns>
        /// The <see cref="IDbConnection"/>.
        /// </returns>
        protected override IDbConnection Open(string connectionString, ConnectionStringSettings setting)
        {
            this.logger.Debug(() => string.Format("open connection to database {0}", setting.Name));
            var connection = base.Open(connectionString, setting);
            this.IntializeEngine(setting.Name, connection);
            return connection;
        }

        /// <summary>
        /// The open.
        /// </summary>
        /// <param name="connectionName">
        /// The connection name.
        /// </param>
        /// <returns>
        /// The <see cref="IDbConnection"/>.
        /// </returns>
        protected override IDbConnection Open(string connectionName)
        {
            ConnectionStringSettings setting = this.GetSetting(connectionName);
            string connectionString = setting.ConnectionString;
            return this.Open(connectionString, setting);
        }

        //private bool CheckDoWeHaveAValidClaimsPrincipal()
        //{
        //    var securityDataProvider = this.serviceLocator.GetUnityOfWorkContainer().ResolveSafe<ISecurityDataProvider>(new NullLogger());
        //    if (securityDataProvider == null || !securityDataProvider.IsAuthenticated())
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        /// <summary>
        /// The get classic connection name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetClassicConnectionName()
        {
            return this.connectionNamePrefix;
        }

        //private string GetCustomerSpecificConnectionName()
        //{
        //    Customer currentCustomer = null;
        //    if (this.CheckDoWeHaveAValidClaimsPrincipal())
        //    {
        //        this.logger.Debug(
        //            () => string.Format("Generate customer specific connectionName for the name {0}", this.connectionNamePrefix));
        //        var customerRuntimeProvider = this.serviceLocator.GetUnityOfWorkContainer().Resolve<ICustomerRuntimeProvider>();
        //        currentCustomer = customerRuntimeProvider.GetCurrentCustomer(true);
        //    }

        //    if (currentCustomer == null)
        //    {
        //        this.logger.Warning(() => "To be able to store customer specific data the customerids must be not null");
        //        ConnectionStringSettings setting =
        //            ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>()
        //                .FirstOrDefault(
        //                    c =>
        //                    c.Name.StartsWith(string.Format("{0}_", this.connectionNamePrefix), StringComparison.InvariantCultureIgnoreCase));
        //        if (setting == null)
        //        {
        //            throw new ConfigurationErrorsException(
        //                string.Format("No connection string found which starts with the connectionName {0}", this.connectionNamePrefix));
        //        }

        //        this.logger.Warning(
        //            () =>
        //            string.Format(
        //                "During the startup we need a database because of the dispatcher. So we wil return here the first found database {0}", 
        //                setting.Name));
        //        return setting.Name;
        //    }

        //    var targetCustomerId = currentCustomer.TechnicalCustomerId;

        //    this.logger.Debug(() => string.Format("The target customer id is {0}", targetCustomerId));

        //    if (targetCustomerId == Guid.Empty)
        //    {
        //        throw new Exception("Customer id must not be null or empty");
        //    }

        //    var connectionName = string.Format("{0}_{1}", this.connectionNamePrefix, targetCustomerId.ToString("N"));
        //    this.logger.Debug(() => string.Format("The generated connection name is {0}", connectionName));
        //    return connectionName;
        //}

        //private string GetTenantSpecificConnectionName()
        //{
        //    TenantRuntime currentTenant = null;
        //    if (this.CheckDoWeHaveAValidClaimsPrincipal())
        //    {
        //        this.logger.Debug(
        //            () => string.Format("Generate tenant specific connectionName for the name {0}", this.connectionNamePrefix));
        //        var tenantRuntimeProvider = this.serviceLocator.GetUnityOfWorkContainer().Resolve<ITenantRuntimeProvider>();
        //        currentTenant = tenantRuntimeProvider.GetCurrentTenant(true, true);
        //    }

        //    if (currentTenant == null)
        //    {
        //        this.logger.Warning(() => "To be able to store tenant specific data the tenantids must be not null");
        //        ConnectionStringSettings setting =
        //            ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>()
        //                .FirstOrDefault(
        //                    c =>
        //                    c.Name.StartsWith(string.Format("{0}_", this.connectionNamePrefix), StringComparison.InvariantCultureIgnoreCase));
        //        if (setting == null)
        //        {
        //            throw new ConfigurationErrorsException(
        //                string.Format("No connection string found which starts with the connectionName {0}", this.connectionNamePrefix));
        //        }

        //        this.logger.Warning(
        //            () =>
        //            string.Format(
        //                "During the startup we need a database because of the dispatcher. So we wil return here the first found database {0}", 
        //                setting.Name));
        //        return setting.Name;
        //    }

        //    var targetTenantId = currentTenant.Tenant.TenantId;

        //    this.logger.Debug(() => string.Format("The target tenantId is {0}", targetTenantId));

        //    if (targetTenantId == Guid.Empty)
        //    {
        //        throw new Exception("TenantId must not be null or empty");
        //    }

        //    var connectionName = string.Format("{0}_{1}", this.connectionNamePrefix, targetTenantId.ToString("N"));
        //    this.logger.Debug(() => string.Format("The generated connection name is {0}", connectionName));
        //    return connectionName;
        //}

        /// <summary>
        /// The intialize engine.
        /// </summary>
        /// <param name="connectionName">
        /// The connection name.
        /// </param>
        /// <param name="connection">
        /// The connection.
        /// </param>
        private void IntializeEngine(string connectionName, IDbConnection connection)
        {
            bool isInitialized;
            if (this.initializedEngines.TryGetValue(connectionName, out isInitialized) && isInitialized)
            {
                this.logger.Debug(() => "Engine is initialized, so go out here");
                return;
            }

            this.logger.Debug(() => string.Format("Initialize storage engine for {0}", connectionName));
            using (var scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                var transaction = this.sqlDialect.OpenTransaction(connection);
                var statement = this.sqlDialect.BuildStatement(scope, connection, transaction);
                statement.ExecuteWithoutExceptions(this.sqlDialect.InitializeStorage);
            }

            this.initializedEngines.AddOrUpdate(connectionName, true, (k, v) => true);
            this.logger.Debug(() => "Initialization done of the storage engine");
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host.Bus.EventStore
{
    using Cloud.Infrastructure.EventStore;
    using Cloud.Infrastructure.Host.Contracts;

    //using Microsoft.Practices.ServiceLocation;
    using Cloud.Infrastructure.Interception;

    using Microsoft.Practices.Unity;

    using NES;
    using NES.Contracts;
    using NES.NEventStore;
    using NES.NServiceBus;

    using NEventStore;
    using NEventStore.Persistence.Sql.SqlDialects;

    using NServiceBus;

    using ILogger = Cloud.Infrastructure.Logging.Contracts.ILogger;
    using Repository = Cloud.Infrastructure.EventStore.Repository;

    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
       
        /// <summary>
        /// The event store classic.
        /// </summary>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <param name="connectionName">
        /// The connection name.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig EventStoreClassic(this IBootstrapperConfig config, string connectionName, ILogger logger = null)
        {
            logger = logger ?? config.Logger();

            // configure the bus for eventsourcing ToDo [bgali]
            var configure = config.Container.Resolve<Configure>();
            configure.NES();

            // register repository
            config.Container.RegisterType<IRepository, Repository>(new ContainerControlledLifetimeManager());

            // event store
            var sqlDialect = new MsSqlDialect();
           
            Wireup.Init()
                .UsingSqlPersistence(connectionName)
                .WithDialect(sqlDialect)
                .EnlistInAmbientTransaction()                                          
                .LogTo(type => new EventStoreLogger(type, logger))
                .NES()
                .UsingJsonSerialization()
                .Build();     
                                  

            // use custom eventsource factory
            DI.Current.Register<IEventSourceFactory>(() => new UnityEventSourceFactory(ServiceLocator.Instance));

            return config;
        }

    }
}
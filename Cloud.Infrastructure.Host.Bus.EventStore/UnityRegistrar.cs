﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityRegistrar.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host.Bus.EventStore
{
    using Cloud.Infrastructure.EventStore;
    using Cloud.Infrastructure.Interception;

    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The unity registrar.
    /// </summary>
    public class UnityRegistrar : IUnityRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public void Register(IUnityContainer container)
        {
            container.RegisterType<IRepositoryPipesFactory, RepositoryPipesFactory>(new ContainerControlledLifetimeManager());
            //container.RegisterType<IStateValidationService, StateValidationService>(new ContainerControlledLifetimeManager());
            //container.RegisterType<IAggregateValidationFailedInvoker, AggregateValidationFailedInvoker>(new ContainerControlledLifetimeManager());
            //container.RegisterType<IApplyEventActionProvider, ApplyEventActionProvider>(new ContainerControlledLifetimeManager());
        }

        #endregion
    }
}
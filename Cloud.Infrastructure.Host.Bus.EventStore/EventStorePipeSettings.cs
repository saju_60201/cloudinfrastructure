﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventStorePipeSettings.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Host.Bus.EventStore
{
    /// <summary>
    /// The event store pipe settings.
    /// </summary>
    public static class EventStorePipeSettings
    {
        [ThreadStatic]
        private static bool isEnabled;

        /// <summary>
        /// Gets a value indicating whether is enabled.
        /// </summary>
        public static bool IsEnabled
        {
            get { return isEnabled; }
        }

        /// <summary>
        /// The disable.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Disable()
        {
            if (!isEnabled)
            {
                return false;
            }

            isEnabled = false;
            return true;
        }

        /// <summary>
        /// The enable.
        /// </summary>
        public static void Enable()
        {
            isEnabled = true;
        }
    }
}
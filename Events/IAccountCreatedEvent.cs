﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAccountCreatedEvent.cs" company="">
//   
// </copyright>
// <summary>
//   The AccountCreatedEvent interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Events
{
    using System;

    using NServiceBus;

    /// <summary>
    /// The AccountCreatedEvent interface.
    /// </summary>
    public interface IAccountCreatedEvent : IEvent
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the account id.
        /// </summary>
        Guid AccountId { get; set; }

        /// <summary>
        /// Gets or sets the account name.
        /// </summary>
        string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        string AccountNumber { get; set; }

        /// <summary>
        /// Gets or sets the balance.
        /// </summary>
        decimal Balance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active.
        /// </summary>
        bool IsActive { get; set; }

        #endregion
    }
}
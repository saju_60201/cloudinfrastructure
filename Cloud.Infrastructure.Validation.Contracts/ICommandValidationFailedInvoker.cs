﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommandValidationFailedInvoker.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Validation.Contracts
{
    /// <summary>
    ///     The CommandValidationFailedInvoker interface.
    /// </summary>
    public interface ICommandValidationFailedInvoker
    {
        /// <summary>
        /// Invoke the handlers
        ///     If one of the Handlers has handled the ValidationResult then
        ///     the exception will not be raised
        /// </summary>
        /// <param name="validationResult">
        /// The validation Result.
        /// </param>
        void InvokeHandlers(ValidationResult validationResult);
    }
}
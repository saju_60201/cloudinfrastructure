﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidationResult.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Cloud.Infrastructure.Validation.Contracts
{
    /// <summary>
    ///     The validation result.
    /// </summary>
    public class ValidationResult : IHandledResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationResult"/> class.
        /// </summary>
        /// <param name="validationNotification">
        /// The validation notification.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="originalNotification">
        /// The original Notification.
        /// </param>
        /// <param name="isValid">
        /// The is Valid.
        /// </param>
        public ValidationResult(            
            object message, 
            object originalNotification, 
            bool isValid)
        {
           // this.ValidationNotification = validationNotification;
            this.Message = message;
            this.OriginalNotification = originalNotification;
            this.IsValid = isValid;
        }

        /// <summary>
        ///     Gets the message.
        /// </summary>
        public object Message { get; private set; }

        /// <summary>
        ///     Gets a value indicating whether is valid.
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        ///     Gets the validation notification.
        /// </summary>
        public string ValidationNotification { get; private set; }

        /// <summary>
        ///     Gets the original notification.
        /// </summary>
        public object OriginalNotification { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is handled.
        /// </summary>
        public bool IsHandled { get; set; }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public override string ToString()
        {
            if (this.OriginalNotification != null)
            {
                return this.OriginalNotification.ToString();
            }

            //if (!string.IsNullOrEmpty(this.ValidationNotification))
            //{
            //    return ValidationNotification;
            //}

            return base.ToString();
        }
    }
}
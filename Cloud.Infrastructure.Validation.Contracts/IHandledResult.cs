﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHandledResult.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Validation.Contracts
{
    /// <summary>
    ///     The HandledResult interface.
    /// </summary>
    public interface IHandledResult
    {
        /// <summary>
        ///     Gets or sets a value indicating whether is handled.
        /// </summary>
        bool IsHandled { get; set; }
    }
}
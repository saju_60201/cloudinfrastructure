﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICommandValidationFailedHandler.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Validation.Contracts
{
    /// <summary>
    ///     The CommandValidationFailedHandler interface.
    /// </summary>
    public interface ICommandValidationFailedHandler
    {
        /// <summary>
        /// The handle command failed.
        /// </summary>
        /// <param name="validationResult">
        /// The validation result.
        /// </param>
        void HandleCommandFailed(ValidationResult validationResult);
    }
}
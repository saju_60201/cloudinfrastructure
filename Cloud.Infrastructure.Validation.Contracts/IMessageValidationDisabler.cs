﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMessageValidationDisabler.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Validation.Contracts
{
    /// <summary>
    ///     The MessageValidationDisabler interface.
    /// </summary>
    public interface IMessageValidationDisabler
    {
        /// <summary>
        /// The disable validation.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void DisableValidation(object message);

        /// <summary>
        /// The is validation disabled.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsValidationDisabled(object message);
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMessageValidationService.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace Cloud.Infrastructure.Validation.Contracts
{
    /// <summary>
    ///     The MessageValidationService interface.
    /// </summary>
    public interface IMessageValidationService
    {
        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="throwExceptionIfNotValid">
        /// The throw exception if not valid.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/>.
        /// </returns>
        ValidationResult Validate(object message, bool throwExceptionIfNotValid = false);
    }
}
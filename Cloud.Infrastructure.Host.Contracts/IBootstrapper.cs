// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBootstrapper.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The BootstrapperWebApi interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Host.Contracts
{
    using System.Collections.Generic;

    using Cloud.Infrastructure.Host.Contracts;

    /// <summary>
    /// The BootstrapperWebApi interface.
    /// </summary>
    public interface IBootstrapper
    {
        /// <summary>
        /// Registers the dynamic request unit of work types.
        /// </summary>
        /// <param name="typesToRegister">The types to register.</param>
        void RegisterDynamicRequestUnitOfWorkTypes(IList<UnityTypeMapping> typesToRegister);
    }
}
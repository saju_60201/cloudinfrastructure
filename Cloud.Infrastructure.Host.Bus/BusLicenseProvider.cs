﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BusLicenseProvider.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using Cloud.Infrastructure.Bus.Contracts;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The bus license provider.
    /// </summary>
    public class BusLicenseProvider : IBusLicenseProvider
    {
        /// <summary>
        ///     The get license.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string GetLicense()
        {
            //todo remove this temporary implementation, every customer has differenct license
            string resourceId = this.GetType().Namespace + ".License.xml";
            var licenseStream = this.GetType().Assembly.GetManifestResourceStream(resourceId);
            using (var sr = new StreamReader(licenseStream))
            {
                return sr.ReadToEnd();
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BusExtensions.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Reflection;
using Cloud.Infrastructure.Bus.Contracts;
using Cloud.Infrastructure.Host.Bus.JsonTemp;
using Cloud.Infrastructure.Host.Contracts;
using Cloud.Infrastructure.Interception;
using Microsoft.Practices.Unity;
using NServiceBus;
using NServiceBus.Configuration.AdvanceExtensibility;
using NServiceBus.Features;
using NServiceBus.Log4Net;
using NServiceBus.Logging;
using NServiceBus.Persistence.NHibernate;
using NServiceBus.Unicast.Transport;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The bus extensions.
    /// </summary>
    public static class BusExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The configure nsb bus.
        /// </summary>
        /// <param name="bootstrapperConfig">
        /// The bootstrapper config.
        /// </param>
        /// <param name="enpointName">
        /// The enpoint name.
        /// </param>
        /// <param name="isAutoSubscribe">
        /// The is auto subscribe.
        /// </param>
        /// <param name="isSendOnly">
        /// The is send only.
        /// </param>
        /// <param name="isCommandServer">
        /// is all handled using web request lifetime
        /// </param>
        /// <param name="useSingleBrokerQueue">
        /// The use Single Broker Queue.
        /// </param>
        /// <param name="disableCallbackReceiver">
        /// The disable Callback Receiver, so Bus.Return is disabled
        ///     Secondary queue tables have the name of the machine appended to the name of the primary queue table with . as
        ///     separator e.g. SomeEndpoint.MyMachine. will not be created
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig ConfigureNsbBus(
            this IBootstrapperConfig bootstrapperConfig, 
            string enpointName = null, 
            bool isAutoSubscribe = false, 
            bool isSendOnly = false, 
            bool isCommandServer = false, 
            bool useSingleBrokerQueue = true, 
            bool disableCallbackReceiver = true)
        {
            if (bootstrapperConfig.IsLoggingEnabled)
            {
                log4net.Config.XmlConfigurator.Configure();
                LogManager.Use<Log4NetFactory>();
            }

            enpointName = enpointName ?? System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();

            // automatically scans bin directory
            BusConfiguration busConfiguration = null;
            try
            {
                busConfiguration = ServiceLocator.Instance.GetRootContainer().Resolve<BusConfiguration>();
            }
            catch (Exception)
            {
                busConfiguration = new BusConfiguration();
            }

            //check dll dependencies
            busConfiguration.AssembliesToScan(CurrentDomainTypes.GetAssemblies());

            busConfiguration.ScanAssembliesInDirectory(GetDirectoryName());

            if (useSingleBrokerQueue)
            {
                busConfiguration.ScaleOut().UseSingleBrokerQueue();
            }

            // configure endpointname
            if (!string.IsNullOrEmpty(enpointName))
            {
                busConfiguration.EndpointName(enpointName);
            }

            // append nlog to log4net
            Log4NetAppender.Initialize(busConfiguration.Logger());

            //busConfiguration.
            busConfiguration.UseContainer<UnityBuilder>(b => b.UseExistingContainer(bootstrapperConfig.Container));
            var trasport = busConfiguration.UseTransport<SqlServerTransport>();
   
            busConfiguration.UsePersistence<NHibernatePersistence>().EnableCachingForSubscriptionStorage(TimeSpan.FromHours(12));

            busConfiguration.PurgeOnStartup(false);

            if (disableCallbackReceiver)
            {
                trasport.DisableCallbackReceiver();
            }

            // required to figure ot which childcontiner is used to resolve the unitofwork interfaces
            //ToDo [bgali] NServiceBus.ObjectBuilder.Unity.DefaultInstances.Add(typeof(IChildContainer));
            busConfiguration.RegisterComponents(c => c.ConfigureComponent<IChildContainer>(DependencyLifecycle.InstancePerUnitOfWork));

            // IBuilder is set so we can now register unityofwork
            ((BootstrapperBus)bootstrapperConfig.Bootstrapper).RegisterUnitOfWorkTypes(isCommandServer, busConfiguration);

            busConfiguration.Transactions().Enable();
            busConfiguration.Transactions().EnableDistributedTransactions();
            busConfiguration.Transactions().DefaultTimeout(TimeSpan.FromSeconds(40));

            busConfiguration.UseSerialization<JsonCustomSerializer>();

            if (!isAutoSubscribe)
            {
                busConfiguration.DisableFeature<AutoSubscribe>();
            }

            if (isSendOnly)
            {
                busConfiguration.GetSettings().Set("Endpoint.SendOnly", true);
                busConfiguration.DisableFeature<TimeoutManager>();
            }

            // register nservicebus
            var licenseProvider = bootstrapperConfig.Container.Resolve<IBusLicenseProvider>();
            busConfiguration.License(licenseProvider.GetLicense());

            bootstrapperConfig.Container.RegisterInstance(typeof(BusConfiguration), busConfiguration);

            busConfiguration.LoadMessageHandlers(new First<FirstDummyCommandHandler>());

            // Enable this if you would like to use your own Fault Manager, this
            // will also disable SLR. This can't be done using INeedInitialization because of the order
            // configure.Configurer.ConfigureComponent<CustomFaultManager>(DependencyLifecycle.InstancePerCall);
            //ToDo [bgali] Configure.ConfigurationComplete += () => Configure.Builder.Build<ITransport>().TransportMessageReceived += OnTransportMessageReceived;
            return bootstrapperConfig;
        }

        /// <summary>
        /// The get header.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="bus">
        /// The bus.
        /// </param>
        /// <param name="headerId">
        /// The header id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetHeader(this object message, IBus bus, string headerId)
        {
            var transportMessage = message as TransportMessage;
            if (transportMessage != null)
            {
                if (!transportMessage.Headers.ContainsKey(headerId))
                {
                    return null;
                }

                return transportMessage.Headers[headerId];
            }

            return bus.GetMessageHeader(message, headerId);
        }

        /// <summary>
        /// The is n service bus control message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="bus">
        /// IBus impl
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsNServiceBusControlMessage(this object message, IBus bus)
        {
            if (message == null)
            {
                return false;
            }

            return message.GetHeader(bus, NServiceBus.Headers.ControlMessageHeader) == true.ToString();
        }

        /// <summary>
        /// The set header.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="bus">
        /// ISendOnlyBus
        /// </param>
        /// <param name="headerId">
        /// The header id.
        /// </param>
        /// <param name="valueToSet">
        /// The value to set.
        /// </param>
        public static void SetHeader(this object message, ISendOnlyBus bus, string headerId, string valueToSet)
        {
            var transportMessage = message as TransportMessage;
            if (transportMessage != null)
            {
                transportMessage.Headers.Add(headerId, valueToSet);
                return;
            }

            bus.SetMessageHeader(message, headerId, valueToSet);
        }

        /// <summary>
        /// The start nsb bus.
        /// </summary>
        /// <param name="bootstrapperConfig">
        /// The bootstrapper config.
        /// </param>
        /// <param name="doInstall">
        /// The do install.
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig StartNsbBus(this IBootstrapperConfig bootstrapperConfig, bool doInstall = true)
        {
            var busConfiguration = bootstrapperConfig.Container.Resolve<BusConfiguration>();

            if (busConfiguration == null)
            {
                throw new ArgumentNullException("busConfiguration not set!");
            }

            if (doInstall)
            {
                busConfiguration.EnableInstallers();
            }

            // start bus
            var bus = NServiceBus.Bus.Create(busConfiguration);
            bus.Start();

            return bootstrapperConfig;
        }

        #endregion

        #region Methods

        private static string GetDirectoryName()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);

            if (uri.Uri.IsUnc)
            {
                return Path.GetDirectoryName(uri.Uri.LocalPath);
            }
            else
            {
                var path = Uri.UnescapeDataString(uri.Path);
                var directoryName = Path.GetDirectoryName(path);
                return directoryName;
            }
        }

        private static void OnTransportMessageReceived(object sender, TransportMessageReceivedEventArgs e)
        {
            sender.Logger().Debug(() => "Transport message received");
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitOfWorkContainerMutator.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Logging.Contracts;
using NServiceBus.MessageMutator;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    /// The unit of work container mutator.
    /// </summary>
    public class UnitOfWorkContainerMutator : IMutateIncomingMessages
    {
        private readonly IChildContainer childContainer;

        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkContainerMutator"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="childContainer">
        /// The child container.
        /// </param>
        public UnitOfWorkContainerMutator(ILogger logger, IChildContainer childContainer)
        {
            this.logger = logger;
            this.childContainer = childContainer;
        }

        /// <summary>
        /// The mutate incoming.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object MutateIncoming(object message)
        {
            ((IUnitOfWorkContainerSetter)ServiceLocator.Instance).SetUnitOfWorkContainerThreadStatic(this.childContainer.Container);
            return message;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidationMessageMutator.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Cloud.Infrastructure.Logging.Contracts;
using Cloud.Infrastructure.Validation.Contracts;
using NServiceBus;
using NServiceBus.MessageMutator;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The validation message mutator.
    /// </summary>
    public class ValidationMessageMutator : IMessageMutator
    {
        private readonly ILogger logger;

        private readonly IMessageValidationService messageValidationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationMessageMutator"/> class.
        /// </summary>
        /// <param name="messageValidationService">
        /// The message Validation Service.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="bus">
        /// The bus.
        /// </param>
        public ValidationMessageMutator(IMessageValidationService messageValidationService, ILogger logger, IBus bus)
        {
            this.messageValidationService = messageValidationService;
            this.logger = logger;
            this.Bus = bus;
        }

        /// <summary>
        ///     Gets or sets the bus.
        /// </summary>
        private IBus Bus { get; set; }

        /// <summary>
        /// The mutate incoming.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object MutateIncoming(object message)
        {
            this.logger.Debug(() => "Validate incoming messages");
            this.ValidateInternal(message);
            return message;
        }

        /// <summary>
        /// The mutate outgoing.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object MutateOutgoing(object message)
        {
            //no validation check on outgoing messages
            return message;
        }

        private void ValidateInternal(object message)
        {
            try
            {
                var valResult = this.messageValidationService.Validate(message, true);
                if (!valResult.IsValid)
                {
                    this.StopTheChain();
                }
            }
            catch
            {
                this.StopTheChain();
                throw;
            }
        }

        private void StopTheChain()
        {
            this.logger.Debug(() => "DoNotContinueDispatchingCurrentMessageToHandlers to prevent going the message to handlers and error queue.");
            this.Bus.DoNotContinueDispatchingCurrentMessageToHandlers();
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageValidationDisabler.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Cloud.Infrastructure.Bus;
using Cloud.Infrastructure.Validation.Contracts;
using Microsoft.Practices.Unity;
using NServiceBus;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The message validation disabler.
    /// </summary>
    public class MessageValidationDisabler : IMessageValidationDisabler
    {
        /// <summary>
        ///     Gets or sets the bus.
        /// </summary>
        [Dependency]
        public IBus Bus { get; set; }

        /// <summary>
        /// The disable validation.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void DisableValidation(object message)
        {
            message.DisableValidation(this.Bus);
        }

        /// <summary>
        /// The is validation disabled.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsValidationDisabled(object message)
        {
            return message.IsValidationDisabled(this.Bus);
        }
    }
}
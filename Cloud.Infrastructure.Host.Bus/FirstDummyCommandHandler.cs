﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FirstDummyCommandHandler.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using NServiceBus;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    /// The first dummy command handler.
    /// </summary>
    public class FirstDummyCommandHandler : NServiceBus.IHandleMessages<ICommand>
    {
        #region Public Methods and Operators

        /// <summary>
        /// The handle.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Handle(ICommand message)
        {
            Debug.Print("my first handler");
        }

        #endregion
    }
}
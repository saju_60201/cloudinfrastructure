﻿using NServiceBus;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The mutator initializer.
    /// </summary>
    public class MutatorInitializer : INeedInitialization
    {
        /// <summary>
        ///     The customize.
        /// </summary>
        /// <param name="configuration">
        ///     The configuration.
        /// </param>
        public void Customize(BusConfiguration configuration)
        {
            var logger = this.Logger();
            logger.Debug(() => "Configure ClaimsMessageMutator");
            configuration.RegisterComponents(
                c => c.ConfigureComponent<UnitOfWorkContainerMutator>(DependencyLifecycle.InstancePerCall));
            //configuration.RegisterComponents(c => c.ConfigureComponent<ClaimsMessageMutator>(DependencyLifecycle.InstancePerCall));

            configuration.RegisterComponents(
                c => c.ConfigureComponent<ValidationMessageMutator>(DependencyLifecycle.InstancePerCall));
            //configuration.RegisterComponents(c => c.ConfigureComponent<AuthorizationMessageMutator>(DependencyLifecycle.InstancePerCall));
            // publish the validationcatalog
            Validation.Extensions.ScanCurrentDomain(this.Logger());
        }
    }
}
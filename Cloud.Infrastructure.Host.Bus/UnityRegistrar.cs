﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityRegistrar.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Cloud.Infrastructure.Bus.Contracts;
using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Validation;
using Cloud.Infrastructure.Validation.Contracts;
using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The unity registrar.
    /// </summary>
    public class UnityRegistrar : IUnityRegistrar
    {
        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public void Register(IUnityContainer container)
        {
            container.RegisterType<ICommandValidationFailedInvoker, CommandValidationFailedInvoker>(new ContainerControlledLifetimeManager());
            //container.RegisterType<IBusinessExceptionHandlerInvoker, BusinessExceptionHandlerInvoker>(new ContainerControlledLifetimeManager());
            //container.RegisterType<ICommandAuthorizationFailedInvoker, CommandAuthorizationFailedInvoker>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBusLicenseProvider, BusLicenseProvider>(new ContainerControlledLifetimeManager());
        }
    }
}
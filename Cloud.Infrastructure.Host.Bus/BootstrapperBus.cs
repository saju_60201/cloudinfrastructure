﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BootstrapperBus.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Cloud.Infrastructure.Host.Contracts;
using Cloud.Infrastructure.Logging.Contracts;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using NServiceBus;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The bootstrapper.
    /// </summary>
    public class BootstrapperBus : BootstrapperWebApi
    {
        private BusConfiguration busConfiguration;

        private bool isCommandServerInternal;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BootstrapperBus" /> class.
        /// </summary>
        /// <param name="enableTenantAwareness">
        ///     if set to <c>true</c> [enable tenant awareness].
        /// </param>
        public BootstrapperBus(bool enableTenantAwareness = false)
            : base(enableTenantAwareness)
        {
            Config.IsAutoRegisterUnityOfWork = false;
        }

        internal void RegisterUnitOfWorkTypes(bool isCommandServer, BusConfiguration configuration)
        {
            isCommandServerInternal = isCommandServer;
            busConfiguration = configuration;
            RegisterRequestUnitOfWorkTypes();

            Cloud.Infrastructure.Interception.ServiceLocator.Instance.GetRootContainer()
                .RegisterInstance<BusConfiguration>(busConfiguration, new ContainerControlledLifetimeManager());
        }

        /// <summary>
        ///     The register request unit of work types.
        /// </summary>
        /// <param name="container">
        ///     The container.
        /// </param>
        /// <param name="typesToRegister">
        ///     The types to register.
        /// </param>
        /// <param name="logger">
        ///     The logger.
        /// </param>
        protected override void RegisterRequestUnitOfWorkTypes(IUnityContainer container,
            IList<UnityTypeMapping> typesToRegister, ILogger logger)
        {           
            if (isCommandServerInternal)
            {
                logger.Debug(
                    () =>
                        "this is a commandserver. So the server should handle unitofwork using web request and not the bus container");
                base.RegisterRequestUnitOfWorkTypes(container, typesToRegister, logger);
                return;
            }

            foreach (var typeToRegister in typesToRegister)
            {
                logger.Debug(
                    () =>
                        string.Format("Register type {0} using {1}", typeToRegister.TypeTo.Name,
                            DependencyLifecycle.InstancePerUnitOfWork.GetType().Name));
                busConfiguration.RegisterComponents(
                    c => c.ConfigureComponent(typeToRegister.TypeTo, DependencyLifecycle.InstancePerUnitOfWork));
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityOfWorkUnityRegistrar.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Validation;
using Cloud.Infrastructure.Validation.Contracts;
using System;

namespace Cloud.Infrastructure.Host.Bus
{
    /// <summary>
    ///     The unity of work unity registrar.
    /// </summary>
    public class UnityOfWorkUnityRegistrar : IRequestUnitOfWorkUnityRegistrar
    {
        /// <summary>
        ///     Gets the types to register.
        /// </summary>
        /// <returns>The registrations.</returns>
        public Dictionary<Type, Type> GetTypesToRegister()
        {
            var types = new Dictionary<Type, Type>
            {
                //{ typeof(MasterDataTrigger), typeof(MasterDataTrigger) }, 
                //{ typeof(ISystemUserProvider), typeof(SystemUserProvider) },
                //{ typeof(ClaimsMessageHelper), typeof(ClaimsMessageHelper) },
                //{ typeof(RequestInfoHelper), typeof(RequestInfoHelper) },
                {typeof (IMessageValidationService), typeof (MessageValidationService)},
                { typeof(IMessageValidationDisabler), typeof(MessageValidationDisabler) },
                {typeof (IMessageValidator), typeof (MessageValidator)}
                //{ typeof(IAuthorizationService), typeof(AuthorizationService) }
            };

            return types;
        }
    }
}
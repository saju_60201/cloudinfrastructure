// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JsonCustomMessageSerializerBase.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NServiceBus.MessageInterfaces;
using NServiceBus.Serialization;

namespace Cloud.Infrastructure.Host.Bus.JsonTemp
{
    /// <summary>
    ///     JSON and BSON base class for <see cref="IMessageSerializer" />.
    /// </summary>
    public abstract class JsonCustomMessageSerializerBase : IMessageSerializer
    {
#pragma warning disable 618
        private readonly IMessageMapper messageMapper;
#pragma warning restore 618

        private readonly JsonSerializerSettings serializerSettings = new JsonSerializerSettings { TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple, TypeNameHandling = TypeNameHandling.Auto, Converters = { new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.RoundtripKind }, new XContainerConverter() } };

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonCustomMessageSerializerBase"/> class.
        /// </summary>
        /// <param name="messageMapper">
        /// The message mapper.
        /// </param>
        protected JsonCustomMessageSerializerBase(IMessageMapper messageMapper)
#pragma warning restore 618
        {
            this.messageMapper = messageMapper;
        }

        /// <summary>
        ///     Gets the content type into which this serializer serializes the content to
        /// </summary>
        public string ContentType
        {
            get
            {
                return this.GetContentType();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether skip array wrapping for single messages.
        /// </summary>
        public bool SkipArrayWrappingForSingleMessages { get; set; }

        /// <summary>
        /// Deserializes from the given stream a set of messages.
        /// </summary>
        /// <param name="stream">
        /// Stream that contains messages.
        /// </param>
        /// <param name="messageTypes">
        /// The list of message types to deserialize. If null the types must be inferred from the
        ///     serialized data.
        /// </param>
        /// <returns>
        /// Deserialized messages.
        /// </returns>
        public object[] Deserialize(Stream stream, IList<Type> messageTypes = null)
        {
            var settings = this.serializerSettings;

            var mostConcreteType = messageTypes != null ? messageTypes.FirstOrDefault() : null;
            var requiresDynamicDeserialization = mostConcreteType != null && mostConcreteType.IsInterface;

            //temporary fix
            //if (requiresDynamicDeserialization)
            //{
            //    settings = new JsonSerializerSettings { TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple, TypeNameHandling = TypeNameHandling.None, Converters = { new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.RoundtripKind }, new XContainerConverter() } };
            //}
            var jsonSerializer = JsonSerializer.Create(settings);
            jsonSerializer.ContractResolver = new MessageContractResolver(this.messageMapper);

            var reader = this.CreateJsonReader(stream);
            reader.Read();

            var firstTokenType = reader.TokenType;

            if (firstTokenType == JsonToken.StartArray)
            {
                if (requiresDynamicDeserialization)
                {
                    return (object[])jsonSerializer.Deserialize(reader, mostConcreteType.MakeArrayType());
                }

                return jsonSerializer.Deserialize<object[]>(reader);
            }

            if (messageTypes != null && messageTypes.Any())
            {
                return new[] { jsonSerializer.Deserialize(reader, messageTypes.First()) };
            }

            return new[] { jsonSerializer.Deserialize<object>(reader) };
        }

        /// <summary>
        /// Serializes the given set of messages into the given stream.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="stream">
        /// Stream for <paramref name="messages"/> to be serialized into.
        /// </param>
        public void Serialize(object message, Stream stream)
        {
            var jsonSerializer = JsonSerializer.Create(this.serializerSettings);
            jsonSerializer.Binder = new MessageSerializationBinder(this.messageMapper);

            var jsonWriter = this.CreateJsonWriter(stream);

            jsonSerializer.Serialize(jsonWriter, message);

            jsonWriter.Flush();
        }

        /// <summary>
        /// The create json reader.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="JsonReader"/>.
        /// </returns>
        protected abstract JsonReader CreateJsonReader(Stream stream);

        /// <summary>
        /// The create json writer.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="JsonWriter"/>.
        /// </returns>
        protected abstract JsonWriter CreateJsonWriter(Stream stream);

        /// <summary>
        ///     The get content type.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        protected abstract string GetContentType();
    }
}
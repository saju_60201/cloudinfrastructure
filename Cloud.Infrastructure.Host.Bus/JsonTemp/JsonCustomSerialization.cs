﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JsonCustomSerialization.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using NServiceBus;
using NServiceBus.Features;
using NServiceBus.MessageInterfaces.MessageMapper.Reflection;

namespace Cloud.Infrastructure.Host.Bus.JsonTemp
{
    /// <summary>
    ///     The json custom serialization.
    /// </summary>
    public class JsonCustomSerialization : Feature
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="JsonCustomSerialization" /> class.
        /// </summary>
        public JsonCustomSerialization()
        {
            this.EnableByDefault();
        }

        /// <summary>
        /// The setup.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        protected override void Setup(FeatureConfigurationContext context)
        {
            context.Container.ConfigureComponent<MessageMapper>(DependencyLifecycle.SingleInstance);
            context.Container.ConfigureComponent<JsonCustomMessageSerializer>(DependencyLifecycle.SingleInstance);
        }
    }
}
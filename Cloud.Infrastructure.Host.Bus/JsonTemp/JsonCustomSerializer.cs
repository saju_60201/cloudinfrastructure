﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JsonCustomSerializer.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using NServiceBus.Serialization;

namespace Cloud.Infrastructure.Host.Bus.JsonTemp
{
    /// <summary>
    ///     The json custom.
    /// </summary>
    public class JsonCustomSerializer : SerializationDefinition
    {
        /// <summary>
        ///     The provided by feature.
        /// </summary>
        /// <returns>
        ///     The <see cref="Type" />.
        /// </returns>
        protected override Type ProvidedByFeature()
        {
            return typeof(JsonCustomSerialization);
        }
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageContractResolver.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Newtonsoft.Json.Serialization;
using NServiceBus.MessageInterfaces;

namespace Cloud.Infrastructure.Host.Bus.JsonTemp
{
    /// <summary>
    ///     The message contract resolver.
    /// </summary>
    public class MessageContractResolver : DefaultContractResolver
    {
#pragma warning disable 618
        private readonly IMessageMapper messageMapper;
#pragma warning restore 618

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageContractResolver"/> class.
        /// </summary>
        /// <param name="messageMapper">
        /// The message mapper.
        /// </param>
        public MessageContractResolver(IMessageMapper messageMapper)
#pragma warning restore 618
            : base(true)
        {
            this.messageMapper = messageMapper;
        }

        /// <summary>
        /// The create object contract.
        /// </summary>
        /// <param name="objectType">
        /// The object type.
        /// </param>
        /// <returns>
        /// The <see cref="JsonObjectContract"/>.
        /// </returns>
        protected override JsonObjectContract CreateObjectContract(Type objectType)
        {
            var mappedTypeFor = this.messageMapper.GetMappedTypeFor(objectType);

            if (mappedTypeFor == null)
            {
                return base.CreateObjectContract(objectType);
            }

            var jsonContract = base.CreateObjectContract(mappedTypeFor);
            jsonContract.DefaultCreator = () => this.messageMapper.CreateInstance(mappedTypeFor);

            return jsonContract;
        }
    }
}
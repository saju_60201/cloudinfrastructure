// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JsonCustomMessageSerializer.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using NServiceBus;
using NServiceBus.MessageInterfaces;

namespace Cloud.Infrastructure.Host.Bus.JsonTemp
{
    /// <summary>
    ///     JSON message serializer.
    /// </summary>
    public class JsonCustomMessageSerializer : JsonCustomMessageSerializerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JsonCustomMessageSerializer"/> class.
        ///     Constructor.
        /// </summary>
        /// <param name="messageMapper">
        /// The message Mapper.
        /// </param>
        public JsonCustomMessageSerializer(IMessageMapper messageMapper)
#pragma warning restore 618
            : base(messageMapper)
        {
        }

        /// <summary>
        /// The deserialize object.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// object to deserialize
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T DeserializeObject<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }

        /// <summary>
        /// The deserialize object.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object DeserializeObject(string value, Type type)
        {
            return JsonConvert.DeserializeObject(value, type);
        }

        /// <summary>
        /// The serialize object.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SerializeObject(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// The create json reader.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="JsonReader"/>.
        /// </returns>
        protected override JsonReader CreateJsonReader(Stream stream)
        {
            var streamReader = new StreamReader(stream, Encoding.UTF8);
            return new JsonTextReader(streamReader);
        }

        /// <summary>
        /// The create json writer.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="JsonWriter"/>.
        /// </returns>
        protected override JsonWriter CreateJsonWriter(Stream stream)
        {
            var streamWriter = new StreamWriter(stream, Encoding.UTF8);
            return new JsonTextWriter(streamWriter) { Formatting = Formatting.None };
        }

        /// <summary>
        ///     The get content type.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        protected override string GetContentType()
        {
            return ContentTypes.Json;
        }
    }
}
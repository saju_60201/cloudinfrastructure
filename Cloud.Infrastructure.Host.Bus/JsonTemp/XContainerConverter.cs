// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XContainerConverter.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Cloud.Infrastructure.Host.Bus.JsonTemp
{
    /// <summary>
    ///     The x container converter.
    /// </summary>
    public class XContainerConverter : JsonConverter
    {
        /// <summary>
        /// The can convert.
        /// </summary>
        /// <param name="objectType">
        /// The object type.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(XContainer).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// The read json.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="objectType">
        /// The object type.
        /// </param>
        /// <param name="existingValue">
        /// The existing value.
        /// </param>
        /// <param name="serializer">
        /// The serializer.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// unexpected token
        /// </exception>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            if (reader.TokenType != JsonToken.String)
            {
                throw new Exception(string.Format(CultureInfo.InvariantCulture, "Unexpected token or value when parsing XContainer. Token: {0}, Value: {1}", reader.TokenType, reader.Value));
            }

            var value = (string)reader.Value;
            if (objectType == typeof(XDocument))
            {
                try
                {
                    return XDocument.Load(new StringReader(value));
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format(CultureInfo.InvariantCulture, "Error parsing XContainer string: {0}", reader.Value), ex);
                }
            }

            return XElement.Load(new StringReader(value));
        }

        /// <summary>
        /// The write json.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="serializer">
        /// The serializer.
        /// </param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
            }

            var container = (XContainer)value;

            writer.WriteValue(container.ToString(SaveOptions.DisableFormatting));
        }
    }
}
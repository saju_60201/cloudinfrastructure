﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageSerializationBinder.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using NServiceBus.MessageInterfaces;

namespace Cloud.Infrastructure.Host.Bus.JsonTemp
{
    /// <summary>
    ///     The message serialization binder.
    /// </summary>
    public class MessageSerializationBinder : SerializationBinder
    {
        #region Fields

        private readonly IMessageMapper messageMapper;

        private readonly IList<Type> messageTypes;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageSerializationBinder"/> class.
        /// </summary>
        /// <param name="messageMapper">
        /// The message mapper.
        /// </param>
        /// <param name="messageTypes">
        /// The message types.
        /// </param>
        public MessageSerializationBinder(IMessageMapper messageMapper, IList<Type> messageTypes = null)
        {
            this.messageMapper = messageMapper;
            this.messageTypes = messageTypes;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The bind to name.
        /// </summary>
        /// <param name="serializedType">
        /// The serialized type.
        /// </param>
        /// <param name="assemblyName">
        /// The assembly name.
        /// </param>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            var mappedType = this.messageMapper.GetMappedTypeFor(serializedType) ?? serializedType;

            assemblyName = null;
            typeName = mappedType.AssemblyQualifiedName;
        }

        /// <summary>
        /// The bind to type.
        /// </summary>
        /// <param name="assemblyName">
        /// The assembly name.
        /// </param>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/>.
        /// </returns>
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type resolved = null;
            if (this.messageTypes != null)
            {
                // usually the requested message types are provided, so this should be fast
                resolved = this.messageTypes.FirstOrDefault(t => t.Name.Contains(typeName));
            }

            if (resolved == null)
            {
                // if the type has been used before it should be resolvable like this
                resolved = Type.GetType(typeName);
            }

            if (resolved == null)
            {
                // if the type has not been used before, we need to find it brute force
                resolved = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(typeName)).FirstOrDefault(t => t != null);
            }

            return resolved;
        }

        #endregion
    }
}
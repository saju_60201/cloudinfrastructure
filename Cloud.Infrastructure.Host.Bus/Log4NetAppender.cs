// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log4NetAppender.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using log4net.Appender;
using log4net.Config;
using log4net.Core;

namespace Cloud.Infrastructure.Host.Bus
{
    using Logging.Contracts;
    using ILogger = Cloud.Infrastructure.Logging.Contracts.ILogger;

    /// <summary>
    ///     The log 4 net appender.
    /// </summary>
    public class Log4NetAppender : AppenderSkeleton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetAppender"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Logger must not be null
        /// </exception>
        public Log4NetAppender(ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.Logger = logger;
        }

        private ILogger Logger { get; set; }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public static void Initialize(ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            var appender = new Log4NetAppender(logger);

            BasicConfigurator.Configure(appender);
        }

        /// <summary>
        /// The append.
        /// </summary>
        /// <param name="loggingEvent">
        /// The logging event.
        /// </param>
        protected override void Append(LoggingEvent loggingEvent)
        {
            var logEvent = ConvertToLogging(loggingEvent);

            this.Logger.Log(logEvent);
        }

        private static LogLevel ConvertLevel(Level level)
        {
            if (level == Level.Info)
            {
                return Logging.Contracts.LogLevel.Info;
            }

            if (level == Level.Debug)
            {
                return Cloud.Infrastructure.Logging.Contracts.LogLevel.Debug;
            }

            if (level == Level.Error)
            {
                return Cloud.Infrastructure.Logging.Contracts.LogLevel.Error;
            }

            if (level == Level.Fatal)
            {
                return Cloud.Infrastructure.Logging.Contracts.LogLevel.Fatal;
            }

            if (level == Level.Trace)
            {
                return Cloud.Infrastructure.Logging.Contracts.LogLevel.Trace;
            }

            if (level == Level.Warn)
            {
                return Cloud.Infrastructure.Logging.Contracts.LogLevel.Warn;
            }

            throw new NotSupportedException(string.Format("Level {0} is currently not supported.", level));
        }

        private static LogEvent ConvertToLogging(LoggingEvent loggingEvent)
        {
            return new LogEvent
                       {
                           Exception = loggingEvent.ExceptionObject, 
                           LoggerName = loggingEvent.LoggerName, 
                           Message = Convert.ToString(loggingEvent.MessageObject), 
                           Level = ConvertLevel(loggingEvent.Level), 
                           TimeStamp = loggingEvent.TimeStamp
                       };
        }
    }
}
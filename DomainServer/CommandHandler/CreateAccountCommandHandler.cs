﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateAccountCommandHandler.cs" company="">
//   
// </copyright>
// <summary>
//   The create account command handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace DomainServer.CommandHandler
{
    using Cloud.Commands;
    using Cloud.Infrastructure.Bus;
    using Cloud.Infrastructure.EventStore;
    using Cloud.Infrastructure.Logging.Contracts;

    using DomainServer.Aggregate;

    using Microsoft.Practices.Unity;

    using NServiceBus;

    /// <summary>
    ///     The create account command handler.
    /// </summary>
    public class CreateAccountCommandHandler : IHandleMessages<CreateAccountCommand>
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        [Dependency]
        public ILogger Logger { get; set; }

        /// <summary>
        ///     Gets or sets the repository.
        /// </summary>
        [Dependency]
        public NES.Repository Repository { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The handle.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        //public override void Handle(CreateAccountCommand @command)
        //{
        //    this.Logger.Debug(() => string.Format("Got command with Id:{0}", @command.AccountId));

        //    var account = new Account(@command.AccountId, @command.AccountName, @command.AccountNumber, true);
        //    this.Repository.Add(account);
        //}

        #endregion

        public void Handle(CreateAccountCommand @command)
        {
            var account = new Account(@command.AccountId, @command.AccountName, @command.AccountNumber, true);
            this.Repository.Add(account);
        }
    }
}
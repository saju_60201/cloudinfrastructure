// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Amount.cs" company="">
//   
// </copyright>
// <summary>
//   The amount.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainServer.Aggregate
{
    /// <summary>
    /// The amount.
    /// </summary>
    public class Amount
    {
        #region Fields

        /// <summary>
        /// The _decimal amount.
        /// </summary>
        private readonly decimal _decimalAmount;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Amount"/> class.
        /// </summary>
        /// <param name="decimalAmount">
        /// The decimal amount.
        /// </param>
        public Amount(decimal decimalAmount)
        {
            this._decimalAmount = decimalAmount;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The op_ implicit.
        /// </summary>
        /// <param name="amount">
        /// The amount.
        /// </param>
        /// <returns>
        /// </returns>
        public static implicit operator decimal(Amount amount)
        {
            return amount._decimalAmount;
        }

        /// <summary>
        /// The op_ implicit.
        /// </summary>
        /// <param name="decimalAmount">
        /// The decimal amount.
        /// </param>
        /// <returns>
        /// </returns>
        public static implicit operator Amount(decimal decimalAmount)
        {
            return new Amount(decimalAmount);
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="amount">
        /// The amount.
        /// </param>
        /// <returns>
        /// The <see cref="Amount"/>.
        /// </returns>
        public Amount Add(Amount amount)
        {
            decimal newDecimalAmount = this._decimalAmount + amount._decimalAmount;
            return new Amount(newDecimalAmount);
        }

        /// <summary>
        /// The is negative.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsNegative()
        {
            return this._decimalAmount < 0;
        }

        /// <summary>
        /// The substract.
        /// </summary>
        /// <param name="amount">
        /// The amount.
        /// </param>
        /// <returns>
        /// The <see cref="Amount"/>.
        /// </returns>
        public Amount Substract(Amount amount)
        {
            decimal newDecimalAmount = this._decimalAmount - amount._decimalAmount;
            return new Amount(newDecimalAmount);
        }

        #endregion
    }
}
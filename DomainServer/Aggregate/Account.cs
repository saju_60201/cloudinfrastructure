﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Account.cs" company="">
//   
// </copyright>
// <summary>
//   The account.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainServer.Aggregate
{
    using System;

    using Cloud.Events;

    using NES;

    /// <summary>
    /// The account.
    /// </summary>
    public class Account : AggregateBase<Guid>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Account"/> class.
        /// </summary>
        /// <param name="accountId">
        /// The account id.
        /// </param>
        /// <param name="accountName">
        /// The account name.
        /// </param>
        /// <param name="accountNumber">
        /// The account number.
        /// </param>
        /// <param name="isActive">
        /// The is active.
        /// </param>
        public Account(Guid accountId, string accountName, string accountNumber, bool isActive)
        {
            Apply<IAccountCreatedEvent>(
                e =>
                    {
                        e.AccountId = accountId;
                        e.AccountName = accountName;
                        e.AccountNumber = accountNumber;
                        e.IsActive = isActive;
                    });
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the account name.
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Gets or sets the balance.
        /// </summary>
        public Balance Balance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active.
        /// </summary>
        public bool IsActive { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The handle.
        /// </summary>
        /// <param name="event">
        /// The event.
        /// </param>
        private void Handle(IAccountCreatedEvent @event)
        {
            this.Id = @event.AccountId;
            this.AccountName = @event.AccountName;
            this.AccountNumber = @event.AccountNumber;
            this.Balance = new Balance();
            this.IsActive = @event.IsActive;
        }

        #endregion
    }
}
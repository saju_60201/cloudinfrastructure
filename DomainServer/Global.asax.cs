﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Cloud.Infrastructure.Host;
using Cloud.Infrastructure.Host.Bus;
using Cloud.Infrastructure.Host.Bus.EventStore;

namespace DomainServer
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);          
            var bootstrapper = 
                new BootstrapperDomain()
                .Initialize()
                .EnableGlobalExceptionHandler()
                .EnableJsonTypeNameHandling()
                .EnableLogging()
                .SetSerializePrivateMembers()
                .ConfigureNsbBus(enpointName: ConfigurationManager.AppSettings["endpointname"], isAutoSubscribe: true)
                .StartNsbBus(doInstall: true)
                .EventStoreClassic("EventStore");
        }
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="">
//   
// </copyright>
// <summary>
//   The home controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainServer.Controllers
{
    using System.Web.Http;

    /// <summary>
    /// The home controller.
    /// </summary>
    public class HomeController : ApiController
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Get()
        {
            return "Domain Server Initiated";
        }

        #endregion
    }
}
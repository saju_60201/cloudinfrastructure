﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BusinessException.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Cloud.Infrastructure.Localization.Contracts;

namespace Cloud.Infrastructure.Exceptions
{
    /// <summary>
    ///     The business exception.
    /// </summary>
    public abstract class BusinessException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        /// </summary>
        /// <param name="exceptionId">
        /// The exception id.
        /// </param>
        /// <param name="resourceCode">
        /// The resource code.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected BusinessException(Guid exceptionId, ILocalizationResource resourceCode, params object[] arguments)
            : base(resourceCode.GetString(arguments))
        {
            this.ExceptionId = exceptionId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessException"/> class.
        /// </summary>
        /// <param name="resourceCode">
        /// The resource code.
        /// </param>
        /// <param name="innerException">
        /// The inner exception.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected BusinessException(ILocalizationResource resourceCode, Exception innerException, params object[] arguments)
            : base(resourceCode.GetString(arguments), innerException)
        {
        }

        /// <summary>
        ///     Gets the exception id.
        /// </summary>
        public Guid ExceptionId { get; private set; }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandValidationException.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Exceptions
{
    /// <summary>
    ///     The command validation exception.
    /// </summary>
    public class CommandValidationException : ValidationException
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandValidationException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="validationResult">
        /// The validation result.
        /// </param>
        public CommandValidationException(string message, object validationResult) : base(message, validationResult)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandValidationException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="innerException">
        /// The inner exception.
        /// </param>
        /// <param name="validationResult">
        /// The validation Result.
        /// </param>
        public CommandValidationException(string message, Exception innerException, object validationResult)
            : base(message, innerException, validationResult)
        {
        }

        #endregion
    }
}
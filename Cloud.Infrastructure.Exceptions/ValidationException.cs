﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidationException.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.Exceptions
{
    /// <summary>
    ///     The validation exception.
    /// </summary>
    public abstract class ValidationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="innerException">
        /// The inner exception.
        /// </param>
        /// <param name="validationResult">
        /// The validation result.
        /// </param>
        protected ValidationException(string message, Exception innerException, object validationResult) : base(message, innerException)
        {
            this.ValidationResult = validationResult;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="validationResult">
        /// The validation result.
        /// </param>
        protected ValidationException(string message, object validationResult) : base(message)
        {
            this.ValidationResult = validationResult;
        }

        /// <summary>
        ///     Gets the validation result.
        /// </summary>
        public object ValidationResult { get; private set; }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            if (this.ValidationResult != null)
            {
                return this.ValidationResult.ToString();
            }

            return base.ToString();
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguagePropertiesODataSerializerProvider.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The my o data serializer provider.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Web.Http.Dependencies;
using System.Web.Http.OData.Formatter.Serialization;
using Cloud.Infrastracture.Caching.Contracts;
using Cloud.Infrastructure.Localization.Contracts;
using Cloud.Infrastructure.Logging.Contracts;
using Microsoft.Data.Edm;

namespace Cloud.Infrastructure.ODataMultilanguage
{
    /// <summary>
    ///     The my o data serializer provider.
    /// </summary>
    public class LanguagePropertiesODataSerializerProvider : DefaultODataSerializerProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguagePropertiesODataSerializerProvider"/> class.
        /// </summary>
        /// <param name="dependencyResolver">
        /// The dependency Resolver.
        /// </param>
        /// <param name="columnsCache">
        /// cache provider
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public LanguagePropertiesODataSerializerProvider(IDependencyResolver dependencyResolver, ICache columnsCache, ILogger logger)
        {
            this.DependencyResolver = dependencyResolver;
            this.Logger = logger;
            this.CacheProvider = columnsCache;
        }

        /// <summary>
        /// Gets or sets the cache provider.
        /// </summary>
        protected ICache CacheProvider { get; set; }

        /// <summary>
        ///     Gets or sets the current culture.
        /// </summary>
        private IDependencyResolver DependencyResolver { get; set; }

        private ILogger Logger { get; set; }

        /// <summary>
        /// The get edm type serializer.
        /// </summary>
        /// <param name="edmType">
        /// The edm type.
        /// </param>
        /// <returns>
        /// The <see cref="ODataEdmTypeSerializer"/>.
        /// </returns>
        public override ODataEdmTypeSerializer GetEdmTypeSerializer(IEdmTypeReference edmType)
        {
            if (this.Logger != null)
            {
                this.Logger.Debug(() => string.Format("Edm type is {0}", edmType.Definition.TypeKind));
            }

            if (edmType.IsEntity())
            {
                if (this.Logger != null)
                {
                    this.Logger.Debug(() => "Return custom LanguagePropertiesODataEntityTypeSerializer");
                }

                return new LanguagePropertiesODataEntityTypeSerializer(this, (ILanguageSupport)this.DependencyResolver.GetService(typeof(ILanguageSupport)), this.CacheProvider, this.Logger);
            }

            return base.GetEdmTypeSerializer(edmType);
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguagePropertiesQueryableAttribute.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The custom queryable attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http.Dependencies;
using System.Web.Http.Hosting;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using Cloud.Infrastracture.Caching.Contracts;
using Cloud.Infrastructure.Localization.Contracts;
using Cloud.Infrastructure.Logging.Contracts;

namespace Cloud.Infrastructure.ODataMultilanguage
{
    /// <summary>
    ///     The custom queryable attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class LanguagePropertiesQueryableAttribute : EnableQueryAttribute
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LanguagePropertiesQueryableAttribute" /> class.
        /// </summary>
        /// <param name="dependencyResolver">
        ///     The dependency Resolver.
        /// </param>
        /// <param name="columnsCache">
        ///     for storing multilanguage columns list
        /// </param>
        /// <param name="logger">
        ///     The logger.
        /// </param>
        public LanguagePropertiesQueryableAttribute(IDependencyResolver dependencyResolver, ICache columnsCache,
            ILogger logger)
        {
            DependencyResolver = dependencyResolver;

            Logger = logger;
            LanguageHelper = new LanguageHelper(columnsCache,
                (ILanguageSupport) DependencyResolver.GetService(typeof (ILanguageSupport)));
        }

        /// <summary>
        ///     Gets or sets the dependency resolver.
        /// </summary>
        public IDependencyResolver DependencyResolver { get; set; }

        /// <summary>
        ///     Gets or sets the current culture.
        /// </summary>
        /// <summary>
        ///     Gets or sets the language handler.
        /// </summary>
        public LanguageHelper LanguageHelper { get; set; }

        private ILogger Logger { get; set; }

        /// <summary>
        ///     The apply query.
        /// </summary>
        /// <param name="queryable">
        ///     The queryable.
        /// </param>
        /// <param name="queryOptions">
        ///     The query options.
        /// </param>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        public override IQueryable ApplyQuery(IQueryable queryable, ODataQueryOptions queryOptions)
        {
            var languageSupport = (ILanguageSupport) DependencyResolver.GetService(typeof (ILanguageSupport));
            // generate new request uri
            var newRequestUri = GetNewRequestUri(queryOptions, languageSupport.GetCurrentLanguage().CultureInfo);
            if (Logger != null)
            {
                Logger.Debug(() => string.Format("Apply query for {0}", newRequestUri));
            }

            // if nothing has changed process with original queryoptions
            if (newRequestUri.Equals(queryOptions.Request.RequestUri))
            {
                return base.ApplyQuery(queryable, queryOptions);
            }

            // set the new uri
            queryOptions.Request.RequestUri = newRequestUri;
            queryOptions.Request.Properties.Remove(HttpPropertyKeys.RequestQueryNameValuePairsKey);
            queryOptions.Request.GetQueryNameValuePairs();

            var newQueryOptions = new ODataQueryOptions(queryOptions.Context, queryOptions.Request);

            return base.ApplyQuery(queryable, newQueryOptions);
        }

        private string ChangeColumnNamesToMultilanguage(
            ODataQueryOptions queryOptions,
            string origColumnNames,
            CultureInfo targetCultureInfo)
        {
            if (string.IsNullOrEmpty(origColumnNames))
            {
                return string.Empty;
            }

            var colMultilanguage = LanguageHelper.GetMultiLanguageProperties(queryOptions);
            if (colMultilanguage.Count == 0)
            {
                return origColumnNames;
            }

            var colOrig = Regex.Split(origColumnNames, @"(,|\s|'|\(|\))", RegexOptions.Compiled);

            for (var i = 0; i < colOrig.Length; i++)
            {
                var lookupName = colOrig[i] + "_" + targetCultureInfo.TwoLetterISOLanguageName;
                var foundCol =
                    colMultilanguage.FirstOrDefault(
                        c => c.Equals(lookupName, StringComparison.InvariantCultureIgnoreCase));
                if (foundCol != null)
                {
                    colOrig[i] = foundCol;
                }
            }

            return string.Join(string.Empty, colOrig);
        }

        private Uri GetNewRequestUri(ODataQueryOptions queryOptions, CultureInfo targetCultureInfo)
        {
            var uri = queryOptions.Request.RequestUri;
            if (uri == null || string.IsNullOrEmpty(uri.Query))
            {
                return uri;
            }

            var query = string.IsNullOrEmpty(uri.Query) ? string.Empty : uri.Query;
            var firstPart = uri.AbsoluteUri.Substring(0, uri.AbsoluteUri.Length - query.Length);

            var queryParts = uri.ParseQueryString();

            // change the orderby
            if (queryParts["$orderby"] != null)
            {
                var newOrderBy = ChangeColumnNamesToMultilanguage(queryOptions, queryOptions.RawValues.OrderBy,
                    targetCultureInfo);
                queryParts["$orderby"] = newOrderBy;
            }

            // change the filter
            if (queryParts["$filter"] != null)
            {
                var newFilter = ChangeColumnNamesToMultilanguage(queryOptions, queryOptions.RawValues.Filter,
                    targetCultureInfo);
                queryParts["$filter"] = newFilter;
            }

            // change the expand
            if (queryParts["$expand"] != null)
            {
                var newExpand = ChangeColumnNamesToMultilanguage(queryOptions, queryOptions.RawValues.Expand,
                    targetCultureInfo);
                queryParts["$expand"] = newExpand;
            }

            // change the select verb
            if (queryParts["$select"] != null)
            {
                var newExpand = ChangeColumnNamesToMultilanguage(queryOptions, queryOptions.RawValues.Select,
                    targetCultureInfo);
                queryParts["$select"] = newExpand;
            }

            var resultUri = firstPart + "?";
            for (var i = 0; i < queryParts.Count; i++)
            {
                resultUri = resultUri + (i == 0 ? string.Empty : "&") + queryParts.GetKey(i) + "=" + queryParts[i];
            }

            return new Uri(resultUri);
        }
    }
}
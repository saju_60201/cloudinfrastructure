﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Web.Http;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Formatter;
using System.Web.Http.OData.Formatter.Deserialization;
using Cloud.Infrastracture.Caching;
using Cloud.Infrastracture.Caching.Contracts;
using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Logging.Contracts;

namespace Cloud.Infrastructure.ODataMultilanguage
{
    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// The enable query support multilaguage.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        /// <param name="serviceLocator">
        /// The service Locator.
        /// </param>
        [Obsolete("Use AddODataQueryFilter")]
        public static void EnableQuerySupportMultilaguage(this HttpConfiguration configuration, IServiceLocator serviceLocator = null)
        {
            configuration.AddODataQueryFilter(serviceLocator);
        }

        /// <summary>
        /// The add o data query filter.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        /// <param name="serviceLocator">
        /// The service locator.
        /// </param>
        public static void AddODataQueryFilter(this HttpConfiguration configuration, IServiceLocator serviceLocator = null)
        {
            //var languageSupport = (ILanguageSupport)configuration.DependencyResolver.GetService(typeof(ILanguageSupport));
            var cacheProvider = configuration.DependencyResolver.GetService(typeof(ICache)) as ICache ?? CachingFactory.Instance.CreateCache("ColumnsContainers");
            var logger = (ILogger)configuration.DependencyResolver.GetService(typeof(ILogger));

            var customFormatters = ODataMediaTypeFormatters.Create(
                new LanguagePropertiesODataSerializerProvider(configuration.DependencyResolver, cacheProvider, logger),
                new DefaultODataDeserializerProvider());
            configuration.Formatters.InsertRange(0, customFormatters);
            configuration.AddODataQueryFilter(new LanguagePropertiesQueryableAttribute(configuration.DependencyResolver, cacheProvider, logger));
        }
    }
}
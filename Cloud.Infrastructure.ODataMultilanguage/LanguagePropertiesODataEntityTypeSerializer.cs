﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguagePropertiesODataEntityTypeSerializer.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.ODataMultilanguage
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Http.OData;
    using System.Web.Http.OData.Formatter.Serialization;

    using Cloud.Infrastracture.Caching.Contracts;
    using Cloud.Infrastructure.Localization.Contracts;
    using Cloud.Infrastructure.Logging.Contracts;

    using Microsoft.Data.Edm;
    using Microsoft.Data.OData;

    /// <summary>
    ///     The custom o data entity type serializer.
    /// </summary>
    public class LanguagePropertiesODataEntityTypeSerializer : ODataEntityTypeSerializer
    {
        #region Static Fields

        /// <summary>
        /// The columns cache.
        /// </summary>
        private static ICache columnsCache;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguagePropertiesODataEntityTypeSerializer"/> class.
        /// </summary>
        /// <param name="serializerProvider">
        /// The serializer provider.
        /// </param>
        /// <param name="languageSupport">
        /// The current Culture.
        /// </param>
        /// <param name="columnsCache">
        /// cache for storing multilanguage columns
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public LanguagePropertiesODataEntityTypeSerializer(
            ODataSerializerProvider serializerProvider, 
            ILanguageSupport languageSupport, 
            ICache columnsCache, 
            ILogger logger)
            : base(serializerProvider)
        {
            this.LanguageSupport = languageSupport;
            this.Logger = logger;
            this.LanguageHelper = new LanguageHelper(columnsCache, this.LanguageSupport);
            LanguagePropertiesODataEntityTypeSerializer.columnsCache = columnsCache;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the language handler.
        /// </summary>
        public LanguageHelper LanguageHelper { get; set; }

        /// <summary>
        ///     Gets or sets the current culture.
        /// </summary>
        public ILanguageSupport LanguageSupport { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        private ILogger Logger { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create entry.
        /// </summary>
        /// <param name="selectExpandNode">
        /// The select expand node.
        /// </param>
        /// <param name="entityInstanceContext">
        /// The entity instance context.
        /// </param>
        /// <returns>
        /// The <see cref="ODataEntry"/>.
        /// </returns>
        public override ODataEntry CreateEntry(SelectExpandNode selectExpandNode, EntityInstanceContext entityInstanceContext)
        {
            Type entityType = entityInstanceContext.EntityInstance.GetType();

            if (this.Logger != null)
            {
                this.Logger.Debug(() => entityType != null ? string.Format("Custom CreateEntry for {0}", entityType.FullName) : null);
            }

            var baseType = entityInstanceContext.EntityInstance.GetType().BaseType;
            string cacheKey;
            string cultureKey = this.LanguageSupport.GetCurrentLanguage().CultureInfo.TwoLetterISOLanguageName;

            if (baseType != null && typeof(object) != baseType)
            {
                cacheKey = baseType.FullName + "_" + entityType.FullName + "_CreateEntryForSelectedNode_"
                           + entityInstanceContext.Url.Request.RequestUri + "_" + cultureKey;
            }
            else
            {
                cacheKey = entityType.FullName + "_CreateEntryForSelectedNode_" + entityInstanceContext.Url.Request.RequestUri + "_"
                           + cultureKey;
            }

            if (!columnsCache.Contains(cacheKey))
            {
                this.RemoveNotRequiredLanguageProperties(
                    entityType, 
                    selectExpandNode.SelectedStructuralProperties, 
                    this.LanguageSupport.GetCurrentLanguage().CultureInfo);

                columnsCache.Add(cacheKey, selectExpandNode);
            }
            else
            {
                selectExpandNode = columnsCache.Get(cacheKey) as SelectExpandNode;
            }

            var entry = base.CreateEntry(selectExpandNode, entityInstanceContext);
            this.RenameTargetLanguageProperties(entry.Properties, this.LanguageSupport.GetCurrentLanguage().CultureInfo);

            return entry;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The define search pattern.
        /// </summary>
        /// <param name="targetCultureInfo">
        /// The target culture info.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string DefineSearchPattern(CultureInfo targetCultureInfo)
        {
            return string.Format("_{0}", targetCultureInfo.TwoLetterISOLanguageName);
        }

        /// <summary>
        /// The find property base name.
        /// </summary>
        /// <param name="languagePropertyName">
        /// The language property name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string FindPropertyBaseName(string languagePropertyName)
        {
            return languagePropertyName.Substring(0, languagePropertyName.Length - 3);
        }

        /// <summary>
        /// The remove not required language properties.
        /// </summary>
        /// <param name="entityType">
        /// The entity type.
        /// </param>
        /// <param name="structuralProperties">
        /// The structural properties.
        /// </param>
        /// <param name="targetCultureInfo">
        /// The target culture info.
        /// </param>
        private void RemoveNotRequiredLanguageProperties(
            Type entityType, 
            ISet<IEdmStructuralProperty> structuralProperties, 
            CultureInfo targetCultureInfo)
        {
            // copy the list
            var props = structuralProperties.ToList();

            // search the properties we must keep
            var searchEndPattern = this.DefineSearchPattern(targetCultureInfo);
            var toPreserve = new List<IEdmStructuralProperty>();

            foreach (var edmStructuralProperty in props)
            {
                var languageMarkedPropertyExists = this.LanguageHelper.IsLanguageProperty(entityType, edmStructuralProperty)
                                                   && edmStructuralProperty.Name.EndsWith(
                                                       searchEndPattern, 
                                                       StringComparison.InvariantCultureIgnoreCase);

                if (languageMarkedPropertyExists)
                {
                    toPreserve.Add(edmStructuralProperty);
                }
            }

            // remove other not needed language properties
            foreach (var odataProperty in props)
            {
                if (this.LanguageHelper.IsLanguageProperty(entityType, odataProperty) && !toPreserve.Contains(odataProperty))
                {
                    structuralProperties.Remove(odataProperty);
                }
            }

            foreach (var edmStructuralProperty in toPreserve)
            {
                var baseName = this.FindPropertyBaseName(edmStructuralProperty.Name);
                var baseProperty = props.FirstOrDefault(p => p.Name == baseName);
                if (baseProperty != null)
                {
                    structuralProperties.Remove(baseProperty);
                }
            }
        }

        /// <summary>
        /// The rename target language properties.
        /// </summary>
        /// <param name="props">
        /// The props.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture info.
        /// </param>
        private void RenameTargetLanguageProperties(IEnumerable<ODataProperty> props, CultureInfo cultureInfo)
        {
            if (this.Logger != null)
            {
                this.Logger.Debug(() => "Rename some properties");
            }

            var searchEndPattern = string.Format("_{0}", cultureInfo.TwoLetterISOLanguageName);
            var languageProperties =
                props.Where(p => p.Name.EndsWith(searchEndPattern, StringComparison.InvariantCultureIgnoreCase)).ToList();
            foreach (var languageProperty in languageProperties)
            {
                if (this.Logger != null)
                {
                    this.Logger.Debug(() => string.Format("Rename {0}", languageProperty.Name));
                }

                languageProperty.Name = this.FindPropertyBaseName(languageProperty.Name);

                if (this.Logger != null)
                {
                    this.Logger.Debug(() => string.Format("New name is {0}", languageProperty.Name));
                }
            }
        }

        #endregion
    }
}
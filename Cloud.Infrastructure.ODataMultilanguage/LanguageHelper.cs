﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguageHelper.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.ODataMultilanguage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http.OData.Query;

    using Cloud.Infrastracture.Caching.Contracts;
    using Cloud.Infrastructure.Localization.Contracts;
    using Cloud.Infrastructure.ODataAttributes;

    using Microsoft.Data.Edm;

    /// <summary>
    ///     The language handler.
    /// </summary>
    public class LanguageHelper
    {
        #region Static Fields

        /// <summary>
        /// The columns cache.
        /// </summary>
        private static ICache columnsCache;

        #endregion

        #region Fields

        /// <summary>
        /// The language support.
        /// </summary>
        private readonly ILanguageSupport languageSupport;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageHelper"/> class.
        /// </summary>
        /// <param name="columnsCache">
        /// The customer Cache.
        /// </param>
        /// <param name="languageSupport">
        /// The current Culture Provider.
        /// </param>
        public LanguageHelper(ICache columnsCache, ILanguageSupport languageSupport)
        {
            LanguageHelper.columnsCache = columnsCache;
            this.languageSupport = languageSupport;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets multi language properties.
        /// </summary>
        /// <param name="queryOptions">
        /// The query options.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> GetMultiLanguageProperties(ODataQueryOptions queryOptions)
        {
            var columns = LanguageHelper.columnsCache.Get(queryOptions.Context.ElementClrType.FullName) as List<string>;
            if (columns != null)
            {
                return columns;
            }

            Type entityType = queryOptions.Context.ElementClrType;

            var structuralProperties = ((IEdmEntityType)queryOptions.Context.ElementType).StructuralProperties();

            columns =
                structuralProperties.Select(edmStructuralProperty => edmStructuralProperty)
                    .Where(structuralProperty => this.IsLanguageProperty(entityType, structuralProperty))
                    .Select(e => e.Name)
                    .ToList();

            LanguageHelper.columnsCache.Add(queryOptions.Context.ElementClrType.FullName, columns);
            return columns;
        }

        /// <summary>
        /// Checks if it is a language property.
        /// </summary>
        /// <param name="entityType">
        /// The entity type.
        /// </param>
        /// <param name="edmProperty">
        /// The edm property.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsLanguageProperty(Type entityType, IEdmStructuralProperty edmProperty)
        {
            bool isLanguageProperty = false;

            var baseType = entityType.BaseType;

            string cacheKey = baseType != null && typeof(object) != baseType ? baseType.FullName : entityType.FullName;

            var languageColumns = LanguageHelper.columnsCache.Get(cacheKey) as List<string>;
            if (languageColumns != null && languageColumns.Contains(edmProperty.Name))
            {
                return true;
            }

            if (languageColumns != null && !languageColumns.Contains(edmProperty.Name))
            {
                return false;
            }

            List<string> props =
                entityType.GetProperties()
                    .Select(propertyInfo => propertyInfo)
                    .Where(column => column.GetCustomAttributes(typeof(LanguageAttribute), false).Any())
                    .Select(e => e.Name)
                    .ToList();

            if (props.Contains(edmProperty.Name))
            {
                isLanguageProperty = true;
                LanguageHelper.columnsCache.Add(cacheKey, props);
            }

            return isLanguageProperty;
        }

        #endregion
    }
}
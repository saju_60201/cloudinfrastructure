﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ClientCommandServer
{
    using System.Configuration;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    using Cloud.Infrastructure.Host;
    using Cloud.Infrastructure.Host.Bus;

    /// <summary>
    /// The web api application.
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        #region Methods

        /// <summary>
        /// The application_ start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            var bootstrapper = new BootstrapperBus()
                .Initialize()
                .EnableLogging() 
                .EnableGlobalExceptionHandler()
                .EnableJsonTypeNameHandling()
                .SetSerializePrivateMembers()
                .ConfigureNsbBus(enpointName: ConfigurationManager.AppSettings["endpointname"], isAutoSubscribe: true)
                .StartNsbBus(doInstall: true);
        }

        #endregion
    }
}
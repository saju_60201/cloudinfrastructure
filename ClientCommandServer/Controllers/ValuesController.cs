﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValuesController.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Web.Http;
using Cloud.Commands;
using Cloud.Infrastructure.Logging.Contracts;
using NServiceBus;

namespace ClientCommandServer.Controllers
{
    /// <summary>
    ///     The values controller.
    /// </summary>
    public class ValuesController : ApiController
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ValuesController" /> class.
        /// </summary>
        /// <param name="logger">
        ///     The logger.
        /// </param>
        public ValuesController(ILogger logger, IBus bus)
        {
            logger.Debug(() => "Test Log");
            var createAccountCommand = new CreateAccountCommand("Shahjahan");
            bus.Send(createAccountCommand);
        }

        #endregion

        // DELETE api/values/5

        #region Public Methods and Operators

        /// <summary>
        ///     The delete.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        public void Delete(int id)
        {
        }

        // GET api/values
        /// <summary>
        ///     The get.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public IEnumerable<string> Get()
        {
            return new[] {"value1", "value2"};
        }

        // GET api/values/5
        /// <summary>
        ///     The get.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        /// <summary>
        ///     The post.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        /// <summary>
        ///     The put.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        public void Put(int id, [FromBody] string value)
        {
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventHandlerBase.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus
{
    using Cloud.Infrastructure.Bus.Contracts;

    /// <summary>
    /// The event handler base.
    /// </summary>
    /// <typeparam name="T">
    /// A class which implements IEvent
    /// </typeparam>
    public abstract class EventHandlerBase<T> : MessageHandlerBase<T>
        where T : IEventBase
    {
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BusExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus
{
    using System;

    using Cloud.Infrastructure.Bus.Contracts;

    using NServiceBus;

    /// <summary>
    ///     The bus extensions.
    /// </summary>
    public static class BusExtensions
    {
        #region Static Fields

        /// <summary>
        /// The current logical message being handled.
        /// </summary>
        [ThreadStatic]
        private static object currentLogicalMessageBeingHandled;

        /// <summary>
        /// The current transport message being handled.
        /// </summary>
        [ThreadStatic]
        private static object currentTransportMessageBeingHandled;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the current logical message being handled.
        /// </summary>
        public static object CurrentLogicalMessageBeingHandled
        {
            get
            {
                return currentLogicalMessageBeingHandled ?? ExtensionMethods.CurrentMessageBeingHandled;
            }

            set
            {
                currentLogicalMessageBeingHandled = value;
            }
        }

        /// <summary>
        ///     Gets or sets the current transport message being handled.
        /// </summary>
        public static object CurrentTransportMessageBeingHandled
        {
            get
            {
                return currentTransportMessageBeingHandled;
            }

            set
            {
                currentTransportMessageBeingHandled = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The copy common properties.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="target">
        /// The target.
        /// </param>
        public static void CopyCommonProperties(this ICommandEventCommon source, ICommandEventCommon target)
        {
            CopyCommonPropertiesToTarget(source, target);
        }

        /// <summary>
        /// Disable command and state validation for this command. Is used often for datamigration
        /// </summary>
        /// <param name="command">
        /// The command which must not be validated
        /// </param>
        /// <param name="bus">
        /// The bus.
        /// </param>
        public static void DisableValidation(this object command, IBus bus)
        {
            var message = command as IMessage;
            if (message == null)
            {
                return;
            }

            bus.SetMessageHeader(message, MessageHeaders.IgnoreValidation, "1");
        }

        /// <summary>
        /// TransportMessageHelpers copied code
        /// </summary>
        /// <param name="message">
        /// message 
        /// </param>
        /// <returns>
        /// number of retries
        /// </returns>
        public static int GetNumberOfRetries(this TransportMessage message)
        {
            string value;
            if (message.Headers.TryGetValue(Headers.Retries, out value))
            {
                int i;
                if (int.TryParse(value, out i))
                {
                    return i;
                }
            }

            return 0;
        }

        /// <summary>
        /// The is validation disabled.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="bus">
        /// The bus.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsValidationDisabled(this object command, IBus bus)
        {
            var message = command as IMessage;
            if (message == null)
            {
                return false;
            }

            var ignoreFlag = bus.GetMessageHeader(message, MessageHeaders.IgnoreValidation);
            return ignoreFlag == "1";
        }

        #endregion

        #region Methods

        /// <summary>
        /// The copy common properties to target.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="target">
        /// The target.
        /// </param>
        private static void CopyCommonPropertiesToTarget(ICommandEventCommon source, ICommandEventCommon target)
        {
            target.TrackingId = source.TrackingId;
            target.ClientUtcTime = source.ClientUtcTime;
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandHandlerDomain.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus
{
    using Microsoft.Practices.Unity;

    using NES.Contracts;

    /// <summary>
    /// The command handler domain.
    /// </summary>
    /// <typeparam name="T">
    /// type of the command
    /// </typeparam>
    public abstract class CommandHandlerDomain<T> : CommandHandlerBase<T>
        where T : CommandBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the repository.
        /// </summary>
        [Dependency]
        public IRepository Repository { get; set; }

        #endregion
    }
}
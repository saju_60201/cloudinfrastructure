﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventHandlerDomain.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus
{
    using Cloud.Infrastructure.Bus.Contracts;

    using Microsoft.Practices.Unity;

    using NES.Contracts;

    /// <summary>
    /// The event handler domain.
    /// </summary>
    /// <typeparam name="T">
    /// Type of the event
    /// </typeparam>
    public abstract class EventHandlerDomain<T> : MessageHandlerBase<T>
        where T : IEventBase
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the repository.
        /// </summary>
        [Dependency]
        public IRepository Repository { get; set; }

        #endregion
    }
}
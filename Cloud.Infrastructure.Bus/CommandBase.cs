﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandBase.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Cloud.Infrastructure.Bus.Contracts;

    using NES;

    using NServiceBus;

    /// <summary>
    ///     The command base.
    /// </summary>
    [KnownType("GetKnownTypes")]
    public abstract class CommandBase : ICommand, ICommandEventCommon
    {
        #region Fields

        /// <summary>
        ///     The _id.
        /// </summary>
        private readonly Guid id = GuidComb.NewGuidComb();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the client utc time.
        /// </summary>
        public DateTime ClientUtcTime { get; set; }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public Guid Id
        {
            get
            {
                return this.id;
            }
        }

        /// <summary>
        ///     Gets or sets the tenant id.
        /// </summary>
        public Guid TenantId { get; set; }

        /// <summary>
        ///     Gets or sets the tracking id.
        /// </summary>
        public Guid TrackingId { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get known types.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public static IEnumerable<Type> GetKnownTypes()
        {
            return CurrentDomainTypes.GetTypesDerivingFrom<CommandBase>();
        }

        #endregion
    }
}
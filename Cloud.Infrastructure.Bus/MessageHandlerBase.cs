// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageHandlerBase.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus
{
    using System;
    using System.Diagnostics;

    using Cloud.Infrastructure.Interception;
    using Cloud.Infrastructure.Logging.Contracts;

    using Microsoft.Practices.Unity;

    using NServiceBus;
    using NServiceBus.Config;

    /// <summary>
    /// The message handler base.
    /// </summary>
    /// <typeparam name="T">
    /// Type of the message
    /// </typeparam>
    public abstract class MessageHandlerBase<T> : IHandleMessages<T>, IDisposable
        where T : IMessage
    {
        #region Fields

        /// <summary>
        /// The disposed.
        /// </summary>
        private bool disposed = false;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the bus.
        /// </summary>
        [Dependency]
        public IBus Bus { get; set; }

        /// <summary>
        ///     Gets or sets the dependency resolver.
        /// </summary>
        [Dependency]
        public IDependencyResolver DependencyResolver { get; set; }

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        [Dependency]
        public ILogger Logger { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The handle.
        /// </summary>
        /// <param name="event">
        /// The event.
        /// </param>
        public abstract void Handle(T @event);

        #endregion

        #region Explicit Interface Methods

        /// <summary>
        /// The handle.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void IHandleMessages<T>.Handle(T message)
        {
            Stopwatch sw = null;

            try
            {
                if (this.Logger.IsLoggingEnabled())
                {
                    sw = new Stopwatch();
                    sw.Start();
                }

                this.Logger.Debug(
                    () => string.Format("Start handling the message {0} {1}", message.GetType(), message), 
                    this.GetType().FullName);

                this.Handle(message);

                this.Logger.Debug(
                    () => string.Format("Handling finished of the message {0} {1}", message.GetType(), message), 
                    this.GetType().FullName);

                if (sw != null)
                {
                    sw.Stop();
                    this.Logger.Debug(
                        () => string.Format("Elapsed time of message processing in milliseconds {0}", sw.ElapsedMilliseconds), 
                        this.GetType().FullName);
                }
            }
            catch (Exception exception)
            {
                if (this.HandleException(exception))
                {
                    return;
                }

                throw;
            }
            finally
            {
                sw = null;
            }
        }

        #endregion

        // Flag: Has Dispose already been called? 

        // Protected implementation of Dispose pattern. 
        #region Methods

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                // Free any other managed objects here. 
            }

            // Free any unmanaged objects here. 
            this.disposed = true;
        }

        /// <summary>
        /// The handle exception.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool HandleException(Exception exception)
        {
            var transportMessage = (TransportMessage)BusExtensions.CurrentTransportMessageBeingHandled;
            var numberOfRetries = transportMessage.GetNumberOfRetries();
            var configurer = ServiceLocator.Instance.GetRootContainer().Resolve<Configure>();
            var maxNumberOfRetries = 0;
            if (configurer != null && configurer.Settings.GetConfigSection<SecondLevelRetriesConfig>() != null
                && configurer.Settings.GetConfigSection<SecondLevelRetriesConfig>().NumberOfRetries != null)
            {
                maxNumberOfRetries = configurer.Settings.GetConfigSection<SecondLevelRetriesConfig>().NumberOfRetries;
            }

            this.Logger.Debug(
                () => string.Format("CurrentNumberOfRetries is {0} MaxNumberOfRetries is {1}", numberOfRetries, maxNumberOfRetries), 
                this.GetType().FullName);

            if (numberOfRetries >= maxNumberOfRetries)
            {
                // var exceptionHandler = this.DependencyResolver.Resolve<BusinessExceptionHandler>();
                bool isHandled = false; //exceptionHandler.Handle(exception);
                if (!isHandled)
                {
                    this.Logger.Exception(exception, this.GetType().FullName);
                    return false;
                }

                return true;
            }

            this.Logger.Warning(
                () => string.Format("Exception: {0}{1}{2}", exception.Message, Environment.NewLine, exception.GetExceptionDetails()), 
                this.GetType().FullName);
            return false;
        }

        #endregion
    }
}
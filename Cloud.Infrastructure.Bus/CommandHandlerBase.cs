﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandHandlerBase.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Bus
{
    /// <summary>
    /// The command handler base.
    /// </summary>
    /// <typeparam name="T">
    /// Type of the command
    /// </typeparam>
    public abstract class CommandHandlerBase<T> : MessageHandlerBase<T>
        where T : CommandBase
    {
    }
}
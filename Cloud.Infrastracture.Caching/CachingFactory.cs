﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CachingFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching
{
    using System;
    using System.Collections.Generic;

    using Cloud.Infrastracture.Caching.Contracts;

    /// <summary>
    ///     The caching factory.
    /// </summary>
    public class CachingFactory : ICachingFactory
    {
        #region Static Fields

        /// <summary>
        /// The cache weak references.
        /// </summary>
        private static readonly Dictionary<string, WeakReference<ICache>> CacheWeakReferences =
            new Dictionary<string, WeakReference<ICache>>();

        /// <summary>
        /// The instance internal.
        /// </summary>
        private static readonly CachingFactory InstanceInternal = new CachingFactory();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static ICachingFactory Instance
        {
            get
            {
                return InstanceInternal;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create cache.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="isGetExisting">
        /// The is Get Existing.
        /// </param>
        /// <returns>
        /// The <see cref="ICache"/>.
        /// </returns>
        public ICache CreateCache(string name, bool isGetExisting = true)
        {
            return InstanceInternal.CreateCacheInternal(name, DateTimeOffset.MaxValue, isGetExisting);
        }

        /// <summary>
        /// The create cache.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="maxItemOffset">
        /// The max item offset.
        /// </param>
        /// <param name="isGetExisting">
        /// The is Get Existing.
        /// </param>
        /// <returns>
        /// The <see cref="ICache"/>.
        /// </returns>
        public ICache CreateCache(string name, DateTimeOffset maxItemOffset, bool isGetExisting = true)
        {
            return InstanceInternal.CreateCacheInternal(name, maxItemOffset, isGetExisting);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create cache internal.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="offset">
        /// The offset.
        /// </param>
        /// <param name="isGetExisting">
        /// The is get existing.
        /// </param>
        /// <returns>
        /// The <see cref="ICache"/>.
        /// </returns>
        private ICache CreateCacheInternal(string name, DateTimeOffset offset, bool isGetExisting = true)
        {
            if (isGetExisting)
            {
                WeakReference<ICache> cacheWeakReference;

                if (CacheWeakReferences.TryGetValue(name, out cacheWeakReference))
                {
                    ICache existingCache;
                    if (cacheWeakReference.TryGetTarget(out existingCache))
                    {
                        return existingCache;
                    }

                    CacheWeakReferences.Remove(name);
                }
            }

            return new MemoryCacheWrapper(name, offset);
        }

        #endregion
    }
}
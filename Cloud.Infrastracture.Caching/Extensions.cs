﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching
{
    using System;
    using System.Runtime.Caching;

    using Cloud.Infrastracture.Caching.Contracts;

    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
        #region Methods

        /// <summary>
        /// The to cache item policy.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="maxOffset">
        /// The max offset.
        /// </param>
        /// <returns>
        /// The <see cref="CacheItemPolicy"/>.
        /// </returns>
        internal static CacheItemPolicy ToCacheItemPolicy(this CacheItemSettings settings, DateTimeOffset maxOffset)
        {
            var policy = new CacheItemPolicy();

            if (settings.SlidingExpiration != ObjectCache.NoSlidingExpiration)
            {
                policy.SlidingExpiration = settings.SlidingExpiration;
            }
            else
            {
                policy.AbsoluteExpiration = settings.AbsoluteExpiration > maxOffset ? maxOffset : settings.AbsoluteExpiration;
            }

            if (settings.RemovedCallback != null)
            {
                policy.RemovedCallback = delegate(CacheEntryRemovedArguments arguments)
                    {
                        var itemCache = GetItemCache(arguments.CacheItem);
                        settings.RemovedCallback(
                            new CacheEntryItemRemovedEventArgs(
                                arguments.Source, 
                                itemCache, 
                                arguments.RemovedReason.ToCustomCacheEntryRemovedReason()));
                    };
            }

            return policy;
        }

        /// <summary>
        /// The get item cache.
        /// </summary>
        /// <param name="cacheItem">
        /// The cache item.
        /// </param>
        /// <returns>
        /// The <see cref="ItemCache"/>.
        /// </returns>
        private static ItemCache GetItemCache(CacheItem cacheItem)
        {
            var keyRegionData = cacheItem.Key.Split(CustomMemoryCache.SEPARATOR);
            var region = keyRegionData[0].Substring(7);
            var key = keyRegionData[1].Substring(4);
            return new ItemCache(key, cacheItem.Value, region);
        }

        #endregion
    }
}
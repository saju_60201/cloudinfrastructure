﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomMemoryCache.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching
{
    using System;
    using System.Collections.Specialized;
    using System.Runtime.Caching;

    /// <summary>
    ///     The custom memory cache which supports regions
    /// </summary>
    public class CustomMemoryCache : MemoryCache
    {
        #region Constants

        /// <summary>
        /// The separator.
        /// </summary>
        public const char SEPARATOR = (char)007;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomMemoryCache"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="config">
        /// The config.
        /// </param>
        public CustomMemoryCache(string name, NameValueCollection config = null)
            : base(name, config)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the default cache capabilities.
        /// </summary>
        public override DefaultCacheCapabilities DefaultCacheCapabilities
        {
            get
            {
                return base.DefaultCacheCapabilities | System.Runtime.Caching.DefaultCacheCapabilities.CacheRegions;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add or get existing.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absoluteExpiration">
        /// The absolute expiration.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object AddOrGetExisting(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null)
        {
            return base.AddOrGetExisting(this.CreateKeyWithRegion(key, regionName), value, absoluteExpiration);
        }

        /// <summary>
        /// The add or get existing.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="policy">
        /// The policy.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object AddOrGetExisting(string key, object value, CacheItemPolicy policy, string regionName = null)
        {
            return base.AddOrGetExisting(this.CreateKeyWithRegion(key, regionName), value, policy);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object Get(string key, string regionName = null)
        {
            return base.Get(this.CreateKeyWithRegion(key, regionName));
        }

        /// <summary>
        /// The get cache item.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="CacheItem"/>.
        /// </returns>
        public override CacheItem GetCacheItem(string key, string regionName = null)
        {
            CacheItem temporary = base.GetCacheItem(this.CreateKeyWithRegion(key, regionName));
            return new CacheItem(key, temporary.Value, regionName);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="policy">
        /// The policy.
        /// </param>
        public override void Set(CacheItem item, CacheItemPolicy policy)
        {
            this.Set(item.Key, item.Value, policy, item.RegionName);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absoluteExpiration">
        /// The absolute expiration.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        public override void Set(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null)
        {
            this.Set(key, value, new CacheItemPolicy { AbsoluteExpiration = absoluteExpiration }, regionName);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="policy">
        /// The policy.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        public override void Set(string key, object value, CacheItemPolicy policy, string regionName = null)
        {
            base.Set(this.CreateKeyWithRegion(key, regionName), value, policy);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create key with region.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string CreateKeyWithRegion(string key, string region)
        {
            return "region:" + (string.IsNullOrEmpty(region) ? "null_region" : region) + SEPARATOR + "key=" + key;
        }

        #endregion
    }
}
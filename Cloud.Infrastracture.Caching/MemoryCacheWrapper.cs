﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MemoryCacheWrapper.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastracture.Caching
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Caching;

    using Cloud.Infrastracture.Caching.Contracts;

    /// <summary>
    ///     The memory cache wrapper.
    /// </summary>
    public class MemoryCacheWrapper : ICache
    {
        #region Fields

        /// <summary>
        /// The cache.
        /// </summary>
        private readonly MemoryCache cache;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryCacheWrapper"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="maxDateTimeOffset">
        /// The max date time offset.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Name must be set
        /// </exception>
        public MemoryCacheWrapper(string name, DateTimeOffset maxDateTimeOffset)
        {
            this.MaxDateTimeOffset = maxDateTimeOffset;
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            this.cache = new CustomMemoryCache(name);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the max date time offset.
        /// </summary>
        public DateTimeOffset MaxDateTimeOffset { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Add(string key, object value, string regionName = null)
        {
            return this.cache.Add(key, value, this.MaxDateTimeOffset, regionName);
        }

        /// <summary>
        /// The add or get existing.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absoluteExpiration">
        /// The absolute expiration.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object AddOrGetExisting(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null)
        {
            return this.cache.AddOrGetExisting(key, value, absoluteExpiration, regionName);
        }

        /// <summary>
        /// The add or get existing.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object AddOrGetExisting(string key, object value, CacheItemSettings setting, string regionName = null)
        {
            return this.cache.Add(key, value, setting.ToCacheItemPolicy(this.MaxDateTimeOffset), regionName);
        }

        /// <summary>
        /// The contains.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Contains(string key, string regionName = null)
        {
            return this.cache.Contains(key, regionName);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Get(string key, string regionName = null)
        {
            return this.cache.Get(key, regionName);
        }

        /// <summary>
        /// The get count.
        /// </summary>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        public long GetCount(string regionName = null)
        {
            return this.cache.GetCount(regionName);
        }

        /// <summary>
        /// The get values.
        /// </summary>
        /// <param name="keys">
        /// The keys.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="IDictionary"/>.
        /// </returns>
        public IDictionary<string, object> GetValues(IEnumerable<string> keys, string regionName = null)
        {
            return this.cache.GetValues(keys, regionName);
        }

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Remove(string key, string regionName = null)
        {
            return this.cache.Remove(key, regionName);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="absoluteExpiration">
        /// The absolute expiration.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        public void Set(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null)
        {
            this.cache.Set(key, value, absoluteExpiration, regionName);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <param name="regionName">
        /// The region name.
        /// </param>
        public void Set(string key, object value, CacheItemSettings setting, string regionName = null)
        {
            this.cache.Set(key, value, setting.ToCacheItemPolicy(this.MaxDateTimeOffset), regionName);
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityOfWorkUnityRegistrar.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Localization.Contracts;

namespace Cloud.Infrastructure.Host
{
    /// <summary>
    ///     The unity of work unity registrar.
    /// </summary>
    public class UnityOfWorkUnityRegistrar : IRequestUnitOfWorkUnityRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get types to register.
        /// </summary>
        /// <returns>
        ///     The <see cref="Dictionary" />.
        /// </returns>
        public Dictionary<Type, Type> GetTypesToRegister()
        {
            var types = new Dictionary<Type, Type>();
            //types.Add(typeof(ISecurityDataProvider), typeof(SecurityDataProvider));
            //types.Add(typeof(ISessionProvider), typeof(SessionProvider));
            //types.Add(typeof(ITenantRuntimeProvider), typeof(TenantRuntimeProvider));
            //types.Add(typeof(ICustomerRuntimeProvider), typeof(CustomerRuntimeProvider));
            //types.Add(typeof(ITenantSelector), typeof(TenantSelector));
            //types.Add(typeof(ISessionValidator), typeof(SessionCustomerTenantValidator));
            types.Add(typeof(ICurrentCultureSetter), typeof(CurrentCultureSetter));
            types.Add(typeof(ILanguageSupport), typeof(LanguageSupport));
            types.Add(typeof(ILocalizationSupport), typeof(LocalizationSupport));
            //types.Add(typeof(ITenantConfigClientService), typeof(TenantConfigClientService));
            //types.Add(typeof(ICustomerTenantConfigProvider), typeof(CustomerTenantConfigProvider));

            //types.Add(typeof(IRequestInfo), typeof(RequestInfo));
            //types.Add(typeof(IUserRequestInfo), typeof(UserRequestInfo));

            //types.Add(typeof(ISessionEntryRepository), typeof(SessionRepositoryWeb));

            //if (EnvironmentInfo.IsDelevopmentMachine)
            //{
            //    types.Add(typeof(ISessionRepository), typeof(SessionRepositoryFile));
            //}
            //else
            //{
            //    types.Add(typeof(ISessionRepository), typeof(SessionRepository));
            //}

            return types;
        }

        #endregion
    }
}
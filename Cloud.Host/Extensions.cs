﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ITraceWriter = System.Web.Http.Tracing.ITraceWriter;

namespace Cloud.Infrastructure.Host
{
    using System;
    using System.Reflection;
    using System.Runtime.Serialization.Formatters;
    using System.Web.Http;
    using Cloud.Infrastructure.Host.Contracts;
    using Cloud.Infrastructure.Host.ExceptionHandling;
    using Cloud.Infrastructure.Interception;
    using Cloud.Infrastructure.Logging.Contracts;

    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
        #region Static Fields

        /// <summary>
        /// The cached logger.
        /// </summary>
        private static ILogger cachedLogger;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The enable all exception filter.
        /// </summary>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <param name="useHttpError">
        /// The use http error.
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        [Obsolete("Please use the new extension EnableGlobalExceptionHandler")]
        public static IBootstrapperConfig EnableAllExceptionFilter(this IBootstrapperConfig config, bool useHttpError = true)
        {
            config.EnableGlobalExceptionHandler();
            return config;
        }

        /// <summary>
        /// The enable global exception handler.
        /// </summary>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig EnableGlobalExceptionHandler(this IBootstrapperConfig config)
        {
            var handler = config.Container.ResolveSafe<IExceptionHandler>(config.Logger()) ?? new ExceptionHandler();
            var globalExceptionHandler = new GlobalExceptionHandler(handler, config.Logger());
            GlobalConfiguration.Configuration.Services.Replace(
                typeof(System.Web.Http.ExceptionHandling.IExceptionHandler), 
                globalExceptionHandler);
            config.Container.RegisterInstance(typeof(System.Web.Http.ExceptionHandling.IExceptionHandler), null, globalExceptionHandler,new ContainerControlledLifetimeManager());
            return config;
        }

        /// <summary>
        /// handle the $type field of the json data
        /// </summary>
        /// <param name="config">
        /// The bootstrapper config
        /// </param>
        /// <param name="typeNameHandling">
        /// TypeNameHandling
        /// </param>
        /// <param name="formatterAssemblyStyle">
        /// FormatterAssemblyStyle
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig EnableJsonTypeNameHandling(
            this IBootstrapperConfig config, 
            TypeNameHandling typeNameHandling = TypeNameHandling.All, 
            FormatterAssemblyStyle formatterAssemblyStyle = FormatterAssemblyStyle.Simple)
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.TypeNameHandling = typeNameHandling;
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.TypeNameAssemblyFormat = formatterAssemblyStyle;

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Error = HandleJsonSerializationErrors;
            return config;
        }

        /// <summary>
        /// The enable logging.
        /// </summary>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <param name="isEnabled">
        /// The is enabled.
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig EnableLogging(this IBootstrapperConfig config, bool isEnabled = true)
        {
            ((BootstrapperConfig)config).IsLoggingEnabled = isEnabled;
            var logger = config.Container.ResolveSafe<ILogger>(new NullLogger());

            // add the default logger to container
            if (logger == null)
            {
                logger = new Logger();
                logger.Debug(() => "Register ILogger on the container");
                config.Container.RegisterInstance(logger);
            }

            // replace the default tracewriter
            logger.Debug(() => "Check if logger is ITraceWriter");
            if (logger is ITraceWriter)
            {
                logger.Debug(() => "Logger is ITraceWriter so replace the service on GlobalConfiguration");
                GlobalConfiguration.Configuration.Services.Replace(typeof(ITraceWriter), logger);
            }

            return config;
        }

        /// <summary>
        /// The enable session management.
        /// </summary>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        /// <summary>
        /// The get current customer.
        /// </summary>
        /// <returns>
        /// The <see cref="Customer"/>.
        /// </returns>
        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="bootstrapper">
        /// The bootstrapper.
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig Initialize(this BootstrapperWebApi bootstrapper)
        {
            return bootstrapper.Run(false);
        }

        /// <summary>
        /// The is null.
        /// </summary>
        /// <param name="subject">
        /// The subject.
        /// </param>
        /// <typeparam name="T">
        /// Type
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsNull<T>(this T subject)
        {
            return ReferenceEquals(subject, null);
        }

        /// <summary>
        /// The logger.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <returns>
        /// The <see cref="ILogger"/>.
        /// </returns>
        public static ILogger Logger(this object instance)
        {
            if (cachedLogger != null)
            {
                return cachedLogger;
            }

            object traceWriter = GlobalConfiguration.Configuration.Services.GetService(typeof(ITraceWriter));
            cachedLogger = traceWriter as ILogger;
            return cachedLogger ?? new NullLogger();
        }

        /// <summary>
        /// The read caching enable flag from config file.
        /// </summary>
        /// <param name="config">
        /// The config.
        /// </param>
        /// <param name="bindingFlags">
        /// Json.Net does not by default use private setters during the deserialization. To enable the private setter you have
        ///     three options
        ///     - use the extension with default value
        ///     - use custom DefaultContractResolver
        ///     - set attribute [JsonProperty] on the specific property and create a private setter
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        /// <summary>
        /// Enable different search flags during deserialization
        /// </summary>
        /// <param name="config">
        /// The bootstrapper config
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        [Obsolete("Use the Method SetSerializePrivateMemeter")]
        public static IBootstrapperConfig SetDefaultMembersSearchFlags(
            this IBootstrapperConfig config, 
            BindingFlags bindingFlags = BindingFlags.NonPublic)
        {
            JsonSerializerSettings settings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            if (settings == null)
            {
                IContractResolver contractResolver = new DefaultContractResolver();
                settings = new JsonSerializerSettings { ContractResolver = contractResolver };
            }

            var defaultContractResolver = settings.ContractResolver as DefaultContractResolver;
            if (defaultContractResolver == null)
            {
                throw new Exception("The actually used contractresolver is not of type DefaultContractResolver");
            }

            defaultContractResolver.DefaultMembersSearchFlags |= bindingFlags;
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings = settings;
            return config;
        }

        /// <summary>
        /// Enable different search flags during deserialization
        /// </summary>
        /// <param name="config">
        /// The bootstrapper config
        ///     Json.Net does not by default use private setters during the deserialization. To enable the private setter you have
        ///     three options
        ///     - use the extension with default value
        ///     - use custom DefaultContractResolver
        ///     - set attribute [JsonProperty] on the specific property and create a private setter
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        public static IBootstrapperConfig SetSerializePrivateMembers(this IBootstrapperConfig config)
        {
            JsonSerializerSettings settings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            if (settings == null)
            {
                IContractResolver contractResolver = new PrivateSetterContractResolver();
                settings = new JsonSerializerSettings { ContractResolver = contractResolver };
            }

            settings.ContractResolver = new PrivateSetterContractResolver();

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings = settings;
            return config;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The handle json serialization errors.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="errorEventArgs">
        /// The error event args.
        /// </param>
        private static void HandleJsonSerializationErrors(object sender, ErrorEventArgs errorEventArgs)
        {
            string errorMessage = string.Format(
                "Path: {0} - Member: {1} - Message: {2} StackTrace: {3}", 
                errorEventArgs.ErrorContext.Path, 
                errorEventArgs.ErrorContext.Member, 
                errorEventArgs.ErrorContext.Error.Message, 
                errorEventArgs.ErrorContext.Error.StackTrace);
            GlobalConfiguration.Configuration.DependencyResolver.Logger().Error(() => errorMessage);
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestHandler.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Cloud.Infrastructure.Logging.Contracts;

namespace Cloud.Infrastructure.Host
{
    /// <summary>
    ///     The request handler.
    /// </summary>
    public class RequestHandler : DelegatingHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestHandler"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// must not be null
        /// </exception>
        public RequestHandler(ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.Logger = logger;
        }

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// The send async.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            this.AddMessageToContenxt(request);

           // string passedCulture = request.Headers.GetCookieOrHeaderFirstValue(MessageHeaders.CultureIdToken);

            //var requestInfo = (IRequestInfo)request.GetDependencyScope().GetService(typeof(IRequestInfo));
            //requestInfo.Headers = request.Headers.Select(h => h);
            //requestInfo.HostName = request.RequestUri.AbsoluteUri.Substring(
            //    0,
            //    request.RequestUri.AbsoluteUri.Length - request.RequestUri.LocalPath.Length);

            //if (request.Headers.Authorization != null)
            //{
            //    requestInfo.AuthorizationParameter = request.Headers.Authorization.Parameter;
            //    requestInfo.AuthorizationScheme = request.Headers.Authorization.Scheme;
            //    this.Logger.Debug(() => string.Format("request.Headers.Authorization.AuthorizationParameter is {0}", request.Headers.Authorization.Parameter));
            //}
            //else
            //{
            //    this.Logger.Warning(() => string.Format("request.Headers.Authorization is null at session handler"));
            //}

            //if (!string.IsNullOrEmpty(passedCulture))
            //{
            //    this.Logger.Debug(() => string.Format("Passed culture is {0}", passedCulture));
            //    int lcid;
            //    CultureInfo selectedCulture = null;
            //    if (int.TryParse(passedCulture, out lcid))
            //    {
            //        selectedCulture = new CultureInfo(lcid);
            //    }

            //    selectedCulture = selectedCulture ?? new CultureInfo(passedCulture);

            //    var cultureSetter = (ICurrentCultureSetter)request.GetDependencyScope().GetService(typeof(ICurrentCultureSetter));
            //    cultureSetter.Culture = selectedCulture;
            //    this.Logger.Debug(() => string.Format("Culture has been set on the culturesetter {0}", selectedCulture.DisplayName));
            //}
            //else
            //{
            //    this.Logger.Debug(() => string.Format("No culture coookie/custom header {0}", MessageHeaders.CultureIdToken));
            //}

            return base.SendAsync(request, cancellationToken);
        }

        private void AddMessageToContenxt(HttpRequestMessage request)
        {
            const string HttpRequestMessageKey = "MS_HttpRequestMessage";

            if (HttpContext.Current.Items.Contains(HttpRequestMessageKey))
            {
                return;
            }

            HttpContext.Current.Items.Add(HttpRequestMessageKey, request);
        }
    }
}
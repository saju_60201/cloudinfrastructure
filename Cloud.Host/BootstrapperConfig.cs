﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BootstrapperConfig.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host
{
    using System;
    using System.Collections.Generic;

    using Cloud.Infrastructure.Host.Contracts;

    using Microsoft.Practices.Unity;

    /// <summary>
    ///     The bootstrapper config.
    /// </summary>
    public class BootstrapperConfig : IBootstrapperConfig
    {
        #region Fields

        /// <summary>
        /// The is tenant awareness enabled.
        /// </summary>
        private bool isTenantAwarenessEnabled = true;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="BootstrapperConfig" /> class.
        /// </summary>
        public BootstrapperConfig()
        {
            this.UnitOfWorkTypesToRegisterDynamic = new Dictionary<Type, Type>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the bootstrapper.
        /// </summary>
        public IBootstrapper Bootstrapper { get; internal set; }

        /// <summary>
        ///     Gets the container.
        /// </summary>
        public IUnityContainer Container { get; internal set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is auto register unity of work.
        /// </summary>
        public bool IsAutoRegisterUnityOfWork { get; set; }

        /// <summary>
        ///     Gets a value indicating whether is logging enabled.
        /// </summary>
        public bool IsLoggingEnabled { get; internal set; }

        /// <summary>
        ///     Gets a value indicating whether [is unity container tenant aware].
        /// </summary>
        /// <value>
        ///     <c>true</c> if [is unity container tenant aware]; otherwise, <c>false</c>.
        /// </value>
        public bool IsTenantAwarenessEnabled
        {
            get
            {
                return this.isTenantAwarenessEnabled;
            }

            internal set
            {
                this.isTenantAwarenessEnabled = value;
            }
        }

        /// <summary>
        ///     Gets or sets the unit of work types to register dynamic.
        /// </summary>
        public Dictionary<Type, Type> UnitOfWorkTypesToRegisterDynamic { get; set; }

        #endregion
    }
}
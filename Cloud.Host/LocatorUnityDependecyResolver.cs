﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocatorUnityDependecyResolver.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Web.Http.Dependencies;
using Cloud.Infrastructure.Interception;
using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Host
{
    //using Cloud.Infrastructure.Interception;
    using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

    // using IDependencyResolver = Cloud.Infrastructure.Interception.IDependencyResolver;

    /// <summary>
    /// The locator unity dependency resolver.
    /// </summary>
    public class LocatorUnityDependencyResolver : UnityDependencyScope, IDependencyResolver, IDependencyScope, IDisposable
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LocatorUnityDependencyResolver"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public LocatorUnityDependencyResolver(IUnityContainer container)
            : base(container)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The begin scope.
        /// </summary>
        /// <returns>
        /// The <see cref="IDependencyScope"/>.
        /// </returns>
        public IDependencyScope BeginScope()
        {
            var childContainer = this.Container.CreateChildContainer();
            ((IUnitOfWorkContainerSetter)ServiceLocator.Instance).SetUnitOfWorkContainerThreadStatic(childContainer);
            return new UnityDependencyScope(childContainer);
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Logger.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Tracing;
using Cloud.Infrastructure.Logging;

namespace Cloud.Infrastructure.Host
{
    /// <summary>
    ///     The logger.
    /// </summary>
    public class Logger : LoggerBase<HttpRequestMessage>, ITraceWriter
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Logger" /> class.
        /// </summary>
        public Logger()
        {
            if (!string.IsNullOrEmpty(RootLoggerName) || HttpRuntime.AppDomainAppId == null)
            {
                return;
            }

            var appDomainName = HttpRuntime.AppDomainAppId;
            if (appDomainName.Contains("/"))
            {
                RootLoggerName =
                    appDomainName.Substring(appDomainName.LastIndexOf("/", StringComparison.Ordinal)).Trim();
            }
        }

        /// <summary>
        ///     The trace.
        /// </summary>
        /// <param name="request">
        ///     The request.
        /// </param>
        /// <param name="category">
        ///     The category.
        /// </param>
        /// <param name="level">
        ///     The level.
        /// </param>
        /// <param name="traceAction">
        ///     The trace action.
        /// </param>
        void ITraceWriter.Trace(HttpRequestMessage request, string category, TraceLevel level,
            Action<TraceRecord> traceAction)
        {
            if (level == TraceLevel.Off || !IsLoggingEnabled())
            {
                return;
            }

            var record = new TraceRecord(request, category, level);
            traceAction(record);
            Log(record, typeof (ITraceWriter).FullName);
        }

        /// <summary>
        ///     The get item log text.
        /// </summary>
        /// <param name="request">
        ///     The request.
        /// </param>
        /// <returns>
        ///     The <see cref="StringBuilder" />.
        /// </returns>
        protected override StringBuilder GetItemLogText(HttpRequestMessage request)
        {
            var message = new StringBuilder();

            if (request == null)
            {
                return null;
            }

            if (request.Method != null)
            {
                message.Append(request.Method);
            }

            if (request.RequestUri != null)
            {
                message.Append(message.Length > 0 ? " " : string.Empty).Append(request.RequestUri);
            }

            return message;
        }

        /// <summary>
        ///     The get message.
        /// </summary>
        /// <param name="record">
        ///     The record.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        private static string GetMessage(TraceRecord record)
        {
            var message = new StringBuilder();

            if (record.Request != null)
            {
                if (record.Request.Method != null)
                {
                    message.Append(record.Request.Method);
                }

                if (record.Request.RequestUri != null)
                {
                    message.Append(message.Length > 0 ? " " : string.Empty).Append(record.Request.RequestUri);
                }
            }

            if (!string.IsNullOrWhiteSpace(record.Category))
            {
                message.Append(message.Length > 0 ? " " : string.Empty).Append(record.Category);
            }

            if (!string.IsNullOrWhiteSpace(record.Operator))
            {
                message.Append(message.Length > 0 ? " " : string.Empty)
                    .Append(record.Operator)
                    .Append(" ")
                    .Append(record.Operation);
            }

            if (!string.IsNullOrWhiteSpace(record.Message))
            {
                message.Append(message.Length > 0 ? " " : string.Empty).Append(record.Message);
            }

            if (record.Exception != null && !string.IsNullOrWhiteSpace(record.Exception.GetBaseException().Message))
            {
                message.Append(message.Length > 0 ? " " : string.Empty)
                    .Append(record.Exception.GetBaseException().Message);
            }

            return message.ToString();
        }

        /// <summary>
        ///     The log.
        /// </summary>
        /// <param name="record">
        ///     The record.
        /// </param>
        /// <param name="loggerName">
        ///     The logger name.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The passed LogLevel does not exist
        /// </exception>
        private void Log(TraceRecord record, string loggerName)
        {
            var message = GetMessage(record);
            switch (record.Level)
            {
                case TraceLevel.Off:
                    break;
                case TraceLevel.Debug:
                    Debug(() => message, loggerName);
                    break;
                case TraceLevel.Info:
                    Info(() => message, loggerName);
                    break;
                case TraceLevel.Warn:
                    Warning(() => message, loggerName);
                    break;
                case TraceLevel.Error:
                    Error(() => message, loggerName);
                    break;
                case TraceLevel.Fatal:
                    Fatal(() => message, loggerName);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // add hopefully the stacktrace to the exception
            if (record.Exception != null)
            {
                Exception(record.Exception);
            }
        }
    }
}
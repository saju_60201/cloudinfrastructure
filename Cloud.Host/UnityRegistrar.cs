﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityRegistrar.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Cloud.Infrastracture.Caching;
using Cloud.Infrastracture.Caching.Contracts;
using Cloud.Infrastructure.Caching.Contracts;
using Cloud.Infrastructure.Configuration;
using Cloud.Infrastructure.Interception;
using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Host
{
    /// <summary>
    ///     The unity registrar.
    /// </summary>
    public class UnityRegistrar : IUnityRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public void Register(IUnityContainer container)
        {
            container.RegisterInstance(ServiceLocator.Instance, new ContainerControlledLifetimeManager());
            //container.RegisterType<ISessionHandler, SessionHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICachingFactory, CachingFactory>(new ContainerControlledLifetimeManager());
            //container.RegisterType<IServerSettingsProvider, ServerSettingsProviderConfigFile>(new ContainerControlledLifetimeManager());
            container.RegisterType<ConfigurationProvider, ConfigurationProvider>(new ContainerControlledLifetimeManager());            
            //container.RegisterType<ServiceTenantConfiguration, ServiceTenantConfiguration>(new ContainerControlledLifetimeManager());
            container.RegisterType<CachingConfiguration, CachingConfiguration>(new ContainerControlledLifetimeManager());    
        }

        #endregion
    }
}
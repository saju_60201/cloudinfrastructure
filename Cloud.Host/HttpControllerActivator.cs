﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpControllerActivator.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace Cloud.Infrastructure.Host
{
    /// <summary>
    ///     The http controller activator.
    /// </summary>
    public class HttpControllerActivator : IHttpControllerActivator
    {
        /// <summary>
        ///     The create.
        /// </summary>
        /// <param name="request">
        ///     The request.
        /// </param>
        /// <param name="controllerDescriptor">
        ///     The controller descriptor.
        /// </param>
        /// <param name="controllerType">
        ///     The controller type.
        /// </param>
        /// <returns>
        ///     The <see cref="IHttpController" />.
        /// </returns>
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor,
            Type controllerType)
        {
            // return GlobalConfiguration.Configuration.DependencyResolver.GetService(controllerType) as IHttpController;
            return GlobalConfiguration.Configuration.DependencyResolver.GetService(controllerType) as IHttpController;
        }
    }
}
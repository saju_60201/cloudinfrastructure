// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BootstrapperWebApi.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Web.Http.Dispatcher;

namespace Cloud.Infrastructure.Host
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web;
    using System.Web.Hosting;
    using System.Web.Http;

    using Cloud.Infrastructure.Caching.Contracts;
    using Cloud.Infrastructure.Host.Contracts;
    using Cloud.Infrastructure.Interception;
    using Cloud.Infrastructure.Logging.Contracts;

    using Microsoft.Practices.Unity;

    using NLog;

    using ILogger = Cloud.Infrastructure.Logging.Contracts.ILogger;

    /// <summary>
    ///     The bootstrapper web api.
    /// </summary>
    public class BootstrapperWebApi : IBootstrapper
    {
        #region Static Fields

        /// <summary>
        ///     The is service locator initialized.
        /// </summary>
        private static volatile bool isServiceLocatorInitialized;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BootstrapperWebApi"/> class.
        /// </summary>
        /// <param name="enableTenantAwareness">
        /// if set to <c>true</c> [enable tenant awareness].
        /// </param>
        public BootstrapperWebApi(bool enableTenantAwareness = false)
        {
            this.InitializeServiceLocator(new ScopeHostUnityContainer());

            this.Config = new BootstrapperConfig
                              {
                                  Container = ServiceLocator.Instance.Current, 
                                  IsAutoRegisterUnityOfWork = true, 
                                  Bootstrapper = this
                              };
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the config.
        /// </summary>
        protected BootstrapperConfig Config { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Registers the dynamic request unit of work types.
        /// </summary>
        /// <param name="typesToRegister">
        /// The types to register.
        /// </param>
        public void RegisterDynamicRequestUnitOfWorkTypes(IList<UnityTypeMapping> typesToRegister)
        {
            var logger = this.Config.Container.Resolve<ILogger>();
            this.RegisterRequestUnitOfWorkTypes(this.Config.Container, typesToRegister, logger);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="isLoggingEnabled">
        /// The is logging enabled.
        /// </param>
        /// <returns>
        /// The <see cref="IBootstrapperConfig"/>.
        /// </returns>
        internal virtual IBootstrapperConfig Run(bool isLoggingEnabled = false)
        {
            this.Config.IsLoggingEnabled = isLoggingEnabled;
            this.SetupContainer();
            this.HandleUnhandledExceptions(this.Config);
            this.HandleApplicationShutdown(this.Config);
            this.RegisterRequestInfoHandler(this.Config);
            this.RegisterLoggingHandler(this.Config);
            this.SetDefaultCachingConfiguration(this.Config);
            return this.Config;
        }

        /// <summary>
        ///     The build unity container.
        /// </summary>
        protected virtual void BuildUnityContainer()
        {
            this.RegisterLogger(this.Config.Container);

            var logger = this.Config.Container.Resolve<ILogger>();

            this.RegisterControllers(this.Config.Container);
            this.CheckAssemlbyLoad(logger);
            this.RegisterTypes(this.Config.Container, logger);

            if (this.Config.IsAutoRegisterUnityOfWork)
            {
                this.RegisterRequestUnitOfWorkTypes();
            }

            this.SubscribeTypeLoad();
        }

        /// <summary>
        /// The register controllers.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        protected virtual void RegisterControllers(IUnityContainer container)
        {
            // Register our http controller activator with NSB
            container.RegisterInstance<IHttpControllerActivator>(new HttpControllerActivator(), new ContainerControlledLifetimeManager());
            //container.RegisterInstance<IHttpControllerActivator>(new DefaultControllerActivator(), new ContainerControlledLifetimeManager());
            
        }

        /// <summary>
        /// Registers the request unit of work types.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <param name="typesToRegister">
        /// The types to register.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        protected virtual void RegisterRequestUnitOfWorkTypes(
            IUnityContainer container, 
            IList<UnityTypeMapping> typesToRegister, 
            ILogger logger)
        {
            foreach (var typeToRegister in typesToRegister)
            {
                this.RegisterTypeMapping(container, typeToRegister, logger);
            }
        }

        /// <summary>
        ///     The register request unit of work types.
        /// </summary>
        protected void RegisterRequestUnitOfWorkTypes()
        {
            var logger = this.Config.Container.Resolve<ILogger>();
            this.RegisterRequestUnitOfWorkTypes(this.Config.Container, this.GetTypesUsingUnityOfWorkUnityRegistrar(logger), logger);
        }

        /// <summary>
        /// Registers the type mapping.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <param name="typeToRegister">
        /// The type to register.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        protected virtual void RegisterTypeMapping(IUnityContainer container, UnityTypeMapping typeToRegister, ILogger logger)
        {
            var nameText = string.IsNullOrEmpty(typeToRegister.Name) ? string.Empty : " with name {0}".FormatWith(typeToRegister.Name);
            if (typeToRegister.InjectionMembers == null)
            {
                logger.Debug(
                    () =>
                    string.Format(
                        "Register type {0} to {1}{2} using {3}", 
                        typeToRegister.TypeFrom.FullName, 
                        typeToRegister.TypeTo.FullName, 
                        nameText, 
                        typeof(HierarchicalLifetimeManager).Name));
                container.RegisterType(
                    typeToRegister.TypeFrom, 
                    typeToRegister.TypeTo, 
                    typeToRegister.Name, 
                    new HierarchicalLifetimeManager());
                return;
            }

            if (typeToRegister.InjectionMembers != null && typeToRegister.InjectionMembers.Length > 0)
            {
                logger.Debug(
                    () =>
                    string.Format(
                        "Register type {0} to {1}{2} using {3}", 
                        typeToRegister.TypeFrom.FullName, 
                        typeToRegister.TypeTo.FullName, 
                        nameText, 
                        typeof(HierarchicalLifetimeManager).Name));
                container.RegisterType(
                    typeToRegister.TypeFrom, 
                    typeToRegister.TypeTo, 
                    typeToRegister.Name, 
                    new HierarchicalLifetimeManager(), 
                    typeToRegister.InjectionMembers);
            }
        }

        /// <summary>
        /// The register types.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        protected virtual void RegisterTypes(IUnityContainer container, ILogger logger)
        {
            this.RegisterTypesUsingUnityRegistrar(logger);
        }

        /// <summary>
        ///     The setup container.
        /// </summary>
        protected virtual void SetupContainer()
        {
            this.BuildUnityContainer();
            GlobalConfiguration.Configuration.DependencyResolver = new LocatorUnityDependencyResolver(this.Config.Container);
        }

        /// <summary>
        /// The get registrar instances.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private static IEnumerable<T> GetRegistrarInstances<T>(ILogger logger)
        {
            try
            {
                logger.Debug(() => string.Format("Look for {0} in the current domain", typeof(T)));

                var registrarTypes = CurrentDomainTypes.GetTypesDerivingFrom<T>(false);

                logger.Debug(() => string.Format("Found {0} types count {1}", typeof(T), registrarTypes.Count()));
                return registrarTypes.Select(registrarType => (T)Activator.CreateInstance(registrarType)).ToList();
            }
            catch (ReflectionTypeLoadException reflectionTypeLoadException)
            {
                logger.Fatal(reflectionTypeLoadException);
                throw;
            }
        }

        /// <summary>
        /// The report shut down reason.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        private static void ReportShutDownReason(ILogger logger)
        {
            // config is disposed, so recreate
            LogManager.Configuration = new LogFactory().Configuration;

            logger.Warning(() => string.Format("Shutdown reason is {0}", HostingEnvironment.ShutdownReason));

            var runtime =
                (HttpRuntime)
                typeof(HttpRuntime).InvokeMember(
                    "_theRuntime", 
                    BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.GetField, 
                    null, 
                    null, 
                    null);

            if (runtime == null)
            {
                ShutDownLogManager();
                return;
            }

            var shutDownMessage =
                (string)
                runtime.GetType()
                    .InvokeMember(
                        "_shutDownMessage", 
                        BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, 
                        null, 
                        runtime, 
                        null);

            var shutDownStack =
                (string)
                runtime.GetType()
                    .InvokeMember(
                        "_shutDownStack", 
                        BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, 
                        null, 
                        runtime, 
                        null);

            var messageToLog = string.Format("\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}", shutDownMessage, shutDownStack);

            switch (HostingEnvironment.ShutdownReason)
            {
                case ApplicationShutdownReason.HostingEnvironment:
                case ApplicationShutdownReason.ChangeInGlobalAsax:
                case ApplicationShutdownReason.ConfigurationChange:
                case ApplicationShutdownReason.UnloadAppDomainCalled:
                case ApplicationShutdownReason.ChangeInSecurityPolicyFile:
                case ApplicationShutdownReason.BinDirChangeOrDirectoryRename:
                case ApplicationShutdownReason.BrowsersDirChangeOrDirectoryRename:
                case ApplicationShutdownReason.CodeDirChangeOrDirectoryRename:
                case ApplicationShutdownReason.ResourcesDirChangeOrDirectoryRename:
                case ApplicationShutdownReason.IdleTimeout:
                case ApplicationShutdownReason.PhysicalApplicationPathChanged:
                case ApplicationShutdownReason.HttpRuntimeClose:
                case ApplicationShutdownReason.BuildManagerChange:
                    logger.Warning(() => messageToLog);
                    break;
                case ApplicationShutdownReason.InitializationError:
                case ApplicationShutdownReason.MaxRecompilationsReached:
                    logger.Error(() => messageToLog);
                    break;
                default:
                    logger.Warning(() => messageToLog);
                    break;
            }

            ////if (!EventLog.SourceExists(".NET Runtime"))
            //{
            ////    EventLog.CreateEventSource(".NET Runtime", "Application");
            ////}

            //EventLog log = new EventLog();
            //log.Source = ".NET Runtime";
            //log.WriteEntry(messageToLog,EventLogEntryType.Error);
            ShutDownLogManager();
        }

        /// <summary>
        ///     The shut down log manager.
        /// </summary>
        private static void ShutDownLogManager()
        {
            if (LogManager.Configuration != null)
            {
                LogManager.Shutdown();
            }
        }

        /// <summary>
        /// If the assemblies can't be loaded, this indicated a bad setup of the project
        ///     and must be solved before going on
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        private void CheckAssemlbyLoad(ILogger logger)
        {
            try
            {
                this.LoadCurrentDomainTypes(logger);
            }
            catch (ReflectionTypeLoadException typeLoadException)
            {
                logger.Fatal(typeLoadException);
                throw;
            }
            catch (Exception exception)
            {
                logger.Fatal(exception);
                throw;
            }
        }

        /// <summary>
        /// The get types using unity of work unity registrar.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private IList<UnityTypeMapping> GetTypesUsingUnityOfWorkUnityRegistrar(ILogger logger)
        {
            var typesOfUnitOfWork = new List<UnityTypeMapping>();

            try
            {
                this.LoadCurrentDomainTypes(logger);

                var registrarInstances = GetRegistrarInstances<IRequestUnitOfWorkUnityRegistrar>(logger);

                foreach (var unityRegistrar in registrarInstances)
                {
                    var registrarFullName = unityRegistrar.GetType().FullName;
                    logger.Debug(() => string.Format("Execute Registrar {0}", registrarFullName));
                    var typesToRegister = unityRegistrar.GetTypesToRegister();
                    if (typesToRegister != null && typesToRegister.Count > 0)
                    {
                        logger.Debug(() => string.Format("{0} has types to register {1}", registrarFullName, typesToRegister.Count));
                        foreach (var type in typesToRegister)
                        {
                            var unitOfWorkType = typesOfUnitOfWork.FirstOrDefault(x => x.TypeFrom == type.Key);
                            if (unitOfWorkType != null)
                            {
                                typesOfUnitOfWork.Remove(unitOfWorkType);
                            }

                            typesOfUnitOfWork.Add(new UnityTypeMapping(type.Key, type.Value));
                        }
                    }
                }

                logger.Debug(() => string.Format("Total types to register for UnityOfWork {0}", typesOfUnitOfWork.Count));
            }
            catch (ReflectionTypeLoadException reflectionTypeLoadException)
            {
                this.Logger().Fatal(reflectionTypeLoadException);
                throw;
            }

            return typesOfUnitOfWork;
        }

        /// <summary>
        /// The handle application shutdown.
        /// </summary>
        /// <param name="bootstrapperConfig">
        /// The bootstrapper config.
        /// </param>
        private void HandleApplicationShutdown(BootstrapperConfig bootstrapperConfig)
        {
            AppDomain.CurrentDomain.DomainUnload += (sender, args) => ReportShutDownReason(bootstrapperConfig.Logger());
        }

        /// <summary>
        /// The handle unhandled exceptions.
        /// </summary>
        /// <param name="bootstrapperConfig">
        /// The bootstrapper config.
        /// </param>
        private void HandleUnhandledExceptions(BootstrapperConfig bootstrapperConfig)
        {
            AppDomain.CurrentDomain.UnhandledException += delegate(object sender, UnhandledExceptionEventArgs args)
                {
                    var exception = args.ExceptionObject as Exception;
                    if (exception != null)
                    {
                        bootstrapperConfig.Logger().Fatal(exception);
                    }
                };
        }

        /// <summary>
        /// The initialize service locator.
        /// </summary>
        /// <param name="hostUnityContainer">
        /// The host unity container.
        /// </param>
        private void InitializeServiceLocator(IHostUnityContainer hostUnityContainer)
        {
            if (isServiceLocatorInitialized)
            {
                return;
            }

            ((IServiceLocatorInitializer)ServiceLocator.Instance).Initialize(hostUnityContainer);
            isServiceLocatorInitialized = true;
        }

        /// <summary>
        /// Loads all types in the current domain and caches them
        /// </summary>
        /// <param name="logger">
        /// Logger
        /// </param>
        private void LoadCurrentDomainTypes(ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            var entryAssembly = EnvironmentInfo.GetEntryAssembly();
            if (entryAssembly != null)
            {
                logger.Debug(() => string.Format("Add entry assembly to CurrentDomainTypes {0}", entryAssembly.FullName));
                CurrentDomainTypes.AddAssembly(entryAssembly.GetName());
            }

            var loadedTypes = CurrentDomainTypes.GetTypes().ToList();

            logger.Debug(
                () =>
                string.Format("Count of loaded types in actual domain {0} {1}", AppDomain.CurrentDomain.FriendlyName, loadedTypes.Count));
        }

        /// <summary>
        /// The on current domain type resolve.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The <see cref="Assembly"/>.
        /// </returns>
        private Assembly OnCurrentDomainTypeResolve(object sender, ResolveEventArgs args)
        {
            var foundType = CurrentDomainTypes.GetTypes().FirstOrDefault(t => t.FullName == args.Name || t.Name == args.Name);
            if (foundType != null)
            {
                return foundType.Assembly;
            }

            return null;
        }

        /// <summary>
        /// The register logger.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        private void RegisterLogger(IUnityContainer container)
        {
            var logger = container.ResolveSafe<ILogger>(new NullLogger());
            if (logger.IsNull())
            {
                container.RegisterType<ILogger, Logger>(new ContainerControlledLifetimeManager());
            }
        }

        /// <summary>
        /// The register logging handler.
        /// </summary>
        /// <param name="bootstrapperConfig">
        /// The bootstrapper config.
        /// </param>
        private void RegisterLoggingHandler(BootstrapperConfig bootstrapperConfig)
        {
            var handler = bootstrapperConfig.Container.Resolve<ElapsedTimeMessageHandler>();
            GlobalConfiguration.Configuration.MessageHandlers.Add(handler);
        }

        /// <summary>
        /// The register request info handler.
        /// </summary>
        /// <param name="bootstrapperConfig">
        /// The bootstrapper config.
        /// </param>
        private void RegisterRequestInfoHandler(BootstrapperConfig bootstrapperConfig)
        {
            var handler = bootstrapperConfig.Container.Resolve<RequestHandler>();
            GlobalConfiguration.Configuration.MessageHandlers.Add(handler);
        }

        /// <summary>
        /// register types using IUnityRegistrar
        /// </summary>
        /// <param name="logger">
        /// Logger
        /// </param>
        private void RegisterTypesUsingUnityRegistrar(ILogger logger)
        {
            this.LoadCurrentDomainTypes(logger);

            var registrarInstances = GetRegistrarInstances<IUnityRegistrar>(logger);

            foreach (var unityRegistrar in registrarInstances)
            {
                logger.Debug(() => string.Format("Execute Registrar {0}", unityRegistrar.GetType().FullName));
                unityRegistrar.Register(this.Config.Container);
            }
        }

        /// <summary>
        /// The set default caching configuration.
        /// </summary>
        /// <param name="bootstrapperConfig">
        /// The bootstrapper config.
        /// </param>
        private void SetDefaultCachingConfiguration(BootstrapperConfig bootstrapperConfig)
        {
            var cachingConfiguration = bootstrapperConfig.Container.Resolve<CachingConfiguration>();
            cachingConfiguration.IsCachingEnabled = true;
        }

        /// <summary>
        ///     The subscribe type load.
        /// </summary>
        private void SubscribeTypeLoad()
        {
            AppDomain.CurrentDomain.TypeResolve += this.OnCurrentDomainTypeResolve;
        }

        #endregion
    }
}
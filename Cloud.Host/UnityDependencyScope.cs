﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityDependencyScope.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;

namespace Cloud.Infrastructure.Host
{
    /// <summary>
    /// The unity dependency scope.
    /// </summary>
    public class UnityDependencyScope : IDependencyScope, IDisposable
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityDependencyScope"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public UnityDependencyScope(IUnityContainer container)
        {
            this.Container = container;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the container.
        /// </summary>
        protected IUnityContainer Container { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Container.Dispose();
        }

        /// <summary>
        /// The get service.
        /// </summary>
        /// <param name="serviceType">
        /// The service type.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetService(Type serviceType)
        {
            if (typeof(IHttpController).IsAssignableFrom(serviceType))
            {
                return UnityContainerExtensions.Resolve(this.Container, serviceType, new ResolverOverride[0]);
            }

            try
            {
                return UnityContainerExtensions.Resolve(this.Container, serviceType, new ResolverOverride[0]);
            }
            catch
            {
                return (object)null;
            }
        }

        /// <summary>
        /// The get services.
        /// </summary>
        /// <param name="serviceType">
        /// The service type.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.Container.ResolveAll(serviceType, new ResolverOverride[0]);
        }

        #endregion
    }
}
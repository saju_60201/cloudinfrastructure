﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ElapsedTimeMessageHandler.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host
{
    using System;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    using Cloud.Infrastructure.Logging.Contracts;

    /// <summary>
    ///     The elapsed time message handler.
    /// </summary>
    public class ElapsedTimeMessageHandler : DelegatingHandler
    {
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ElapsedTimeMessageHandler"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// logger must not be null
        /// </exception>
        public ElapsedTimeMessageHandler(ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.logger = logger;
        }

        /// <summary>
        /// The send async.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!this.logger.IsLoggingEnabled())
            {
                return await base.SendAsync(request, cancellationToken);
            }

            var sw = new Stopwatch();
            sw.Start();

            var response = await base.SendAsync(request, cancellationToken);
            sw.Stop();

            this.logger.Debug(() => string.Format("Elapsed time for Uri {0} in Milliseconds {1}", request.RequestUri, sw.ElapsedMilliseconds));

            // todo: disable this, might be too expensive for big responses
            //if (response.Content != null)
            //{
            //    await response.Content.LoadIntoBufferAsync();
            //    this.logger.Debug(() => string.Format("Size of the message {0}", response.Content.Headers.ContentLength));
            //}
            return response;
        }
    }
}
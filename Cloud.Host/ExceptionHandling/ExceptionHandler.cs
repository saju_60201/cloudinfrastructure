﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionHandler.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Host.ExceptionHandling
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Web.Http.ExceptionHandling;

    using NLog.Internal;

    /// <summary>
    ///     The exception handler.
    /// </summary>
    public class ExceptionHandler : Cloud.Infrastructure.Host.Contracts.IExceptionHandler
    {
        #region Public Methods and Operators

        /// <summary>
        /// The handle.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        public void HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            this.Logger().Exception(context.Exception);

            string messageToReturn = "Something went wrong on the server";

           
            if (EnvironmentInfo.IsDelevopmentMachine)
            {
                var sb = new StringBuilder();
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("------------------- Development Environment ----------------------");
                sb.AppendLine(context.Exception.GetType().FullName);
                sb.AppendLine(this.GetMessageToReturn(context.Exception, Environment.NewLine));
                sb.AppendLine(context.Exception.StackTrace);

                messageToReturn = sb.ToString();
            }

            var statusCode = HttpStatusCode.InternalServerError;

            if (context.Exception is NotImplementedException)
            {
                statusCode = HttpStatusCode.NotImplemented;
            }
            //else if (context.Exception is AuthorizationException)
            //{
            //    statusCode = HttpStatusCode.Forbidden;
            //}

            // return the error back to the caller
            //context.Response = useHttpError
            //                       ? context.Request.CreateErrorResponse(statusCode, messageToReturn)
            //                       : context.Request.CreateResponse(statusCode);
            var resp = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(messageToReturn),
                ReasonPhrase = context.Exception.GetType().FullName,
                RequestMessage = context.ExceptionContext.Request
            };

            context.Result = new ErrorMessageResult(resp);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get message to return.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="delimitter">
        /// The delimitter.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetMessageToReturn(Exception exception, string delimitter = " ")
        {
            if (exception == null)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();

            sb.Append(exception.Message + delimitter);
            if (exception.InnerException == null)
            {
                return sb.ToString().Trim();
            }

            return sb.Append((this.GetMessageToReturn(exception.InnerException, delimitter) + delimitter).Trim()).ToString().Trim();
        }

        #endregion
    }
}
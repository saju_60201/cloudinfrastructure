﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BankingRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The banking repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Repository
{
    using Cloud.Infrastructure.Repository;
    using Cloud.Repository.Models;

    /// <summary>
    ///     The banking repository.
    /// </summary>
    public class BankingRepository : RepositoryBase<CoreBankingContext>, IBankingRepository
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BankingRepository"/> class.
        /// </summary>
        public BankingRepository()
            : base()
        {
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBankingRepository.cs" company="">
//   
// </copyright>
// <summary>
//   The BankingRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Repository
{
    using Cloud.Infrastructure.Repository.Contracts;

    /// <summary>
    ///     The BankingRepository interface.
    /// </summary>
    public interface IBankingRepository : IRepository
    {
    }
}
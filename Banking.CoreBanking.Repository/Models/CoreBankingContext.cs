// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CoreBankingContext.cs" company="">
//   
// </copyright>
// <summary>
//   The core banking context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Repository.Models
{
    using System.Data.Entity;

    using Banking.Dtos;

    using Cloud.Infrastructure.Repository;
    using Cloud.Repository.Models.Mapping;

    /// <summary>
    /// The core banking context.
    /// </summary>
    public class CoreBankingContext : ContextBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="CoreBankingContext"/> class.
        /// </summary>
        static CoreBankingContext()
        {
            // Database.SetInitializer<CoreBankingContext>(new CoreBankingContextInitializer());
            Database.SetInitializer<CoreBankingContext>(null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CoreBankingContext"/> class.
        /// </summary>
        public CoreBankingContext()
            : base("Name=CoreBankingContext")
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the accounts.
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The on model creating.
        /// </summary>
        /// <param name="modelBuilder">
        /// The model builder.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountMap());
        }

        #endregion
    }
}
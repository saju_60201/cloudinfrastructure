using System.Data.Entity.ModelConfiguration;
using Banking.Dtos;

namespace Cloud.Repository.Models.Mapping
{
    public class AccountMap : EntityTypeConfiguration<Account>
    {
        public AccountMap()
        {
            // Primary Key
            this.HasKey(t => t.AccountId);

            // Properties
            this.Property(t => t.AccountName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.AccountNumber)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Account");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.AccountNumber).HasColumnName("AccountNumber");
            this.Property(t => t.Balance).HasColumnName("Balance");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}

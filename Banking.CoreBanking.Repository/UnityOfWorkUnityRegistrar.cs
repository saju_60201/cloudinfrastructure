﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityOfWorkUnityRegistrar.cs" company="">
//   
// </copyright>
// <summary>
//   The unity of work unity registrar.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Repository
{
    using System;
    using System.Collections.Generic;

    using Cloud.Infrastructure.Interception;
    using Cloud.Infrastructure.Repository.Contracts;

    /// <summary>
    ///     The unity of work unity registrar.
    /// </summary>
    public class UnityOfWorkUnityRegistrar : IRequestUnitOfWorkUnityRegistrar
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get types to register.
        /// </summary>
        /// <returns>
        ///     The <see cref="Dictionary" />.
        /// </returns>
        public Dictionary<Type, Type> GetTypesToRegister()
        {
            var types = new Dictionary<Type, Type>();

            types.Add(typeof(IBankingRepository), typeof(BankingRepository));

            // types.Add(typeof(ApplicationSignInManager), typeof(ApplicationSignInManager));
            return types;
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IConnectionStringFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository.Contracts
{
    using System.Configuration;

    /// <summary>
    /// The ConnectionStringFactory interface.
    /// </summary>
    /// <typeparam name="TContext">
    /// Db Context
    /// </typeparam>
    public interface IConnectionStringFactory<TContext>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <param name="settingsPrefix">
        /// The settingsPrefix used for write model settings lookup.
        /// </param>
        /// <returns>
        /// The db context.
        /// </returns>
        ConnectionStringSettings Create(string settingsPrefix = null);

        #endregion
    }
}
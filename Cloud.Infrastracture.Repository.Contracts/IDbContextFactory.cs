﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDbContextFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository.Contracts
{
    /// <summary>
    /// Interface for DbContextFactory implementations.
    /// </summary>
    /// <typeparam name="TContext">
    /// The type of the context.
    /// </typeparam>
    public interface IDbContextFactory<out TContext>
    {
        #region Public Methods and Operators

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <param name="settingsPrefix">
        /// The settingsPrefix used for write model settings lookup.
        /// </param>
        /// <returns>
        /// The db context.
        /// </returns>
        TContext Create(string settingsPrefix = null);

        #endregion
    }
}
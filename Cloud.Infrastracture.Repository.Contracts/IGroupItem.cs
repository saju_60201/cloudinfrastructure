// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGroupItem.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository.Contracts
{
    using System.Linq;

    /// <summary>
    /// The GroupItem interface.
    /// </summary>
    /// <typeparam name="T">
    /// The item being grouped
    /// </typeparam>
    public interface IGroupItem<T>
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        IOrderedQueryable<T> Value { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get key.
        /// </summary>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        object GetKey();

        #endregion
    }
}
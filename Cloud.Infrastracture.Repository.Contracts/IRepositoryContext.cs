﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepositoryContext.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository.Contracts
{
    /// <summary>
    /// The RepositoryContext interface.
    /// </summary>
    /// <typeparam name="TContext">
    /// type of the context
    /// </typeparam>
    public interface IRepositoryContext<out TContext>
        where TContext : class, new()
    {
        #region Public Properties

        /// <summary>
        ///     Gets the context.
        /// </summary>
        TContext Context { get; }

        #endregion
    }
}
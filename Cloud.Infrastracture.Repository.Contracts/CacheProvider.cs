// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheProvider.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository.Contracts
{
    using System.Diagnostics.CodeAnalysis;

    using Cloud.Infrastracture.Caching;
    using Cloud.Infrastracture.Caching.Contracts;

    /// <summary>
    ///     The cache provider.
    /// </summary>
    public class CacheProvider
    {
        #region Constants

        /// <summary>
        /// The cache key separator.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1303:ConstFieldNamesMustBeginWithUpperCaseLetter", 
            Justification = "Reviewed. Suppression is OK here.")]
        private const string cacheKeySeparator = "_";

        /// <summary>
        /// The full text columns container name.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1303:ConstFieldNamesMustBeginWithUpperCaseLetter", 
            Justification = "Reviewed. Suppression is OK here.")]
        private const string fullTextColumnsContainerName = "FullTextColumnsContainers";

        #endregion

        // ReSharper disable FieldCanBeMadeReadOnly.Local
        #region Static Fields

        /// <summary>
        /// The cache instance.
        /// </summary>
        private static ICache cacheInstance = CachingFactory.Instance.CreateCache(FullTextColumnsContainerName);

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the cache instance.
        /// </summary>
        public static ICache CacheInstance
        {
            get
            {
                return cacheInstance;
            }
        }

        // ReSharper restore FieldCanBeMadeReadOnly.Local

        /// <summary>
        ///     Gets the cache key separator.
        /// </summary>
        public static string CacheKeySeparator
        {
            get
            {
                return cacheKeySeparator;
            }
        }

        /// <summary>
        ///     Gets the full text columns container name.
        /// </summary>
        public static string FullTextColumnsContainerName
        {
            get
            {
                return fullTextColumnsContainerName;
            }
        }

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GroupItem.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository.Contracts
{
    using System.Linq;

    /// <summary>
    /// The group item.
    /// </summary>
    /// <typeparam name="TKey">
    /// the type of the key
    /// </typeparam>
    /// <typeparam name="T">
    /// the item it self
    /// </typeparam>
    public class GroupItem<TKey, T> : IGroupItem<T>
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the key.
        /// </summary>
        public TKey Key { get; set; }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public IOrderedQueryable<T> Value { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get key.
        /// </summary>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        public object GetKey()
        {
            return this.Key;
        }

        #endregion
    }
}
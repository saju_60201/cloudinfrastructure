﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepositoryRead.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository.Contracts
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    ///     The RepositoryRead interface.
    /// </summary>
    public interface IRepositoryRead : IDisposable
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets or sets the current culture provider.
        /// </summary>
        /// <summary>
        /// Gets the specified filter.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The searched entity.
        /// </returns>
        IQueryable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class;

        /// <summary>
        ///     Gets this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>The entity.</returns>
        IQueryable<TEntity> Get<TEntity>() where TEntity : class;

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="keyValues">
        /// The key values.
        /// </param>
        /// <returns>
        /// The searched entity.
        /// </returns>
        TEntity GetById<TEntity>(params object[] keyValues) where TEntity : class;

        /// <summary>
        /// Searches the full text.
        /// </summary>
        /// <typeparam name="TEntity">
        /// The type of the entity.
        /// </typeparam>
        /// <param name="keywords">
        /// The keywords.
        /// </param>
        /// <returns>
        /// Returned
        /// </returns>
        IQueryable<TEntity> SearchFullText<TEntity>(string keywords) where TEntity : class;

        #endregion
    }
}
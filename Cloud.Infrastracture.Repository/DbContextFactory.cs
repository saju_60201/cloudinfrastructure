﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbContextFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// Standard factory to create dbcontext with dedicated connectionstring.
    /// </summary>
    /// <typeparam name="TContext">
    /// The type of the context.
    /// </typeparam>
    public class DbContextFactory<TContext> : IDbContextFactory<TContext>
        where TContext : ContextBase, new()
    {
        #region Fields

        /// <summary>
        /// The db connection.
        /// </summary>
        private readonly IDbConnection dbConnection;

        /// <summary>
        /// The name or connection string.
        /// </summary>
        private readonly string nameOrConnectionString;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextFactory{TContext}"/> class.
        /// </summary>
        /// <param name="nameOrConnectionString">
        /// The name or connection string.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// nameOrConnectionString
        /// </exception>
        public DbContextFactory(string nameOrConnectionString)
        {
            if (string.IsNullOrEmpty(nameOrConnectionString))
            {
                throw new ArgumentNullException("nameOrConnectionString");
            }

            this.nameOrConnectionString = nameOrConnectionString;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextFactory{TContext}"/> class.
        /// </summary>
        /// <param name="dbConnection">
        /// The database connection.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// dbConnection
        /// </exception>
        public DbContextFactory(IDbConnection dbConnection)
        {
            if (dbConnection == null)
            {
                throw new ArgumentNullException("dbConnection");
            }

            this.dbConnection = dbConnection;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="TContext"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public TContext Create()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <param name="settingsPrefix">
        /// used for write model settings lookup.
        /// </param>
        /// <returns>
        /// An instance of type "TContext"
        /// </returns>
        public TContext Create(string settingsPrefix = null)
        {
            var contextType = typeof(TContext);

            if (!string.IsNullOrEmpty(this.nameOrConnectionString))
            {
                if (contextType.GetConstructor(new[] { typeof(string) }) == null)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Please make sure that your dbcontext has a contructor with Argument '{0}'. Derive your context from {1} base class and implement all base constructors to get all allowed constructors.", 
                            typeof(string).FullName, 
                            typeof(ContextBase).FullName));
                }

                return Activator.CreateInstance(contextType, this.nameOrConnectionString) as TContext;
            }

            if (this.dbConnection != null)
            {
                if (contextType.GetConstructor(new[] { typeof(DbConnection) }) == null)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Please make sure that your dbcontext has a contructor with Argument '{0}'. Derive your context from {1} base class and implement all base constructors to get all allowed constructors.", 
                            typeof(DbConnection).FullName, 
                            typeof(ContextBase).FullName));
                }

                return Activator.CreateInstance(contextType, this.dbConnection) as TContext;
            }

            return Activator.CreateInstance(contextType) as TContext;
        }

        #endregion
    }
}
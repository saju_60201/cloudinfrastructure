﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NamedRepositoryLocker.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.Entity;

    using Cloud.Infrastructure.Logging.Contracts;
    using Cloud.Infrastructure.Repository.Contracts;

    /// <summary>
    /// The named repository locker.
    /// </summary>
    /// <typeparam name="TContext">
    /// Type of the context
    /// </typeparam>
    public class NamedRepositoryLocker<TContext> : INamedLocker
        where TContext : ContextBase, new()
    {
        #region Fields

        /// <summary>
        /// The connection.
        /// </summary>
        private DbConnection connection;

        /// <summary>
        /// The context.
        /// </summary>
        private TContext context;

        /// <summary>
        /// The disposed.
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// The is commited.
        /// </summary>
        private bool isCommited;

        /// <summary>
        /// The transaction.
        /// </summary>
        private DbContextTransaction transaction;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NamedRepositoryLocker{TContext}"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public NamedRepositoryLocker(IRepository<TContext> repository, ILogger logger)
        {
            this.Logger = logger;
            this.context = ((IRepositoryContext<TContext>)repository).Context;
            this.transaction = this.context.Database.BeginTransaction();
            this.connection = this.context.Database.Connection;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        public ILogger Logger { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The commit.
        /// </summary>
        public void Commit()
        {
            this.context.SaveChanges();
            this.transaction.Commit();
            this.isCommited = true;
        }

        /// <summary>
        ///     Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The lock.
        /// </summary>
        /// <param name="lockName">
        /// The lock name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Lock(string lockName)
        {
            return CreateLock(this.connection, this.transaction, lockName, this.Logger);
        }

        /// <summary>
        ///     The rollback.
        /// </summary>
        public void Rollback()
        {
            this.transaction.Rollback();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                if (!this.isCommited)
                {
                    this.transaction.Rollback();
                }

                this.connection = null;
                this.transaction = null;
                this.context = null;
            }

            // Free any unmanaged objects here.
            this.disposed = true;
        }

        /// <summary>
        /// The create lock.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="subject">
        /// The subject.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CreateLock(DbConnection connection, DbContextTransaction transaction, string subject, ILogger logger)
        {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction.UnderlyingTransaction;
            cmd.CommandText = "sp_getapplock";
            cmd.CommandType = CommandType.StoredProcedure;

            var param = cmd.CreateParameter();
            param.ParameterName = "Resource";
            param.DbType = DbType.AnsiString;
            param.Value = subject;
            param.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "LockMode";
            param.DbType = DbType.AnsiString;
            param.Value = "Exclusive";
            param.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "LockOwner";
            param.DbType = DbType.AnsiString;
            param.Value = "Transaction";
            param.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "LockTimeout";
            param.DbType = DbType.Int64;
            param.Value = 5000;
            param.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(param);

            var returnValue = cmd.CreateParameter();
            returnValue.DbType = DbType.Int32;
            returnValue.Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            var lockResult = (int)returnValue.Value;
            if (lockResult == 0 || lockResult == 1)
            {
                var id = subject;
                logger.Debug(() => string.Format("Lock for subject id {0} successful", id));
            }
            else
            {
                var id = subject;
                logger.Error(() => string.Format("Lock for subject id {0} failed with sp_getapplock error code {1}", id, lockResult));
                return false;
            }

            return true;
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbConnectionFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository
{
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    ///     Responsible to instantiate a DbConnection.
    /// </summary>
    public class DbConnectionFactory
    {
        #region Fields

        /// <summary>
        /// The connection string.
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// The provider invariant name.
        /// </summary>
        private readonly string providerInvariantName;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnectionFactory"/> class.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        public DbConnectionFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnectionFactory"/> class.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="providerInvariantName">
        /// Name of the provider invariant.
        /// </param>
        public DbConnectionFactory(string connectionString, string providerInvariantName)
        {
            this.connectionString = connectionString;
            this.providerInvariantName = providerInvariantName;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Creates the DbConnection.
        /// </summary>
        /// <returns>The DbConnection.</returns>
        public IDbConnection Create()
        {
            IDbConnection dbConnection = null;
            try
            {
                dbConnection = DbProviderFactories.GetFactory(this.providerInvariantName ?? "System.Data.SqlClient").CreateConnection();
                dbConnection.ConnectionString = this.connectionString;
            }
            catch
            {
                dbConnection = (DbConnection)new SqlConnection(this.connectionString);
            }

            return dbConnection;
        }

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryBase.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Repository
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;

    using Cloud.Infrastracture.Caching;
    using Cloud.Infrastracture.Caching.Contracts;
    using Cloud.Infrastructure.Localization.Contracts;
    using Cloud.Infrastructure.Logging.Contracts;
    using Cloud.Infrastructure.Repository.Contracts;
    using Cloud.Infrastructure.T4EFMultilanguage;

    using Microsoft.Practices.Unity;

    /// <summary>
    /// The repository base.
    /// </summary>
    /// <typeparam name="TContext">
    /// The type of the context.
    /// </typeparam>
    public abstract class RepositoryBase<TContext> : IRepository<TContext>, IRepositoryContext<TContext>
        where TContext : ContextBase, new()
    {
        #region Constants

        /// <summary>
        ///     The cache key separator.
        /// </summary>
        private const string CacheKeySeparator = "_";

        /// <summary>
        ///     The full text columns container name.
        /// </summary>
        private const string FullTextColumnsContainerName = "FullTextColumnsContainers";

        #endregion

        #region Fields

        /// <summary>
        ///     The do not track.
        /// </summary>
        private readonly bool doNotTrack;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{TContext}"/> class.
        /// </summary>
        /// <param name="doNotTrack">
        /// if set to <c>true</c> all queries will be called with .AsNoTracking().
        /// </param>
        protected RepositoryBase(bool doNotTrack = false)
        {
            this.doNotTrack = doNotTrack;
            this.ContextInternal = new TContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{TContext}"/> class.
        /// </summary>
        /// <param name="contextProvider">
        /// The context provider.
        /// </param>
        /// <param name="doNotTrack">
        /// if set to <c>true</c> all queries will be called with .AsNoTracking().
        /// </param>
        protected RepositoryBase(Contracts.IDbContextFactory<TContext> contextProvider, bool doNotTrack = false)
        {
            this.doNotTrack = doNotTrack;
            if (contextProvider == null)
            {
                throw new ArgumentNullException("contextProvider");
            }

            this.ContextInternal = contextProvider.Create();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the cache instance that stores repository related entries: list of multilanguage columns etc.
        /// </summary>
        [Dependency(FullTextColumnsContainerName)]
        public static ICache CacheInstance { get; set; }

        /// <summary>
        ///     Gets or sets the current culture provider.
        /// </summary>
        [Dependency]
        public ILanguageSupport LanguageSupport { get; set; }

        #endregion

        #region Explicit Interface Properties

        /// <summary>
        ///     Gets the context.
        /// </summary>
        /// <value>
        ///     The context.
        /// </value>
        TContext IRepositoryContext<TContext>.Context
        {
            get
            {
                return this.ContextInternal;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the context internal.
        /// </summary>
        protected TContext ContextInternal { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The create locker.
        /// </summary>
        /// <returns>
        ///     The <see cref="INamedLocker" />.
        /// </returns>
        public INamedLocker CreateNamedLocker()
        {
            return new NamedRepositoryLocker<TContext>(this, new NullLogger());
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="keyValues">
        /// The key values.
        /// </param>
        /// <typeparam name="TEntity">
        /// Entity use for this method
        /// </typeparam>
        public virtual void Delete<TEntity>(params object[] keyValues) where TEntity : class
        {
            TEntity entityToDelete = this.ContextInternal.Set<TEntity>().Find(keyValues);
            if (entityToDelete != null)
            {
                Delete(entityToDelete);
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entityToDelete">
        /// The entity to delete.
        /// </param>
        /// <typeparam name="TEntity">
        /// Entity use for this method
        /// </typeparam>
        public virtual void Delete<TEntity>(TEntity entityToDelete) where TEntity : class
        {
            if (this.ContextInternal.Entry(entityToDelete).State == EntityState.Detached)
            {
                this.ContextInternal.Set<TEntity>().Attach(entityToDelete);
            }

            this.ContextInternal.Set<TEntity>().Remove(entityToDelete);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <typeparam name="TEntity">
        /// actual object
        /// </typeparam>
        public void Delete<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class
        {
            foreach (var entityToDelete in this.ContextInternal.Set<TEntity>().Where(filter))
            {
                Delete(entityToDelete);
            }
        }

        /// <summary>
        ///     Dispose the context
        /// </summary>
        public void Dispose()
        {
            if (this.ContextInternal != null)
            {
                this.ContextInternal.Dispose();
            }

            this.ContextInternal = null;
        }

        /// <summary>
        /// The execute sql.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        public void ExecuteSql(string sql, params object[] parameters)
        {
            this.ContextInternal.Database.ExecuteSqlCommand(TransactionalBehavior.EnsureTransaction, sql, parameters);
        }

        /// <summary>
        /// Get the entities
        /// </summary>
        /// <typeparam name="TEntity">
        /// entity type
        /// </typeparam>
        /// <param name="filter">
        /// where clauses
        /// </param>
        /// <returns>
        /// return querable
        /// </returns>
        public virtual IQueryable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class
        {
            return this.ContextInternal.Set<TContext, TEntity>(this.doNotTrack).Where(filter);
        }

        /// <summary>
        ///     The create query.
        /// </summary>
        /// <typeparam name="TEntity">
        ///     Entity use for this method
        /// </typeparam>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        public IQueryable<TEntity> Get<TEntity>() where TEntity : class
        {
            return this.ContextInternal.Set<TContext, TEntity>(this.doNotTrack);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="keyValues">
        /// The key values.
        /// </param>
        /// <typeparam name="TEntity">
        /// The entity this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity GetById<TEntity>(params object[] keyValues) where TEntity : class
        {
            return this.ContextInternal.Set<TEntity>().Find(keyValues);
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <typeparam name="TEntity">
        /// Entity use for this method
        /// </typeparam>
        public virtual void Insert<TEntity>(TEntity entity) where TEntity : class
        {
            this.ContextInternal.Set<TEntity>().Add(entity);
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <typeparam name="TEntity">
        /// Entity use for this method
        /// </typeparam>
        public virtual void Save<TEntity>(TEntity entity) where TEntity : class
        {
            ObjectContext objContext = ((IObjectContextAdapter)this.ContextInternal).ObjectContext;
            ObjectSet<TEntity> objSet = objContext.CreateObjectSet<TEntity>();
            EntityKey entityKey = objContext.CreateEntityKey(objSet.EntitySet.Name, entity);

            object foundEntity;
            bool exists = objContext.TryGetObjectByKey(entityKey, out foundEntity);
            if (exists)
            {
                objContext.Detach(foundEntity);
                this.ContextInternal.Set<TEntity>().Attach(entity);
                this.ContextInternal.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                this.ContextInternal.Set<TEntity>().Add(entity);
            }
        }

        /// <summary>
        ///     The save changes.
        /// </summary>
        public virtual void SaveChanges()
        {
            this.ContextInternal.SaveChanges();
        }

        /// <summary>
        /// Search full text.
        /// </summary>
        /// <param name="keywords">
        /// The keywords.
        /// </param>
        /// <typeparam name="TEntity">
        /// domain entity
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TEntity> SearchFullText<TEntity>(string keywords) where TEntity : class
        {
            DbSet<TEntity> queryable = this.ContextInternal.Set<TEntity>();
            if (!string.IsNullOrEmpty(keywords))
            {
                this.CheckDependencies();
                string sqlStatement = this.MakeSqlStatement<TEntity>(keywords);

                if (this.doNotTrack)
                {
                    return queryable.SqlQuery(sqlStatement).AsNoTracking().AsQueryable();
                }

                return queryable.SqlQuery(sqlStatement).AsQueryable();
            }

            return queryable;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entityToUpdate">
        /// The entity to update.
        /// </param>
        /// <typeparam name="TEntity">
        /// Entity use for this method
        /// </typeparam>
        public virtual void Update<TEntity>(TEntity entityToUpdate) where TEntity : class
        {
            this.ContextInternal.Set<TEntity>().Attach(entityToUpdate);
            this.ContextInternal.Entry(entityToUpdate).State = EntityState.Modified;
        }

        #endregion

        #region Methods

        /// <summary>
        /// breakdown keywords
        /// </summary>
        /// <param name="keywords">
        /// seach data
        /// </param>
        /// <returns>
        /// return the keywords
        /// </returns>
        private static string SetKeywords(string keywords)
        {
            if (string.IsNullOrEmpty(keywords))
            {
                return null;
            }

            string[] words = keywords.Replace("\"", string.Empty)
                .Replace("'", "''")
                .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < words.Length; i++)
            {
                stringBuilder.Append(string.Format("\"{0}\"", words[i]));
                if (i != words.Length - 1)
                {
                    stringBuilder.Append(" AND ");
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        ///     The check dependencies.
        /// </summary>
        /// <exception cref="Exception">
        /// </exception>
        private void CheckDependencies()
        {
            if (CacheInstance == null)
            {
                CacheInstance = CachingFactory.Instance.CreateCache(FullTextColumnsContainerName);
            }

            if (this.LanguageSupport == null)
            {
                throw new Exception("languageSupport is null. Please ensure the Repository is resolved using DI Container");
            }
        }

        /// <summary>
        /// The make sql statement.
        /// </summary>
        /// <param name="keyword">
        /// The keyword.
        /// </param>
        /// <typeparam name="TEntity">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string MakeSqlStatement<TEntity>(string keyword) where TEntity : class
        {
            string sql = string.Empty;
            Type type = typeof(TEntity);
            string tableName = type.Name;

            var columsList =
                CacheInstance.Get(
                    tableName.ToLower() + CacheKeySeparator
                    + this.LanguageSupport.GetCurrentLanguage().CultureInfo.TwoLetterISOLanguageName.ToLower()) as string;

            if (columsList == null || string.IsNullOrEmpty(columsList))
            {
                columsList = type.GetColumnsList(this.LanguageSupport);
                CacheInstance.Add(
                    tableName.ToLower() + CacheKeySeparator
                    + this.LanguageSupport.GetCurrentLanguage().CultureInfo.TwoLetterISOLanguageName.ToLower(), 
                    columsList);
            }

            keyword = SetKeywords(keyword);

            if (!string.IsNullOrEmpty(keyword) && string.IsNullOrEmpty(columsList))
            {
                sql = string.Format("SELECT * FROM {0} T WHERE CONTAINS(T.*, '{1}')", tableName, keyword);
            }
            else if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(columsList))
            {
                sql = string.Format("SELECT * FROM {0} WHERE CONTAINS(({2}), '{1}')", tableName, keyword, columsList);
            }

            return sql;
        }

        #endregion
    }
}
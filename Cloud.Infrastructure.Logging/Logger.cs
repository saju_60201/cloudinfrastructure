﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Logger.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Logging
{
    using System.Text;

    /// <summary>
    ///     The logger.
    /// </summary>
    public class Logger : LoggerBase<object>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Logger" /> class.
        /// </summary>
        public Logger()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="defaultLoggerName">
        /// The logger name.
        /// </param>
        public Logger(string defaultLoggerName)
            : base(defaultLoggerName)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get item log text.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="StringBuilder"/>.
        /// </returns>
        protected override StringBuilder GetItemLogText(object item)
        {
            return null;
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using Cloud.Infrastructure.Logging.Contracts;
using SpecExpress;

namespace Cloud.Infrastructure.Validation
{
    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
        private static bool haveScannedCurrentDomain;

        /// <summary>
        /// The scan current domain.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="forceScan">
        /// The force Scan.
        /// </param>
        public static void ScanCurrentDomain(ILogger logger, bool forceScan = false)
        {
            if (logger == null)
            {
                logger = new NullLogger();
            }

            if (!forceScan && haveScannedCurrentDomain)
            {
                logger.Debug(() => "Domain has been already scanned, so go out here");
                return;
            }

            ValidationContextUnitOfWork.ScanForSpecification(forceScan);

            logger.Debug(
                () => string.Format("Specifications found in CurrentDomain {0}", ValidationContextUnitOfWork.GetFoundSpecificationsCount()));

            haveScannedCurrentDomain = true;
        }

        /// <summary>
        /// Ensures that when validating an instance of T, that another specification also be enforced.
        /// </summary>
        /// <param name="spec">
        /// The spec.
        /// </param>
        /// <typeparam name="U">
        /// The type of the object the specification to enforce is validating.
        /// </typeparam>
        /// <typeparam name="TSpecType">
        /// The type of the specification to enforce.
        /// </typeparam>
        public static void UsingFixed<U, TSpecType>(this SpecificationBase spec) where TSpecType : Validates<U>, new()
        {
            //Get the PropertyValidators from the Using Spec and add it to this specification
            var usingSpec =
                ValidationCatalog.SpecificationContainer.GetAllSpecifications().FirstOrDefault(x => x.GetType() == typeof(TSpecType));
            if (usingSpec == null)
            {
                ValidationCatalog.AddSpecification<TSpecType>();
                spec.UsingFixed<U, TSpecType>();
                return;
            }

            spec.PropertyValidators.AddRange(usingSpec.PropertyValidators);
        }
    }
}
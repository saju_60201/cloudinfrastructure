﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandValidationFailedInvoker.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Logging.Contracts;
using Cloud.Infrastructure.Validation.Contracts;

namespace Cloud.Infrastructure.Validation
{
    /// <summary>
    ///     The command validation failed invoker.
    /// </summary>
    public class CommandValidationFailedInvoker : HandlerInvokerBase<ICommandValidationFailedHandler, ValidationResult>, 
                                                  ICommandValidationFailedInvoker
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandValidationFailedInvoker"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="serviceLocator">
        /// The service Locator.
        /// </param>
        public CommandValidationFailedInvoker(ILogger logger, IServiceLocator serviceLocator)
            : base(logger, serviceLocator, (handler, result) => handler.HandleCommandFailed(result))
        {
        }
    }
}
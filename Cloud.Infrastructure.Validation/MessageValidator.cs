﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageValidator.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Cloud.Infrastructure.Exceptions;
using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Logging.Contracts;
using SpecExpress;

namespace Cloud.Infrastructure.Validation
{
    using ValidationResult = Contracts.ValidationResult;

    /// <summary>
    /// The message validator.
    /// </summary>
    public class MessageValidator : IMessageValidator
    {
       

        private readonly IDependencyResolver resolver;

        private ILogger logger; 

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageValidator"/> class.
        /// </summary>
        /// <param name="resolver">Resolver
        /// </param>
        /// <param name="languageSupport">
        /// The language Support.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public MessageValidator(IDependencyResolver resolver, ILogger logger)
        {
            this.resolver = resolver;           
            this.logger = logger;
        }

        #endregion

        /// <summary>
        /// The validate using all supported languages.
        /// </summary>
        /// <param name="toValidationInstance">
        /// The to validation instance.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/>.
        /// </returns>
        public ValidationResult ValidateUsingAllSupportedLanguages(object toValidationInstance)
        {
            return this.ValidateUsingLanguage(toValidationInstance);
        }

        /// <summary>
        /// The validate using language.
        /// </summary>
        /// <param name="toValidationInstance">
        /// The to validation instance.
        /// </param>
        /// <param name="languages">
        /// The languages.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/>.
        /// </returns>
        //public ValidationResult ValidateUsingLanguage(object toValidationInstance)
        //{          
        //    object validationNotification = null;
        //    object currentLanguageNotification = null;
        //    bool isValidAndNoMessages = false;
        //    ValidationResult validationResult = null;

        //        if (validationResult == null || !isValidAndNoMessages)
        //        {
        //            validationResult = this.ValidateUsingLanguage(toValidationInstance);
        //            var hasMessages = validationResult.OriginalNotification != null
        //                              && ((ValidationNotification)validationResult.OriginalNotification).All().Any();

        //            isValidAndNoMessages = validationResult.IsValid && !hasMessages;
        //        }
        //        else
        //        {
        //            validationResult = CreateValidationResult(
        //                toValidationInstance,                       
        //                (ValidationNotification)validationResult.OriginalNotification);
        //        }

        //        validationNotification = validationResult.OriginalNotification;
        //        if (language.CultureInfo.Equals(Thread.CurrentThread.CurrentCulture))
        //        {
        //            currentLanguageNotification = validationResult.OriginalNotification;
        //        }

        //        languageValues.Add(validationResult.ValidationNotification.First());
         

        //    currentLanguageNotification = currentLanguageNotification ?? validationNotification;

        //    bool isValid = currentLanguageNotification == null || ((ValidationNotification)currentLanguageNotification).IsValid;

        //    return new ValidationResult(toValidationInstance, currentLanguageNotification, isValid);
        //}

        /// <summary>
        /// The validate using language.
        /// </summary>
        /// <param name="toValidationInstance">
        /// The to validation instance.
        /// </param>
        /// <param name="language">
        /// The language.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/>.
        /// </returns>
        public ValidationResult ValidateUsingLanguage(object toValidationInstance)
        {
            ValidationNotification validationNotification = null;

            var currentCulture = Thread.CurrentThread.CurrentCulture;
            var currentUiCulture = Thread.CurrentThread.CurrentUICulture;

            try
            {
               
                validationNotification = this.Validate(toValidationInstance);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
                Thread.CurrentThread.CurrentUICulture = currentUiCulture;
            }

            return CreateValidationResult(toValidationInstance, validationNotification);
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="toValidationInstance">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationNotification"/>.
        /// </returns>
        /// <exception cref="CommandValidationException">
        /// If the message is not valid the exception will be thrown
        /// </exception>
        public ValidationNotification Validate(object toValidationInstance)
        {
            if (toValidationInstance == null)
            {
                throw new ArgumentNullException("toValidationInstance");
            }

            if (this.logger == null)
            {
                this.logger = new NullLogger();
            }

            var messageType = toValidationInstance.GetMessageType();

            this.logger.Debug(() => "Unity Of Work Id: {0}".FormatWith(resolver.GetHashCode()));
            ValidationContextUnitOfWork.SetUnitOfWorkId(resolver);
            var spec = ValidationCatalog<ValidationContextUnitOfWork>.SpecificationContainer.TryGetSpecification(messageType);
            if (spec == null)
            {
                this.logger.Debug(() => string.Format("no specifications found for {0}", messageType.FullName));
                return new ValidationNotification();
            }

            this.logger.Debug(() => string.Format("Validating {0}", messageType.FullName));

            var validationResults = ValidationCatalog<ValidationContextUnitOfWork>.Validate(toValidationInstance);

            if (validationResults.IsValid)
            {
                this.logger.Debug(() => string.Format("Validation succeeded for message: {0}", messageType.FullName));
                return validationResults;
            }

            var errorMessage = new StringBuilder();
            errorMessage.Append(
                string.Format("Validation failed for message {0}, with the following error/s: " + Environment.NewLine, messageType.FullName));

            foreach (var validationResult in validationResults.Errors)
            {
                errorMessage.Append(string.Join(Environment.NewLine, validationResult.AllErrorMessages().ToArray()));
            }

            this.logger.Debug(this.ToErrorMessage(validationResults).ToString);

            return validationResults;
        }

        /// <summary>
        /// The to error message.
        /// </summary>
        /// <param name="validationNotification">
        /// The validation notification.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ToErrorMessage(ValidationNotification validationNotification)
        {
            var sb = new StringBuilder();
            foreach (var validationResult in validationNotification.Errors)
            {
                sb.Append(string.Join(Environment.NewLine, validationResult.AllErrorMessages().ToArray()));
            }

            return sb.ToString();
        }

        private ValidationResult CreateValidationResult(
            object toValidationInstance,           
            ValidationNotification validationNotification)
        {          
            return new ValidationResult( toValidationInstance, validationNotification, validationNotification.IsValid);
        }
    }
}
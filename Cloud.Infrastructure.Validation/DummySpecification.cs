﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DummySpecification.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using SpecExpress;

namespace Cloud.Infrastructure.Validation
{
    /// <summary>
    ///     Only required so that the scan returns one spec and does not throw an exception
    /// </summary>
    internal class DummySpecification : Validates<string>
    {
    }
}
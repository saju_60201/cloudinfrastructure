// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HandlerInvokerBase.cs" company="Ruf Informatik AG">
//   Copyright � Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Cloud.Infrastructure.Interception;
using Cloud.Infrastructure.Logging.Contracts;
using Cloud.Infrastructure.Validation.Contracts;

namespace Cloud.Infrastructure.Validation
{
    /// <summary>
    /// The handler invoker base.
    /// </summary>
    /// <typeparam name="T">
    /// The Handler type, which will be invoked to handle a result
    /// </typeparam>
    /// <typeparam name="TC">
    /// The Type of the result which should be handled
    /// </typeparam>
    public abstract class HandlerInvokerBase<T, TC>
        where T : class where TC : IHandledResult
    {
        private static List<T> handlers;

        private readonly Action<T, TC> handleAction;

        private readonly object syncObject = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="HandlerInvokerBase{T,TC}"/> class.
        ///     Initializes a new instance of the <see cref="BusinessExceptionHandlerInvoker"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="serviceLocator">
        /// The service Locator.
        /// </param>
        /// <param name="handleAction">
        /// The handle Action.
        /// </param>
        protected HandlerInvokerBase(ILogger logger, IServiceLocator serviceLocator, Action<T, TC> handleAction)
        {
            this.handleAction = handleAction;
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (serviceLocator == null)
            {
                throw new ArgumentNullException("serviceLocator");
            }

            if (handleAction == null)
            {
                throw new ArgumentNullException("handleAction");
            }

            this.Logger = logger;
            this.LoadHandlers(logger, serviceLocator);
        }

        private ILogger Logger { get; set; }

        /// <summary>
        /// Invoke the handlers
        ///     If one of the Handlers has handled the ValidationResult then
        ///     the exception will not be raised
        /// </summary>
        /// <param name="resultToHandle">
        /// The result To Handle.
        /// </param>
        public void InvokeHandlers(TC resultToHandle)
        {
            foreach (var businessExceptionHandler in handlers)
            {
                T handler = businessExceptionHandler;
                this.Logger.Debug(() => string.Format("Invoke handler {0}", handler.GetType().FullName));
                this.handleAction(businessExceptionHandler, resultToHandle);
                this.Logger.Debug(
                    () => string.Format("Is BusinessExceptionResult handled {0} {1}", handler.GetType().FullName, resultToHandle.IsHandled));
            }
        }

        private void LoadHandlers(ILogger logger, IServiceLocator serviceLocator)
        {
            lock (this.syncObject)
            {
                if (handlers != null)
                {
                    return;
                }

                var types = CurrentDomainTypes.GetTypesDerivingFrom<T>(CurrentDomainTypes.IsInstantiableNotGenericClass).ToList();

                logger.Debug(() => string.Format("Found {0} {1}", typeof(T).FullName, types.Count));

                if (types.Count == 0)
                {
                    logger.Warning(() => "No handlers found");
                    handlers = new List<T>();
                    return;
                }

                handlers = types.Select(type => (T)serviceLocator.Current.ResolveOrThrow(type)).ToList();
            }
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageValidationService.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Cloud.Infrastructure.Exceptions;
using Cloud.Infrastructure.Logging.Contracts;
using Cloud.Infrastructure.Validation.Contracts;
using SpecExpress;
using ValidationResult = Cloud.Infrastructure.Validation.Contracts.ValidationResult;

namespace Cloud.Infrastructure.Validation
{
    /// <summary>
    ///     The message validation service.
    /// </summary>
    public class MessageValidationService : IMessageValidationService
    {
        private readonly ICommandValidationFailedInvoker commandValidationFailedInvoker;

        // private readonly ILanguageSupport languageSupport;

        private readonly ILogger logger;

        private readonly IMessageValidationDisabler messageValidationDisabler;

        private readonly IMessageValidator messageValidator;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MessageValidationService" /> class.
        /// </summary>
        /// <param name="logger">
        ///     The logger.
        /// </param>
        /// <param name="messageValidationDisabler">
        ///     The message bus.
        /// </param>
        /// <param name="commandValidationFailedInvoker">
        ///     The command Validation Failed Invoker.
        /// </param>
        /// <param name="languageSupport">
        ///     The language support.
        /// </param>
        /// <param name="messageValidator">messageValidator </param>
        /// <exception cref="ArgumentNullException">
        ///     All arguments must not be null
        /// </exception>
        public MessageValidationService(
            ILogger logger,
            IMessageValidationDisabler messageValidationDisabler,
            ICommandValidationFailedInvoker commandValidationFailedInvoker,
            // ILanguageSupport languageSupport, 
            IMessageValidator messageValidator)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (messageValidationDisabler == null)
            {
                throw new ArgumentNullException("messageValidationDisabler");
            }

            if (commandValidationFailedInvoker == null)
            {
                throw new ArgumentNullException("commandValidationFailedInvoker");
            }

            //if (languageSupport == null)
            //{
            //    throw new ArgumentNullException("languageSupport");
            //}

            if (messageValidator == null)
            {
                throw new ArgumentNullException("messageValidator");
            }

            this.logger = logger;

            this.messageValidationDisabler = messageValidationDisabler;
            this.commandValidationFailedInvoker = commandValidationFailedInvoker;
            //this.languageSupport = languageSupport;
            this.messageValidator = messageValidator;
            Extensions.ScanCurrentDomain(logger);
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <param name="message">
        ///     The message.
        /// </param>
        /// <param name="throwExceptionIfNotValid">
        ///     The throw exception if not valid.
        /// </param>
        /// <returns>
        ///     The <see cref="Contracts.ValidationResult" />.
        /// </returns>
        public ValidationResult Validate(object message, bool throwExceptionIfNotValid = false)
        {
            if (messageValidationDisabler.IsValidationDisabled(message))
            {
                logger.Debug(() => "Validation for this command so go out here");

                return new ValidationResult(message, new ValidationNotification(), true);
            }

            var validationResult = messageValidator.ValidateUsingAllSupportedLanguages(message);

            if (!validationResult.IsValid)
            {
                commandValidationFailedInvoker.InvokeHandlers(validationResult);

                if (validationResult.IsHandled)
                {
                    logger.Debug(() => "Validation has been handled, so go out here without an exception");
                    return validationResult;
                }
            }

            if (!validationResult.IsValid && throwExceptionIfNotValid)
            {
                throw new CommandValidationException(string.Format("Command Validation Failed {0}", validationResult),
                    validationResult);
            }

            return validationResult;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidationContextUnitOfWork.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Cloud.Infrastructure.Interception;

    using SpecExpress;

    /// <summary>
    ///     The validation context unit of work.
    /// </summary>
    public class ValidationContextUnitOfWork : ValidationContext
    {
        #region Static Fields

        /// <summary>
        /// The found specifications.
        /// </summary>
        private static List<Type> foundSpecifications = null;

        /// <summary>
        /// The resolver internal.
        /// </summary>
        [ThreadStatic]
        private static IDependencyResolver resolverInternal;

        /// <summary>
        /// The unit of work specification container.
        /// </summary>
        [ThreadStatic]
        private static SpecificationContainer unitOfWorkSpecificationContainer;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ValidationContextUnitOfWork" /> class.
        /// </summary>
        public ValidationContextUnitOfWork()
        {
            this.FillContextContainer();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get found specifications count.
        /// </summary>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public static int GetFoundSpecificationsCount()
        {
            return foundSpecifications != null ? foundSpecifications.Count - 1 : 0;
        }

        /// <summary>
        /// The scan for specification.
        /// </summary>
        /// <param name="isForce">
        /// The is force.
        /// </param>
        public static void ScanForSpecification(bool isForce = false)
        {
            isForce = isForce || foundSpecifications == null;

            if (!isForce)
            {
                return;
            }

            var scanner = new SpecificationScanner();
            scanner.AddAssemblies(CurrentDomainTypes.GetAssemblies().ToList());

            var fieldSpecsFound = scanner.GetType().GetField("_specifications", BindingFlags.Instance | BindingFlags.NonPublic);
            foundSpecifications = (List<Type>)fieldSpecsFound.GetValue(scanner);

            if (foundSpecifications.Count == 0)
            {
                foundSpecifications.Add(typeof(DummySpecification));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The set unit of work id.
        /// </summary>
        /// <param name="resolver">
        /// The resolver.
        /// </param>
        internal static void SetUnitOfWorkId(IDependencyResolver resolver)
        {
            if (resolver != resolverInternal)
            {
                unitOfWorkSpecificationContainer = null;
            }

            resolverInternal = resolver;
        }

        /// <summary>
        /// The fill context container.
        /// </summary>
        private void FillContextContainer()
        {
            if (resolverInternal == null)
            {
                resolverInternal =
                    ServiceLocator.Instance.GetRootContainer().Resolve(typeof(IDependencyResolver), null) as IDependencyResolver;
            }

            var creator = new SpecificationCreator(resolverInternal);
            if (unitOfWorkSpecificationContainer != null)
            {
                this.SpecificationContainer.Add(unitOfWorkSpecificationContainer.GetAllSpecifications());
                return;
            }

            var instances = creator.CreateSpecifications(foundSpecifications);
            this.SpecificationContainer.Add(instances);
            unitOfWorkSpecificationContainer = this.SpecificationContainer;
        }

        #endregion
    }
}
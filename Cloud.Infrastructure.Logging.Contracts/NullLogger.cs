﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NullLogger.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Logging.Contracts
{
    using System;

    /// <summary>
    ///     The null logger.
    /// </summary>
    public class NullLogger : ILogger
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the root logger name.
        /// </summary>
        public string RootLoggerName { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        public void Debug(Func<string> message, string loggerName = null)
        {
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        public void Error(Func<string> message, string loggerName = null)
        {
        }

        /// <summary>
        /// Log an exception.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        public void Exception(Exception exception, string loggerName = null)
        {
        }

        /// <summary>
        /// The fatal.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        public void Fatal(Exception exception, string loggerName = null)
        {
        }

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        public void Info(Func<string> message, string loggerName = null)
        {
        }

        /// <summary>
        ///     The is logging enabled.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool IsLoggingEnabled()
        {
            return true;
        }

        /// <summary>
        /// The log.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="category">
        /// The category.
        /// </param>
        /// <param name="priority">
        /// The priority.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        public void Log(Func<string> message, LogLevel category, Priority priority, Exception exception = null, string loggerName = null)
        {
        }

        /// <summary>
        /// The log.
        /// </summary>
        /// <param name="logEvent">
        /// The log event.
        /// </param>
        public void Log(LogEvent logEvent)
        {
        }

        /// <summary>
        /// The warning.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        public void Warning(Func<string> message, string loggerName = null)
        {
        }

        #endregion
    }
}
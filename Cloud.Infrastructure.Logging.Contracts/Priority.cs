﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Priority.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Logging.Contracts
{
    /// <summary>
    ///     The priority.
    /// </summary>
    public enum Priority
    {
        /// <summary>
        ///     The none.
        /// </summary>
        None, 

        /// <summary>
        ///     The high.
        /// </summary>
        High, 

        /// <summary>
        ///     The medium.
        /// </summary>
        Medium, 

        /// <summary>
        ///     The low.
        /// </summary>
        Low
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILogger.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Logging.Contracts
{
    using System;

    /// <summary>
    ///     The Logger interface.
    /// </summary>
    public interface ILogger
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the root logger name.
        /// </summary>
        string RootLoggerName { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Debug(Func<string> message, string loggerName = null);

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Error(Func<string> message, string loggerName = null);

        /// <summary>
        /// Log an exception.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Exception(Exception exception, string loggerName = null);

        /// <summary>
        /// The fatal.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Fatal(Exception exception, string loggerName = null);

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Info(Func<string> message, string loggerName = null);

        /// <summary>
        ///     The is logging enabled.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        bool IsLoggingEnabled();

        /// <summary>
        /// The log.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="category">
        /// The category.
        /// </param>
        /// <param name="priority">
        /// The priority.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Log(Func<string> message, LogLevel category, Priority priority, Exception exception = null, string loggerName = null);

        /// <summary>
        /// The log.
        /// </summary>
        /// <param name="logEvent">
        /// The log event.
        /// </param>
        void Log(LogEvent logEvent);

        /// <summary>
        /// The warning.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Warning(Func<string> message, string loggerName = null);

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILoggerGeneric.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Logging.Contracts
{
    using System;

    /// <summary>
    /// The LoggerGeneric interface.
    /// </summary>
    /// <typeparam name="T">
    /// The item used as LogRecord resp. ExtendedItem
    /// </typeparam>
    public interface ILoggerGeneric<in T> : ILogger
        where T : class
    {
        #region Public Methods and Operators

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Debug(Func<string> message, T item = null, string loggerName = null);

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Error(Func<string> message, T item = null, string loggerName = null);

        /// <summary>
        /// Log an exception.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Exception(Exception exception, T item = null, string loggerName = null);

        /// <summary>
        /// The fatal.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Fatal(Exception exception, T item = null, string loggerName = null);

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Info(Func<string> message, T item = null, string loggerName = null);

        /// <summary>
        /// The warning.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="loggerName">
        /// The logger name.
        /// </param>
        void Warning(Func<string> message, T item = null, string loggerName = null);

        #endregion
    }
}
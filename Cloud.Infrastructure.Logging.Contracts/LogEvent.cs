﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogEvent.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Logging.Contracts
{
    /// <summary>
    ///     The log record.
    /// </summary>
    public class LogEvent : LogRecord<object>
    {
    }
}
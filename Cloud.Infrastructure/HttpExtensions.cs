﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;

    /// <summary>
    ///     Http Extensions.
    /// </summary>
    public static class HttpExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add authorization headers.
        /// </summary>
        /// <param name="httpClient">
        /// The http client.
        /// </param>
        /// <param name="authorizationScheme">
        /// The authorization scheme.
        /// </param>
        /// <param name="authorizationParameter">
        /// The authorization parameter.
        /// </param>
        public static void AddAuthorizationHeader(this HttpClient httpClient, string authorizationScheme, string authorizationParameter)
        {
            if (string.IsNullOrEmpty(authorizationScheme))
            {
                throw new ArgumentNullException("authorizationScheme");
            }

            if (string.IsNullOrEmpty(authorizationParameter))
            {
                throw new ArgumentNullException("authorizationParameter");
            }

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorizationScheme, authorizationParameter);
        }

        /// <summary>
        /// Returns the first value of the header or cookie
        /// </summary>
        /// <param name="headers">
        /// the headers
        /// </param>
        /// <param name="headerId">
        /// the theader id
        /// </param>
        /// <returns>
        /// the value of the header
        /// </returns>
        public static string GetCookieOrHeaderFirstValue(this HttpRequestHeaders headers, string headerId)
        {
            var cookie = headers.GetCookieOrHeaderFirstValue(headerId);
            if (cookie != null)
            {
                return cookie;
            }

            return headers.GetCustomHeaderFirst(headerId);
        }

        /// <summary>
        /// Returns a list of header values
        /// </summary>
        /// <param name="headers">
        /// the request headers
        /// </param>
        /// <param name="headerId">
        /// the header id
        /// </param>
        /// <returns>
        /// list of header values
        /// </returns>
        public static IEnumerable<string> GetCustomHeader(
            this IEnumerable<KeyValuePair<string, IEnumerable<string>>> headers, 
            string headerId)
        {
            if (headers == null)
            {
                return null;
            }

            return headers.FirstOrDefault(c => c.Key == headerId).Value;
        }

        /// <summary>
        /// Looks for the header and returns the first value
        /// </summary>
        /// <param name="headers">
        /// the headers
        /// </param>
        /// <param name="headerId">
        /// the headerid
        /// </param>
        /// <returns>
        /// the first value of the header
        /// </returns>
        public static string GetCustomHeaderFirst(this IEnumerable<KeyValuePair<string, IEnumerable<string>>> headers, string headerId)
        {
            var headerValues = headers.GetCustomHeader(headerId);
            if (headerValues == null)
            {
                return null;
            }

            return headerValues.FirstOrDefault();
        }

        #endregion
    }
}
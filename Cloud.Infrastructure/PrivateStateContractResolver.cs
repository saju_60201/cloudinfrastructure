﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PrivateStateContractResolver.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    /// <summary>
    ///     The private state contract resolver.
    /// </summary>
    public class PrivateStateContractResolver : DefaultContractResolver
    {
        #region Methods

        /// <summary>
        /// The create property.
        /// </summary>
        /// <param name="member">
        /// The member.
        /// </param>
        /// <param name="memberSerialization">
        /// The member serialization.
        /// </param>
        /// <returns>
        /// The <see cref="JsonProperty"/>.
        /// </returns>
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var prop = base.CreateProperty(member, memberSerialization);

            if (!prop.Writable)
            {
                var property = member as PropertyInfo;
                if (property != null)
                {
                    prop.Writable = property.CanWrite;
                }
                else
                {
                    var field = member as FieldInfo;
                    if (field != null)
                    {
                        prop.Writable = true;
                    }
                }
            }

            if (!prop.Readable)
            {
                var field = member as FieldInfo;
                if (field != null)
                {
                    prop.Readable = true;
                }
            }

            return prop;
        }

        /// <summary>
        /// The get serializable members.
        /// </summary>
        /// <param name="objectType">
        /// The object type.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        protected override List<MemberInfo> GetSerializableMembers(Type objectType)
        {
            const BindingFlags BindingFlags =
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | System.Reflection.BindingFlags.Static;
            var properties = objectType.GetProperties(BindingFlags);
            var fields = objectType.GetFields(BindingFlags);

            var allMembers = properties.Cast<MemberInfo>().Union(fields);
            return allMembers.ToList();
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Image.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    /// <summary>
    ///     The image.
    /// </summary>
    public class Image
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the base 64 payload.
        /// </summary>
        public string Base64Payload { get; set; }

        /// <summary>
        ///     Gets or sets the image type.
        /// </summary>
        public ImageType ImageType { get; set; }

        #endregion
    }
}
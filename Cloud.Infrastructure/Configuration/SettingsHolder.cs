﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsHolder.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Configuration
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    ///     The settings holder.
    /// </summary>
    public class SettingsHolder
    {
        #region Fields

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly ConcurrentDictionary<string, object> settings = new ConcurrentDictionary<string, object>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <typeparam name="T">
        /// The property lambda expression () =&gt; this.MyProperty
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T Get<T>(Expression<Func<T>> property)
        {
            var key = GetPropertyName(property);

            return this.Get<T>(key);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <typeparam name="T">
        /// Type of the setting to return
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T Get<T>(string key)
        {
            T val;
            if (this.TryGet(key, out val))
            {
                return val;
            }

            return default(T);
        }

        /// <summary>
        /// The get settings.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <typeparam name="T">
        /// Type of the setting to return
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T GetSettings<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return default(T);
            }

            object setting = null;
            T targetSetting = default(T);
            if (this.settings.TryGetValue(key, out setting))
            {
                if (setting is T)
                {
                    targetSetting = (T)setting;
                }
            }

            return targetSetting;
        }

        /// <summary>
        /// The has setting.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasSetting(string key)
        {
            return !string.IsNullOrEmpty(key) && this.settings.ContainsKey(key);
        }

        /// <summary>
        ///     The has setting.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of the setting
        /// </typeparam>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool HasSetting<T>()
        {
            return this.HasSetting(typeof(T).FullName);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <param name="val">
        /// The val.
        /// </param>
        /// <typeparam name="T">
        /// Type of the setting to return
        /// </typeparam>
        public void Set<T>(Expression<Func<T>> property, T val)
        {
            var key = GetPropertyName(property);
            this.Set(key, val);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="val">
        /// The val.
        /// </param>
        /// <typeparam name="T">
        /// Type of the setting to return
        /// </typeparam>
        public void Set<T>(T val)
        {
            this.settings[typeof(T).FullName] = val;
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="val">
        /// The val.
        /// </param>
        /// <typeparam name="T">
        /// Type of the setting to return
        /// </typeparam>
        /// <exception cref="ArgumentNullException">
        /// Key must not be empty
        /// </exception>
        public void Set<T>(string key, T val)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            this.settings[key] = val;
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Key must not be empty
        /// </exception>
        public void Set(string key, object value)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            this.settings[key] = value;
        }

        /// <summary>
        /// The try get.
        /// </summary>
        /// <param name="val">
        /// The val.
        /// </param>
        /// <typeparam name="T">
        /// Type of the setting to return
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TryGet<T>(out T val)
        {
            return this.TryGet(typeof(T).FullName, out val);
        }

        /// <summary>
        /// The try get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="val">
        /// The val.
        /// </param>
        /// <typeparam name="T">
        /// Type of the setting to return
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TryGet<T>(string key, out T val)
        {
            bool keyExists = false;
            return this.TryGetInternal(key, out val, out keyExists);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get property name.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        private static string GetPropertyName<T>(Expression<Func<T>> property)
        {
            var propertyInfo = (property.Body as MemberExpression).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
            }

            return propertyInfo.Name;
        }

        /// <summary>
        /// The try get internal.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="val">
        /// The val.
        /// </param>
        /// <param name="keyExists">
        /// The key exists.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        private bool TryGetInternal<T>(string key, out T val, out bool keyExists)
        {
            keyExists = false;
            val = default(T);

            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            object setting = null;
            if (this.settings.TryGetValue(key, out setting))
            {
                keyExists = true;
                if (setting is T)
                {
                    val = (T)setting;
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
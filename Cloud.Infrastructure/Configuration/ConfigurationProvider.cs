﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationProvider.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Configuration
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    ///     The configuration provider.
    /// </summary>
    public class ConfigurationProvider
    {
        #region Fields

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly ConcurrentDictionary<string, SettingsHolder> settings = new ConcurrentDictionary<string, SettingsHolder>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of the Configuration
        /// </typeparam>
        /// <returns>
        ///     The <see cref="T" />.
        /// </returns>
        public T Get<T>() where T : ConfigurationBase
        {
            var foundSettings = this.GetSettings(typeof(T).FullName, true);
            if (foundSettings != null)
            {
                return this.CreateConfigurationInstance<T>();
            }

            return null;
        }

        /// <summary>
        ///     The has configuration.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of the Configuration
        /// </typeparam>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool HasConfiguration<T>() where T : ConfigurationBase
        {
            return this.Get<T>() != null;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create settings.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        /// <returns>
        /// The <see cref="SettingsHolder"/>.
        /// </returns>
        internal SettingsHolder CreateSettings(ConfigurationBase configuration)
        {
            var key = configuration.GetType().FullName;
            SettingsHolder settingsHolder = this.GetSettings(key, false);

            if (settingsHolder != null)
            {
                return settingsHolder;
            }

            settingsHolder = new SettingsHolder();

            while (true)
            {
                if (this.settings.TryAdd(key, settingsHolder))
                {
                    return settingsHolder;
                }

                SettingsHolder foundSettings = this.GetSettings(key, false);
                if (foundSettings != null)
                {
                    return foundSettings;
                }

                Thread.Sleep(TimeSpan.FromMilliseconds(200));
            }
        }

        /// <summary>
        ///     The create configuration instance.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        ///     The <see cref="T" />.
        /// </returns>
        private T CreateConfigurationInstance<T>()
        {
            return (T)Activator.CreateInstance(typeof(T), new object[] { this });
        }

        /// <summary>
        /// The get settings.
        /// </summary>
        /// <param name="settingCategory">
        /// The setting category.
        /// </param>
        /// <param name="raiseException">
        /// The raise exception.
        /// </param>
        /// <returns>
        /// The <see cref="SettingsHolder"/>.
        /// </returns>
        /// <exception cref="KeyNotFoundException">
        /// </exception>
        private SettingsHolder GetSettings(string settingCategory, bool raiseException)
        {
            SettingsHolder configuration;
            if (this.settings.TryGetValue(settingCategory, out configuration))
            {
                return configuration;
            }

            if (raiseException)
            {
                throw new KeyNotFoundException(string.Format("Configuration with key {0} not found", settingCategory));
            }

            return null;
        }

        #endregion
    }
}
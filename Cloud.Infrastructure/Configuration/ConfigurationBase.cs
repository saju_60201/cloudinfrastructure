﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationBase.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Configuration
{
    /// <summary>
    ///     The configuration base.
    /// </summary>
    public abstract class ConfigurationBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationBase"/> class.
        /// </summary>
        /// <param name="configurationProvider">
        /// The configuration provider.
        /// </param>
        protected ConfigurationBase(ConfigurationProvider configurationProvider)
        {
            this.Settings = configurationProvider.CreateSettings(this);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the settings.
        /// </summary>
        protected SettingsHolder Settings { get; private set; }

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileHelper.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Utility
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading;

    /// <summary>
    ///     The file helper.
    /// </summary>
    public class FileHelper
    {
        #region Public Methods and Operators

        /// <summary>
        /// Compares the specified files to verify if they are equal.
        /// </summary>
        /// <param name="file1">
        /// Full path to file1.
        /// </param>
        /// <param name="file2">
        /// Full path to file2.
        /// </param>
        /// <returns>
        /// Returns true if files are equal otherwise false.
        /// </returns>
        public static bool Compare(string file1, string file2)
        {
            if (string.IsNullOrEmpty(file1))
            {
                throw new ArgumentNullException("file1");
            }

            if (string.IsNullOrEmpty(file2))
            {
                throw new ArgumentNullException("file2");
            }

            // Determine if the same file was referenced two times.
            if (file1 == file2)
            {
                // Return true to indicate that the files are the same.
                return true;
            }

            // Open the two files.
            using (var fs1 = new FileStream(file1, FileMode.Open, FileAccess.Read))
            {
                using (var fs2 = new FileStream(file2, FileMode.Open, FileAccess.Read))
                {
                    // Check the file sizes. If they are not the same, the files 
                    // are not the same.
                    if (fs1.Length != fs2.Length)
                    {
                        // Close the file
                        fs1.Close();
                        fs2.Close();

                        // Return false to indicate files are different
                        return false;
                    }

                    // Read and compare a byte from each file until either a
                    // non-matching set of bytes is found or until the end of
                    // file1 is reached.
                    int file1Byte;
                    int file2Byte;
                    do
                    {
                        // Read one byte from each file.
                        file1Byte = fs1.ReadByte();
                        file2Byte = fs2.ReadByte();
                    }
                    while ((file1Byte == file2Byte) && (file1Byte != -1));

                    // Close the files.
                    fs1.Close();
                    fs2.Close();

                    // Return the success of the comparison. "file1byte" is 
                    // equal to "file2byte" at this point only if the files are 
                    // the same.
                    return (file1Byte - file2Byte) == 0;
                }
            }
        }

        /// <summary>
        /// Compares the specified files by assembly version to verify if they are equal.
        /// </summary>
        /// <param name="file1">
        /// Full path to file1.
        /// </param>
        /// <param name="file2">
        /// Full path to file2.
        /// </param>
        /// <returns>
        /// Returns true if files are equal otherwise false.
        /// </returns>
        public static bool CompareByVersion(string file1, string file2)
        {
            if (string.IsNullOrEmpty(file1))
            {
                throw new ArgumentNullException("file1");
            }

            if (string.IsNullOrEmpty(file2))
            {
                throw new ArgumentNullException("file2");
            }

            if (!File.Exists(file1))
            {
                return false;
            }

            var file1AssemblyVersion = AssemblyName.GetAssemblyName(file1).Version;

            if (!File.Exists(file2))
            {
                return false;
            }

            var file2AssemblyVersion = AssemblyName.GetAssemblyName(file2).Version;

            // Determine if the same file was referenced two times.
            if (file1AssemblyVersion.CompareTo(file2AssemblyVersion) == 0)
            {
                // Return true to indicate that the files have the same version.
                return true;
            }

            return false;
        }

        /// <summary>
        /// Downloads the file ressource.
        /// </summary>
        /// <param name="resourceName">
        /// Name of the resource.
        /// </param>
        /// <param name="ressourceAssembly">
        /// The ressource assembly.
        /// </param>
        /// <param name="location">
        /// The location.
        /// </param>
        /// <returns>
        /// Path to the downloaded ressource.
        /// </returns>
        public static string DownloadFileRessource(string resourceName, Assembly ressourceAssembly, Uri location)
        {
            var stream = GetFileStream(resourceName, ressourceAssembly);

            using (var fileStream = new FileStream(location.AbsolutePath, FileMode.Create))
            {
                stream.CopyTo(fileStream);
            }

            return location.AbsolutePath;
        }

        /// <summary>
        /// Gets the file stream from ressource.
        /// </summary>
        /// <param name="resourceName">
        /// Name of the resource.
        /// </param>
        /// <param name="ressourceAssembly">
        /// The ressource assembly.
        /// </param>
        /// <returns>
        /// The FileStream.
        /// </returns>
        public static Stream GetFileStream(string resourceName, Assembly ressourceAssembly)
        {
            var stream = ressourceAssembly.GetManifestResourceStream(resourceName);

            if (stream == null)
            {
                throw new InvalidOperationException("The ressource could not be found.");
            }

            return stream;
        }

        /// <summary>
        /// Gets the relative path.
        /// </summary>
        /// <param name="fromPath">
        /// From path.
        /// </param>
        /// <param name="toPath">
        /// To path.
        /// </param>
        /// <returns>
        /// The relative path to toPath.
        /// </returns>
        public static string GetRelativePath(string fromPath, string toPath)
        {
            var fromUri = new Uri(fromPath);
            var toUri = new Uri(toPath);
            var relativeUri = fromUri.MakeRelativeUri(toUri);

            return Uri.UnescapeDataString(relativeUri.ToString().Replace('/', Path.DirectorySeparatorChar));
        }

        /// <summary>
        /// The delay file locked.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="delayMilliseconds">
        /// The delay Milliseconds.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsFileLocked(string filePath, int delayMilliseconds = 1000)
        {
            Thread.Sleep(delayMilliseconds);
            var file = new FileInfo(filePath);

            try
            {
                using (file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    return false;
                }
            }
            catch (IOException)
            {
                // the file is unavailable because it is:
                // still being written to
                // or being processed by another thread
                // or does not exist (has already been processed)
                return true;
            }
        }

        /// <summary>
        /// The delete and move.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="destination">
        /// The destination.
        /// </param>
        /// <param name="overwrite">
        /// The overwrite.
        /// </param>
        public static void Move(string source, string destination, bool overwrite = true)
        {
            if (overwrite && File.Exists(destination))
            {
                File.Delete(destination);
            }

            File.Move(source, destination);
        }

        #endregion
    }
}
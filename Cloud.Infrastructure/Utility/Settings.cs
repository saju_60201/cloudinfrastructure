// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Settings.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Utility
{
    using System;
    using System.Configuration;

    /// <summary>
    ///     The settings.
    /// </summary>
    public static class Settings
    {
        #region Constants

        /// <summary>
        ///     The system user.
        /// </summary>
        public const string SystemUser = "System";

        #endregion

        /// <summary>
        ///     The application.
        /// </summary>
        public static class Application
        {
            #region Public Properties

            /// <summary>
            ///     Gets the ech message geres temp path.
            /// </summary>
            public static string EchMessageGeresTempPath
            {
                get
                {
                    return ConfigurationManager.AppSettings["EchMessageGeresTempPath"];
                }
            }

            /// <summary>
            ///     Gets the ech message handled inbox temp path.
            /// </summary>
            public static string EchMessageHandledInboxTempPath
            {
                get
                {
                    return ConfigurationManager.AppSettings["EchMessageHandledInboxTempPath"];
                }
            }

            /// <summary>
            ///     Gets the ech message inbox path.
            /// </summary>
            public static string EchMessageInboxPath
            {
                get
                {
                    return ConfigurationManager.AppSettings["EchMessageInboxPath"];
                }
            }

            /// <summary>
            ///     Gets the ech message outbox path.
            /// </summary>
            public static string EchMessageOutboxPath
            {
                get
                {
                    return ConfigurationManager.AppSettings["EchMessageOutboxPath"];
                }
            }

            /// <summary>
            ///     Gets the ech message receipts path.
            /// </summary>
            public static string EchMessageReceiptsPath
            {
                get
                {
                    return ConfigurationManager.AppSettings["EchMessageReceiptsPath"];
                }
            }

            /// <summary>
            ///     Gets the ech message receiver community temp path.
            /// </summary>
            public static string EchMessageReceiverCommunityTempPath
            {
                get
                {
                    return ConfigurationManager.AppSettings["EchMessageReceiverCommunityTempPath"];
                }
            }

            /// <summary>
            ///     Gets the ech message sedex temp message.
            /// </summary>
            public static string EchMessageSedexTempMessage
            {
                get
                {
                    return ConfigurationManager.AppSettings["EchMessageSedexTempMessage"];
                }
            }

            /// <summary>
            ///     Gets the system message count.
            /// </summary>
            public static int SystemMessageCount
            {
                get
                {
                    return Convert.ToInt32(ConfigurationManager.AppSettings["SystemMessageCount"]);
                }
            }

            #endregion
        }
    }
}
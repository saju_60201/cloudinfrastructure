// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectXmlSerializer.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Utility
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;
    using System.Xml.XPath;
    using System.Xml.Xsl;

    using Cloud.Infrastructure.Contracts;

    /// <summary>
    ///     The object xml serializer.
    /// </summary>
    public static class ObjectXmlSerializer
    {
        #region Public Methods and Operators

        /// <summary>
        /// The apply xslt.
        /// </summary>
        /// <param name="serializableObject">
        /// The serializable object.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ApplyXslt<T>(T serializableObject, string filePath) where T : class
        {
            var doc = new XPathDocument(GetXmlStream(serializableObject.GetType(), serializableObject, null));

            // Load the style sheet.
            var xslt = new XslCompiledTransform();
            xslt.Load(filePath);
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new XmlTextWriter(memoryStream, Encoding.UTF8))
                {
                    xslt.Transform(doc, writer);
                    memoryStream.Position = 0;
                    using (var streamReader = new StreamReader(memoryStream))
                    {
                        var strHtml = streamReader.ReadToEnd();
                        streamReader.Close();
                        memoryStream.Close();
                        return strHtml;
                    }
                }
            }
        }

        /// <summary>
        /// The format xml.
        /// </summary>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string FormatXml(string xml)
        {
            using (var reader = XmlReader.Create(new StringReader(xml), new XmlReaderSettings { IgnoreComments = true }))
            {
                using (var stringWriter = new StringWriter())
                {
                    using (
                        var xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true }))
                    {
                        var document = new XmlDocument();
                        document.Load(reader);
                        document.Save(xmlWriter);
                        return stringWriter.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// The get object.
        /// </summary>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T GetObject<T>(string xml, string path, XmlAttributeOverrides attributeOverrides = null) where T : Configurable
        {
            var data = GetObject(typeof(T), xml, attributeOverrides) as T;
            if (data != null)
            {
                data.Initialize(path);
            }

            return data;
        }

        /// <summary>
        /// The get object.
        /// </summary>
        /// <param name="t">
        /// The t.
        /// </param>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object GetObject(Type t, string xml, XmlAttributeOverrides attributeOverrides = null)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }

            var xs = new XmlSerializer(t, attributeOverrides);
            var memoryStream = new MemoryStream(new UTF8Encoding().GetBytes(xml));
            return xs.Deserialize(memoryStream);
        }

        /// <summary>
        /// The get xml.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="serializableObject">
        /// The serializable object.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetXml(Type type, object serializableObject, XmlAttributeOverrides attributeOverrides = null)
        {
            using (var streamReader = new StreamReader(GetXmlStream(type, serializableObject, attributeOverrides), Encoding.UTF8))
            {
                return streamReader.ReadToEnd();
            }
        }

        /// <summary>
        /// The get xml.
        /// </summary>
        /// <param name="serializableObject">
        /// The serializable object.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetXml<T>(T serializableObject, XmlAttributeOverrides attributeOverrides = null) where T : class
        {
            return GetXml(serializableObject.GetType(), serializableObject, attributeOverrides);
        }

        /// <summary>
        /// The load.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T Load<T>(Stream stream) where T : class
        {
            using (var textReader = new StreamReader(stream))
            {
                return new XmlSerializer(typeof(T)).Deserialize(textReader) as T;
            }
        }

        /// <summary>
        /// The load.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T Load<T>(string path) where T : class
        {
            return LoadFromDocumentFormat<T>(path, null);
        }

        /// <summary>
        /// The load.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T Load<T>(string path, XmlAttributeOverrides attributeOverrides) where T : Configurable
        {
            var data = LoadFromDocumentFormat<T>(path, attributeOverrides);
            data.Initialize(path);
            return data;
        }

        /// <summary>
        /// The load from xml string.
        /// </summary>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T LoadFromXmlString<T>(string xml) where T : class
        {
            return LoadFromDocumentFormatFromXmlString<T>(xml, null);
        }

        /// <summary>
        /// The load with xml.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T LoadWithXml<T>(string path, out string xml) where T : class
        {
            return LoadFromDocumentFormat<T>(path, out xml, null);
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="serializableObject">
        /// The serializable object.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        public static void Save<T>(T serializableObject, string path, XmlAttributeOverrides attributeOverrides = null) where T : class
        {
            SaveToDocumentFormat(serializableObject, path, attributeOverrides);
        }

        /// <summary>
        /// The save and get xml.
        /// </summary>
        /// <param name="serializableObject">
        /// The serializable object.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string SaveAndGetXml<T>(T serializableObject, string path, XmlAttributeOverrides attributeOverrides = null)
            where T : class
        {
            SaveToDocumentFormat(serializableObject, path, attributeOverrides);
            var xml = GetXml(serializableObject.GetType(), serializableObject, attributeOverrides);
            return FormatXml(xml);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get xml stream.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="serializableObject">
        /// The serializable object.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <returns>
        /// The <see cref="MemoryStream"/>.
        /// </returns>
        private static MemoryStream GetXmlStream(Type type, object serializableObject, XmlAttributeOverrides attributeOverrides)
        {
            var xs = new XmlSerializer(type, attributeOverrides);
            var memoryStream = new MemoryStream();
            var xtWriter = new XmlTextWriter(memoryStream, Encoding.UTF8) { Formatting = Formatting.Indented };
            xs.Serialize(xtWriter, serializableObject);
            memoryStream.Position = 0;
            return memoryStream;
        }

        /// <summary>
        /// The load from document format.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        private static T LoadFromDocumentFormat<T>(string path, XmlAttributeOverrides attributeOverrides) where T : class
        {
            using (var textReader = new StreamReader(path))
            {
                return new XmlSerializer(typeof(T), attributeOverrides).Deserialize(textReader) as T;
            }
        }

        /// <summary>
        /// The load from document format.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        private static T LoadFromDocumentFormat<T>(string path, out string xml, XmlAttributeOverrides attributeOverrides) where T : class
        {
            var xmlText = File.ReadAllText(path);
            xml = FormatXml(xmlText);
            using (var stream = new StringReader(xmlText))
            {
                return new XmlSerializer(typeof(T), attributeOverrides).Deserialize(stream) as T;
            }
        }

        /// <summary>
        /// The load from document format from xml string.
        /// </summary>
        /// <param name="xml">
        /// The xml.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        private static T LoadFromDocumentFormatFromXmlString<T>(string xml, XmlAttributeOverrides attributeOverrides) where T : class
        {
            using (var stream = new StringReader(xml))
            {
                return new XmlSerializer(typeof(T), attributeOverrides).Deserialize(stream) as T;
            }
        }

        /// <summary>
        /// The save to document format.
        /// </summary>
        /// <param name="serializableObject">
        /// The serializable object.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="attributeOverrides">
        /// The attribute overrides.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        private static void SaveToDocumentFormat<T>(T serializableObject, string path, XmlAttributeOverrides attributeOverrides)
            where T : class
        {
            using (var textWriter = new StreamWriter(path))
            {
                var xmlSerializer = new XmlSerializer(serializableObject.GetType(), attributeOverrides);
                xmlSerializer.Serialize(textWriter, serializableObject);
            }
        }

        #endregion
    }
}
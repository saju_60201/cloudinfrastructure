﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Mapper.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Utility
{
    using System;
    using System.Reflection;

    /// <summary>
    ///     The mapper.
    /// </summary>
    public class Mapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The set object data dynamically.
        /// </summary>
        /// <param name="contract">
        /// The type.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="destination">
        /// The destination.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object Map(Type contract, object source, object destination)
        {
            if (source != null)
            {
                foreach (var prop in contract.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var sourceProperty = source.GetType().GetProperty(prop.Name);
                    if (sourceProperty != null)
                    {
                        destination.GetType().GetProperty(prop.Name).SetValue(destination, prop.GetValue(source, null), null);
                    }
                }
            }

            return destination;
        }

        /// <summary>
        /// The map.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="destination">
        /// The destination.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object Map(object source, object destination)
        {
            if (source != null)
            {
                foreach (var prop in source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var destinationProperty = destination.GetType().GetProperty(prop.Name);
                    if (destinationProperty != null)
                    {
                        destination.GetType().GetProperty(prop.Name).SetValue(destination, prop.GetValue(source, null), null);
                    }
                }
            }

            return destination;
        }

        #endregion
    }
}
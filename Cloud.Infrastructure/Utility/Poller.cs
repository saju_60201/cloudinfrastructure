﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Poller.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Utility
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    ///     Represents an generic Poller.
    /// </summary>
    public static class Poller
    {
        #region Public Methods and Operators

        /// <summary>
        /// Polls on each interval and executes the specified function.
        /// </summary>
        /// <typeparam name="TResult">
        /// The type of Result expected from the progress update.
        /// </typeparam>
        /// <param name="executeOnPoll">
        /// The function to execute.
        /// </param>
        /// <param name="executeOnException">
        /// The execute on exception.
        /// </param>
        /// <param name="interval">
        /// The interval of polls.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress updater.
        /// </param>
        /// <returns>
        /// Returns the poll task.
        /// </returns>
        public static async Task PollAsync<TResult>(
            Func<CancellationToken, TResult> executeOnPoll, 
            Action<AggregateException> executeOnException, 
            TimeSpan interval, 
            CancellationToken cancellationToken, 
            IProgress<TResult> progress = null)
        {
            while (true)
            {
                var task = Task.Run(() => executeOnPoll.Invoke(cancellationToken), cancellationToken);

                try
                {
                    var taskResult = await task;

                    if (progress != null)
                    {
                        progress.Report(taskResult);
                    }
                }
                catch (AggregateException ex)
                {
                    executeOnException.Invoke(ex);
                }
                catch (Exception)
                {
                    executeOnException.Invoke(task.Exception);
                }

                await Task.Delay(interval, cancellationToken);
            }
        }

        #endregion
    }
}
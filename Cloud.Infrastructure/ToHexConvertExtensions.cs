﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToHexConvertExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     The fast.
    /// </summary>
    public static class ToHexConvertExtensions
    {
        #region Static Fields

        /// <summary>
        ///     The lookup 32.
        /// </summary>
        private static readonly uint[] Lookup32 = CreateLookup32();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The byte array to hex via lookup 32.
        /// </summary>
        /// <param name="bytes">
        /// The bytes.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1407:ArithmeticExpressionsMustDeclarePrecedence", 
            Justification = "Reviewed. Suppression is OK here.")]
        public static string ByteArrayToHexViaLookup32(this byte[] bytes)
        {
            var lookup32 = Lookup32;
            var result = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                var val = lookup32[bytes[i]];
                result[2 * i] = (char)val;
                result[2 * i + 1] = (char)(val >> 16);
            }

            return new string(result);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The create lookup 32.
        /// </summary>
        /// <returns>
        ///     The <see cref="uint[]" />.
        /// </returns>
        private static uint[] CreateLookup32()
        {
            var result = new uint[256];
            for (int i = 0; i < 256; i++)
            {
                string s = i.ToString("X2");
                result[i] = ((uint)s[0]) + ((uint)s[1] << 16);
            }

            return result;
        }

        #endregion
    }
}
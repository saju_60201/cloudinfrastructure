﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageHeaders.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    /// <summary>
    ///     The bus headers.
    /// </summary>
    public static class MessageHeaders
    {
        #region Constants

        /// <summary>
        ///     The culture id token.
        /// </summary>
        public const string CultureIdToken = "user-culture";

        /// <summary>
        ///     The claimns headerid.
        /// </summary>
        public const string ClaimnsHeaderid = "ClaimsPrincipalToken";

        /// <summary>
        ///     The authentication schema
        /// </summary>
        public const string AuthSchema = "AuthorizationSchema";

        /// <summary>
        ///     The authentication parameter
        /// </summary>
        public const string AuthParameter = "AuthorizationParameter";

        /// <summary>
        ///     The is publi web control message.
        /// </summary>
        public const string IsPubliWebControlMessage = "IsPubliWebControlMessage";
       
        /// <summary>
        ///     The session id.
        /// </summary>
        public const string SessionId = "SessionId";

        /// <summary>
        ///     Ignore State and Command Validation
        /// </summary>
        public const string IgnoreValidation = "IgnoreValidation";

        #endregion
    }
}
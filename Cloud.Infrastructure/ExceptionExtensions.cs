﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System;
    using System.Reflection;
    using System.Text;

    /// <summary>
    ///     The exception extensions.
    /// </summary>
    public static class ExceptionExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get exception details.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="delimitter">
        /// The delimitter.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetExceptionDetails(this Exception exception, string delimitter = null)
        {
            var sb = new StringBuilder();
            if (exception == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(delimitter))
            {
                delimitter = Environment.NewLine;
            }

            sb.AppendLine(string.Format("{0}{1}{2}", exception.Message, delimitter, exception.StackTrace));

            var aggregateException = exception as AggregateException;
            if (aggregateException != null)
            {
                foreach (var innerException in aggregateException.InnerExceptions)
                {
                    sb.AppendLine("------- AggregateException InnerExceptions -------");
                    sb.AppendLine(GetExceptionDetails(innerException, delimitter));
                }
            }

            var reflectionTypeLoadException = exception as ReflectionTypeLoadException;
            if (reflectionTypeLoadException != null)
            {
                foreach (var loaderException in reflectionTypeLoadException.LoaderExceptions)
                {
                    sb.AppendLine("------- ReflectionTypeLoadException LoaderExceptions -------");
                    sb.AppendLine(GetExceptionDetails(loaderException, delimitter));
                }
            }

            if (exception.InnerException == null)
            {
                return sb.ToString();
            }

            sb.AppendLine("------- Innerexception -------");
            return sb + delimitter + GetExceptionDetails(exception.InnerException, delimitter);
        }

        #endregion
    }
}
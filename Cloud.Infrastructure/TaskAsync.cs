﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskAsync.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    /// <summary>
    ///     Uses in Background the Task class, but copies always the HttpContext.Current
    /// </summary>
    public class TaskAsync
    {
        #region Public Methods and Operators

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task Run(Action action)
        {
            return Task.Run(GetAction(action));
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task Run(Action action, CancellationToken cancellationToken)
        {
            return Task.Run(GetAction(action), cancellationToken);
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        /// <typeparam name="TResult">
        /// type of result
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task<TResult> Run<TResult>(Func<TResult> function)
        {
            return Task<TResult>.Run(GetFunction(function));
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <typeparam name="TResult">
        /// type of result
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task<TResult> Run<TResult>(Func<TResult> function, CancellationToken cancellationToken)
        {
            return Task<TResult>.Run(GetFunction(function), cancellationToken);
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task Run(Func<Task> function)
        {
            return Task.Run(GetFunction(function));
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task Run(Func<Task> function, CancellationToken cancellationToken)
        {
            return Task.Run(GetFunction(function), cancellationToken);
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        /// <typeparam name="TResult">
        /// type of result
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task<TResult> Run<TResult>(Func<Task<TResult>> function)
        {
            return Task<TResult>.Run(GetFunction(function));
        }

        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <typeparam name="TResult">
        /// type of result
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task<TResult> Run<TResult>(Func<Task<TResult>> function, CancellationToken cancellationToken)
        {
            return Task<TResult>.Run(GetFunction(function), cancellationToken);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get action.
        /// </summary>
        /// <param name="toRun">
        /// The to run.
        /// </param>
        /// <returns>
        /// The <see cref="Action"/>.
        /// </returns>
        private static Action GetAction(Action toRun)
        {
            var context = HttpContext.Current;
            return delegate
                {
                    HttpContext.Current = context;
                    toRun();
                };
        }

        /// <summary>
        /// The get function.
        /// </summary>
        /// <param name="toRun">
        /// The to run.
        /// </param>
        /// <typeparam name="TResult">
        /// </typeparam>
        /// <returns>
        /// The <see cref="Func"/>.
        /// </returns>
        private static Func<TResult> GetFunction<TResult>(Func<TResult> toRun)
        {
            var context = HttpContext.Current;
            return delegate
                {
                    HttpContext.Current = context;
                    return toRun();
                };
        }

        #endregion
    }
}
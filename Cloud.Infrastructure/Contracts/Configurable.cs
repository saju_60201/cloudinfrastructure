// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Configurable.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Contracts
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    ///     The configurable.
    /// </summary>
    [Serializable]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public abstract class Configurable
    {
        #region Public Methods and Operators

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        public abstract void Initialize(string file);

        /// <summary>
        ///     The prepare compare.
        /// </summary>
        public virtual void PrepareCompare()
        {
        }

        #endregion

        // public virtual void Update(PackageInformation packageInformation, string file) { }
    }

    /// <summary>
    ///     The report ignore attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class ReportIgnoreAttribute : Attribute
    {
    }
}
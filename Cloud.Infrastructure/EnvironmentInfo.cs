﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnvironmentInfo.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System;
    using System.Reflection;

    /// <summary>
    ///     The environment info.
    /// </summary>
    public static class EnvironmentInfo
    {
        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether is delevopment machine.
        /// </summary>
        public static bool IsDelevopmentMachine
        {
            get
            {
                var devEnv = (Environment.GetEnvironmentVariable("IsDevelopmentMachine") ?? string.Empty).ToLowerInvariant();
                bool isDevMachine = devEnv == "true" || devEnv == "1";

                var domainDns = (Environment.GetEnvironmentVariable("USERDNSDOMAIN") ?? string.Empty).ToUpperInvariant();
                var domain = (Environment.GetEnvironmentVariable("USERDOMAIN") ?? string.Empty).ToUpperInvariant();

                domainDns =
                    (string.IsNullOrEmpty(domainDns)
                         ? System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName
                         : domainDns).ToUpperInvariant();

                bool isDomainOk = domainDns.Equals("RI.RUF.GROUP", StringComparison.InvariantCultureIgnoreCase)
                                  || domainDns.Equals("RUF.GROUP", StringComparison.InvariantCultureIgnoreCase)
                                  || domainDns.Contains("SELISE") || domain.Equals("CH-RUF", StringComparison.InvariantCultureIgnoreCase)
                                  || domain.Contains("SELISE");

                return isDevMachine && isDomainOk;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get entry assembly.
        /// </summary>
        /// <returns>
        ///     The <see cref="Assembly" />.
        /// </returns>
        public static Assembly GetEntryAssembly()
        {
            var entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly != null)
            {
                return entryAssembly;
            }

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.ApplicationInstance != null)
            {
                var type = System.Web.HttpContext.Current.ApplicationInstance.GetType();
                while (type != null && type.Namespace == "ASP")
                {
                    type = type.BaseType;
                }

                return type == null ? null : type.Assembly;
            }

            return null;
        }

        #endregion
    }
}
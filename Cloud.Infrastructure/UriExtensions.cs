﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UriExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Cloud.Infrastructure.Utility;

    /// <summary>
    ///     Extensions
    /// </summary>
    public static class UriExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Checks if the URI exists.
        ///     Be aware this method currently only support checks for the scheme types "file" and "http".
        /// </summary>
        /// <param name="uri">
        /// The URI.
        /// </param>
        /// <returns>
        /// Returns true if the URI exists otherwise false.
        /// </returns>
        public static bool Exists(this Uri uri)
        {
            if (!uri.IsAbsoluteUri)
            {
                return false;
            }

            switch (uri.Scheme)
            {
                case "http":
                    return CheckHttpExistance(uri);
                case "file":
                    return CheckFileExistance(uri);
                default:
                    throw new InvalidOperationException(string.Format("This method doesn't support the uri scheme \"{0}\"", uri.Scheme));
            }
        }

        /// <summary>
        /// Checks if the response of this HTTP-URI is available.
        ///     Be aware that this method gets the whole response to check the availability.
        ///     Use this method over the method "Exists" if the response of the http uri doesn't provide a HEAD field.
        /// </summary>
        /// <param name="uri">
        /// The HTTP URI.
        /// </param>
        /// <returns>
        /// Returns true if the response of this HTTP URI is available otherwise false.
        /// </returns>
        public static bool HttpResponseAvailable(this Uri uri)
        {
            if (!string.Equals(uri.Scheme, "http"))
            {
                throw new InvalidOperationException(string.Format("This method doesn't support the uri scheme \"{0}\"", uri.Scheme));
            }

            if (!CheckNetworkAvailability(uri))
            {
                return false;
            }

            // 2. Check if http request succeeds
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(uri, HttpCompletionOption.ResponseHeadersRead).Result;

                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }

                return true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The check file existance.
        /// </summary>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CheckFileExistance(Uri uri)
        {
            return File.Exists(uri.LocalPath) || Directory.Exists(uri.LocalPath);
        }

        /// <summary>
        /// The check http existance.
        /// </summary>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CheckHttpExistance(Uri uri)
        {
            if (!CheckNetworkAvailability(uri))
            {
                return false;
            }

            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Head, uri);
                HttpResponseMessage response = client.SendAsync(request).Result;

                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// The check network availability.
        /// </summary>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CheckNetworkAvailability(Uri uri)
        {
            // 0. Check if network is available
            if (!NetworkHelper.IsNetworkAvailable())
            {
                return false;
            }

            // 1. Check if dns will resolve
            Task<IPHostEntry> dnsTask = Dns.GetHostEntryAsync(uri.Host);
            try
            {
                dnsTask.Wait();
            }
            catch (AggregateException)
            {
                return false;
            }

            if (!dnsTask.Result.AddressList.Any())
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
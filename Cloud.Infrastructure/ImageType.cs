﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageType.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    /// <summary>
    ///     The image type.
    /// </summary>
    public enum ImageType
    {
        /// <summary>
        ///     The png.
        /// </summary>
        Png, 

        /// <summary>
        ///     The jpeg.
        /// </summary>
        Jpeg, 

        /// <summary>
        ///     The tiff.
        /// </summary>
        Tiff, 

        /// <summary>
        ///     The bmp.
        /// </summary>
        Bmp, 

        /// <summary>
        ///     The svg.
        /// </summary>
        Svg
    }
}
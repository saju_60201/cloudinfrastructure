﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectionStringBuilder.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Data
{
    using System.Configuration;
    using System.Data.SqlClient;

    /// <summary>
    ///     Default ConnectionString builder.
    /// </summary>
    public class ConnectionStringBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="connectionStringNameOrPrefix">
        /// The connection string name or Initial Catalog prefix.
        /// </param>
        /// <returns>
        /// The connectionString
        /// </returns>
        public virtual string GetConnectionString(string connectionStringNameOrPrefix)
        {
            var baseConnectionString = string.Empty;
            var value = ConfigurationManager.ConnectionStrings[connectionStringNameOrPrefix];

            if (value == null)
            {
                // Default convention for ConnectionString
                baseConnectionString =
                    "Data Source=(local); Initial Catalog={0}; Persist Security Info=True;User ID=publiWebUser;Password=publiWebUser; MultipleActiveResultSets=True;";

                return this.FormatConnectionString(baseConnectionString.FormatWith(connectionStringNameOrPrefix));
            }

            return this.FormatConnectionString(value.ConnectionString);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The format connection string.
        /// </summary>
        /// <param name="baseConnectionString">
        /// The base connection string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string FormatConnectionString(string baseConnectionString)
        {
            var builder = new SqlConnectionStringBuilder(baseConnectionString);
            return builder.ToString();
        }

        #endregion
    }
}
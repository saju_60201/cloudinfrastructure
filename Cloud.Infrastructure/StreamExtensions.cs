﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StreamExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System.IO;
    using System.Security.Cryptography;

    /// <summary>
    ///     The stream extensions.
    /// </summary>
    public static class StreamExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The compute sha 256 hash.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ComputeSha256Hash(this Stream stream)
        {
            using (var sha = new SHA256CryptoServiceProvider())
            {
                stream.Seek(0, SeekOrigin.Begin);
                var hashValue = sha.ComputeHash(stream);
                return hashValue.ByteArrayToHexViaLookup32();
            }
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NamedWriterLockerTransactional.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Locking
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Transactions;

    /// <summary>
    ///     The named reader writer locker.
    /// </summary>
    public class NamedWriterLockerTransactional : INamedWriterLocker
    {
        #region Fields

        /// <summary>
        ///     The locker.
        /// </summary>
        private readonly NamedReaderWriterLocker locker = new NamedReaderWriterLocker();

        /// <summary>
        ///     The transaction locks.
        /// </summary>
        private readonly ConcurrentDictionary<string, List<string>> transactionLocks = new ConcurrentDictionary<string, List<string>>();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the current transation.
        /// </summary>
        /// <exception cref="Exception">
        /// </exception>
        private Transaction CurrentTransation
        {
            get
            {
                if (Transaction.Current == null)
                {
                    throw new Exception("There is no transaction running");
                }

                return Transaction.Current;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The exists lock name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ExistsLockName(string name)
        {
            return this.locker.ExistsLockName(name);
        }

        /// <summary>
        /// The exit writer lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ExitWriterLock(string name)
        {
            var rwLock = this.locker.GetLock(name);
            if (rwLock.IsWriteLockHeld)
            {
                rwLock.ExitWriteLock();
                return true;
            }

            return false;
        }

        /// <summary>
        /// The get lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="ReaderWriterLockSlim"/>.
        /// </returns>
        public ReaderWriterLockSlim GetLock(string name)
        {
            this.SubscribeTransactionEvents();
            this.AddTransactionLock(name);
            return this.locker.GetLock(name);
        }

        /// <summary>
        /// The remove lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public void RemoveLock(string name)
        {
            this.RemoveTransactionLockName(name);
            this.locker.RemoveLock(name);
        }

        /// <summary>
        /// The run with write lock. It releases the lock when the transaction is completed or aborted
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        /// <typeparam name="TResult">
        /// type of the result
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public TResult RunWithWriteLock<TResult>(string name, Func<TResult> body)
        {
            this.GetLock(name);
            return this.locker.RunWithWriteLockInternal(name, body, false);
        }

        /// <summary>
        /// The run with write lock. It releases the lock when the transaction is completed or aborted
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        public void RunWithWriteLock(string name, Action body)
        {
            this.GetLock(name);
            this.locker.RunWithWriteLockInternal(name, body, false);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add transaction lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        private void AddTransactionLock(string name)
        {
            this.ExecuteActionOnTransactionNames(
                transactionNames =>
                    {
                        if (!transactionNames.Contains(name))
                        {
                            transactionNames.Add(name);
                        }
                    });
        }

        /// <summary>
        /// The can be removed.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CanBeRemoved(string name)
        {
            if (!this.locker.ExistsLockName(name))
            {
                return true;
            }

            var rwLock = this.locker.GetLock(name);
            return rwLock.CurrentReadCount == 0 && rwLock.WaitingWriteCount == 0 && rwLock.WaitingReadCount == 0
                   && rwLock.WaitingUpgradeCount == 0 && rwLock.RecursiveWriteCount == 0;
        }

        /// <summary>
        /// The execute action on transaction names.
        /// </summary>
        /// <param name="actionToRun">
        /// The action to run.
        /// </param>
        private void ExecuteActionOnTransactionNames(Action<List<string>> actionToRun)
        {
            var transactionNames = this.transactionLocks.GetOrAdd(this.GetTransactionId(this.CurrentTransation), new List<string>());
            lock (transactionNames)
            {
                actionToRun(transactionNames);
            }
        }

        /// <summary>
        /// The get transaction id.
        /// </summary>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetTransactionId(Transaction transaction)
        {
            return transaction.TransactionInformation.LocalIdentifier;
        }

        /// <summary>
        /// The on transaction completed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void OnTransactionCompleted(object sender, TransactionEventArgs e)
        {
            this.RemoveTransactionLocks(this.GetTransactionId(e.Transaction));
            e.Transaction.TransactionCompleted -= this.OnTransactionCompleted;
        }

        /// <summary>
        /// The remove transaction lock name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        private void RemoveTransactionLockName(string name)
        {
            this.ExecuteActionOnTransactionNames(
                transactionNames =>
                    {
                        if (transactionNames.Contains(name))
                        {
                            transactionNames.Remove(name);
                        }
                    });
        }

        /// <summary>
        /// The remove transaction locks.
        /// </summary>
        /// <param name="trancationId">
        /// The trancation id.
        /// </param>
        private void RemoveTransactionLocks(string trancationId)
        {
            List<string> transactionLockNames;
            if (this.transactionLocks.TryGetValue(trancationId, out transactionLockNames))
            {
                lock (transactionLockNames)
                {
                    foreach (var lockName in transactionLockNames.ToList())
                    {
                        if (this.ExistsLockName(lockName) && this.ExitWriterLock(lockName))
                        {
                            this.locker.RemoveLock(lockName);
                        }
                        else
                        {
                            if (this.CanBeRemoved(lockName))
                            {
                                this.locker.RemoveLock(lockName);
                            }
                        }
                    }

                    List<string> temp;
                    this.transactionLocks.TryRemove(trancationId, out temp);
                }
            }
        }

        /// <summary>
        ///     The subscribe transaction events.
        /// </summary>
        private void SubscribeTransactionEvents()
        {
            this.CurrentTransation.TransactionCompleted -= this.OnTransactionCompleted;
            this.CurrentTransation.TransactionCompleted += this.OnTransactionCompleted;
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="INamedWriterLocker.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Locking
{
    using System;
    using System.Threading;

    /// <summary>
    ///     The NamedWriterLocker interface.
    /// </summary>
    public interface INamedWriterLocker
    {
        #region Public Methods and Operators

        /// <summary>
        /// The exists lock name. This does not mean that the resource is locked for write. Please use RunWithWriteLock
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ExistsLockName(string name);

        /// <summary>
        /// The get lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="ReaderWriterLockSlim"/>.
        /// </returns>
        ReaderWriterLockSlim GetLock(string name);

        /// <summary>
        /// The remove lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        void RemoveLock(string name);

        /// <summary>
        /// The run with write lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        /// <typeparam name="TResult">
        /// type of the result
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        TResult RunWithWriteLock<TResult>(string name, Func<TResult> body);

        /// <summary>
        /// The run with write lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        void RunWithWriteLock(string name, Action body);

        #endregion
    }
}
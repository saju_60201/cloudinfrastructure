﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NamedReaderWriterLocker.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Locking
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading;

    /// <summary>
    ///     The named reader writer locker.
    /// </summary>
    public class NamedReaderWriterLocker : INamedWriterLocker
    {
        #region Static Fields

        /// <summary>
        ///     The lock dict.
        /// </summary>
        private static readonly ConcurrentDictionary<string, ReaderWriterLockSlim> LockDict =
            new ConcurrentDictionary<string, ReaderWriterLockSlim>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The exists lock name. This does not mean that the resource is locked for write. Please use RunWithWriteLock
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ExistsLockName(string name)
        {
            ReaderWriterLockSlim foundLock;
            return LockDict.TryGetValue(name, out foundLock);
        }

        /// <summary>
        /// The get lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="ReaderWriterLockSlim"/>.
        /// </returns>
        public virtual ReaderWriterLockSlim GetLock(string name)
        {
            return LockDict.GetOrAdd(name, new ReaderWriterLockSlim());
        }

        /// <summary>
        /// The remove lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public void RemoveLock(string name)
        {
            ReaderWriterLockSlim o;
            LockDict.TryRemove(name, out o);
        }

        /// <summary>
        /// The run with read lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        /// <typeparam name="TResult">
        /// Type of the result
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public TResult RunWithReadLock<TResult>(string name, Func<TResult> body)
        {
            var rwLock = this.GetLock(name);
            try
            {
                rwLock.EnterReadLock();
                return body();
            }
            finally
            {
                rwLock.ExitReadLock();
            }
        }

        /// <summary>
        /// The run with read lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        public void RunWithReadLock(string name, Action body)
        {
            var rwLock = this.GetLock(name);
            try
            {
                rwLock.EnterReadLock();
                body();
            }
            finally
            {
                rwLock.ExitReadLock();
            }
        }

        /// <summary>
        /// The run with write lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        /// <typeparam name="TResult">
        /// type of the result
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public virtual TResult RunWithWriteLock<TResult>(string name, Func<TResult> body)
        {
            return this.RunWithWriteLockInternal(name, body, true);
        }

        /// <summary>
        /// The run with write lock.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        public virtual void RunWithWriteLock(string name, Action body)
        {
            this.RunWithWriteLockInternal(name, body, true);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The run with write lock internal.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        /// <param name="removeWriterLock">
        /// The remove writer lock.
        /// </param>
        internal void RunWithWriteLockInternal(string name, Action body, bool removeWriterLock)
        {
            var rwLock = this.GetLock(name);
            try
            {
                rwLock.EnterWriteLock();
                body();
            }
            finally
            {
                if (removeWriterLock)
                {
                    rwLock.ExitWriteLock();
                }
            }
        }

        /// <summary>
        /// The run with write lock internal.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="body">
        /// The body.
        /// </param>
        /// <param name="removeWriterLock">
        /// The remove writer lock.
        /// </param>
        /// <typeparam name="TResult">
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        internal virtual TResult RunWithWriteLockInternal<TResult>(string name, Func<TResult> body, bool removeWriterLock)
        {
            var rwLock = this.GetLock(name);
            try
            {
                rwLock.EnterWriteLock();
                return body();
            }
            finally
            {
                rwLock.ExitWriteLock();
            }
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SerializationExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure
{
    using System;
    using System.Collections;
    using System.Runtime.Serialization.Formatters;

    using Newtonsoft.Json;

    /// <summary>
    ///     The serialization extensions.
    /// </summary>
    public static class SerializationExtensions
    {
        #region Static Fields

        /// <summary>
        ///     The json serialization settings.
        /// </summary>
        private static readonly JsonSerializerSettings JsonSerializationSettings = CreateJsonSerializationSettings();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The deep clone.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        /// <typeparam name="T">
        /// The type to be cloned
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T DeepClone<T>(this T obj)
        {
            if (object.Equals(obj, null))
            {
                return default(T);
            }

            T deserialized;

            var json = JsonConvert.SerializeObject(obj, JsonSerializationSettings);

            if (typeof(T).IsAbstract && !(obj is IEnumerable))
            {
                deserialized = (T)JsonConvert.DeserializeObject(json, obj.GetType());
            }
            else
            {
                deserialized = JsonConvert.DeserializeObject<T>(json, JsonSerializationSettings);
            }

            if (object.Equals(deserialized, null))
            {
                throw new Exception(string.Format("DeepClone failed for the type {0}", obj.GetType()));
            }

            return deserialized;
        }

        /// <summary>
        /// The deserialize from json.
        /// </summary>
        /// <param name="jsonString">
        /// The json string.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object DeserializeFromJson(this string jsonString)
        {
            return JsonConvert.DeserializeObject(jsonString, JsonSerializationSettings);
        }

        /// <summary>
        /// The deserialize from json.
        /// </summary>
        /// <param name="jsonString">
        /// The json string.
        /// </param>
        /// <typeparam name="T">
        /// Type you want to deserialize to
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T DeserializeFromJson<T>(this string jsonString)
        {
            return string.IsNullOrEmpty(jsonString) ? default(T) : JsonConvert.DeserializeObject<T>(jsonString, JsonSerializationSettings);
        }

        /// <summary>
        /// The deserialize from json.
        /// </summary>
        /// <param name="jsonString">
        /// The json string.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object DeserializeFromJson(this string jsonString, Type targetType)
        {
            return string.IsNullOrEmpty(jsonString)
                       ? null
                       : JsonConvert.DeserializeObject(jsonString, targetType, JsonSerializationSettings);
        }

        /// <summary>
        /// The serialize to json.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string SerializeToJson(this object instance)
        {
            if (instance == null)
            {
                return string.Empty;
            }

            return JsonConvert.SerializeObject(instance, JsonSerializationSettings);
        }

        /// <summary>
        /// The serialize to json including object names.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string SerializeToJsonIncludingObjectNames(this object instance)
        {
            if (instance == null)
            {
                return string.Empty;
            }

            var settings = CreateJsonSerializationSettings();
            settings.TypeNameHandling = TypeNameHandling.Objects;

            return JsonConvert.SerializeObject(instance, settings);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The create json serialization settings.
        /// </summary>
        /// <returns>
        ///     The <see cref="JsonSerializerSettings" />.
        /// </returns>
        private static JsonSerializerSettings CreateJsonSerializationSettings()
        {
            var jsonSerializationSettings = new JsonSerializerSettings();
            jsonSerializationSettings.TypeNameHandling = TypeNameHandling.Auto;
            jsonSerializationSettings.TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple;
            var contractResolver = new PrivateSetterContractResolver();
            jsonSerializationSettings.ContractResolver = contractResolver;
            return jsonSerializationSettings;
        }

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.LinqExt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    ///     Refer to http://www.albahari.com/nutshell/linqkit.html and
    ///     http://tomasp.net/blog/linq-expand.aspx for more information.
    /// </summary>
    public static class Extensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The as expandable.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public static IQueryable<T> AsExpandable<T>(this IQueryable<T> query)
        {
            if (query is ExpandableQuery<T>)
            {
                return (ExpandableQuery<T>)query;
            }

            return new ExpandableQuery<T>(query);
        }

        /// <summary>
        /// The expand.
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <typeparam name="TDelegate">
        /// the type of the delegate returned as Expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public static Expression<TDelegate> Expand<TDelegate>(this Expression<TDelegate> expr)
        {
            return (Expression<TDelegate>)new ExpressionExpander().Visit(expr);
        }

        /// <summary>
        /// The expand.
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public static Expression Expand(this Expression expr)
        {
            return new ExpressionExpander().Visit(expr);
        }

        /// <summary>
        /// The for each.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
            {
                action(element);
            }
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <typeparam name="TResult">
        /// The result type of the expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public static TResult Invoke<TResult>(this Expression<Func<TResult>> expr)
        {
            return expr.Compile().Invoke();
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <param name="arg1">
        /// The arg 1.
        /// </param>
        /// <typeparam name="T1">
        /// the type of the argument passed into the method
        /// </typeparam>
        /// <typeparam name="TResult">
        /// The result type of the expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public static TResult Invoke<T1, TResult>(this Expression<Func<T1, TResult>> expr, T1 arg1)
        {
            return expr.Compile().Invoke(arg1);
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <param name="arg1">
        /// The arg 1.
        /// </param>
        /// <param name="arg2">
        /// The arg 2.
        /// </param>
        /// <typeparam name="T1">
        /// The type of the argument1 passed into the method
        /// </typeparam>
        /// <typeparam name="T2">
        /// The type of the argument2 passed into the method
        /// </typeparam>
        /// <typeparam name="TResult">
        /// The result type of the expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public static TResult Invoke<T1, T2, TResult>(this Expression<Func<T1, T2, TResult>> expr, T1 arg1, T2 arg2)
        {
            return expr.Compile().Invoke(arg1, arg2);
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <param name="arg1">
        /// The arg 1.
        /// </param>
        /// <param name="arg2">
        /// The arg 2.
        /// </param>
        /// <param name="arg3">
        /// The arg 3.
        /// </param>
        /// <typeparam name="T1">
        /// The type of the argument1 passed into the method
        /// </typeparam>
        /// <typeparam name="T2">
        /// The type of the argument2 passed into the method
        /// </typeparam>
        /// <typeparam name="T3">
        /// The type of the argument3 passed into the method
        /// </typeparam>
        /// <typeparam name="TResult">
        /// The result type of the expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public static TResult Invoke<T1, T2, T3, TResult>(this Expression<Func<T1, T2, T3, TResult>> expr, T1 arg1, T2 arg2, T3 arg3)
        {
            return expr.Compile().Invoke(arg1, arg2, arg3);
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <param name="arg1">
        /// The arg 1.
        /// </param>
        /// <param name="arg2">
        /// The arg 2.
        /// </param>
        /// <param name="arg3">
        /// The arg 3.
        /// </param>
        /// <param name="arg4">
        /// The arg 4.
        /// </param>
        /// <typeparam name="T1">
        /// The type of the argument1 passed into the method
        /// </typeparam>
        /// <typeparam name="T2">
        /// The type of the argument2 passed into the method
        /// </typeparam>
        /// <typeparam name="T3">
        /// The type of the argument3 passed into the method
        /// </typeparam>
        /// <typeparam name="T4">
        /// The type of the argument4 passed into the method
        /// </typeparam>
        /// <typeparam name="TResult">
        /// The result type of the expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        public static TResult Invoke<T1, T2, T3, T4, TResult>(
            this Expression<Func<T1, T2, T3, T4, TResult>> expr, 
            T1 arg1, 
            T2 arg2, 
            T3 arg3, 
            T4 arg4)
        {
            return expr.Compile().Invoke(arg1, arg2, arg3, arg4);
        }

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectContextExtention.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.LinqExt
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    ///     The object context extensions.
    /// </summary>
    public static class ObjectContextExtensions
    {
        /*
          /// <summary>
          /// Searches in all string properties for the specifed search key.
          /// It is also able to search for several words. If the searchKey is for example 'John Travolta' then
          /// all records which contain either 'John' or 'Travolta' in some string property
          /// are returned.
          /// </summary>
          /// <typeparam name="T"></typeparam>
          /// <param name="query"></param>
          /// <param name="searchKey"></param>
          /// <returns></returns>*/
        #region Public Methods and Operators

        /// <summary>
        /// The full text search.
        /// </summary>
        /// <param name="queryable">
        /// The queryable.
        /// </param>
        /// <param name="searchKey">
        /// The search key.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public static IQueryable<T> FullTextSearch<T>(this IQueryable<T> queryable, string searchKey)
        {
            return FullTextSearch(queryable, searchKey, false);
        }

        /*
          /// <summary>
          /// Searches in all string properties for the specifed search key.
          /// It is also able to search for several words. If the searchKey is for example 'John Travolta' then
          /// with exactMatch set to false all records which contain either 'John' or 'Travolta' in some string property
          /// are returned.
          /// </summary>
          /// <typeparam name="T"></typeparam>
          /// <param name="query"></param>
          /// <param name="searchKey"></param>
          /// <param name="exactMatch">Specifies if only the whole word or every single word should be searched.</param>
          /// <returns></returns>*/

        /// <summary>
        /// The full text search.
        /// </summary>
        /// <param name="queryable">
        /// The queryable.
        /// </param>
        /// <param name="searchKey">
        /// The search key.
        /// </param>
        /// <param name="exactMatch">
        /// The exact match.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public static IQueryable<T> FullTextSearch<T>(this IQueryable<T> queryable, string searchKey, bool exactMatch)
        {
            var parameter = Expression.Parameter(typeof(T), "c");

            var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            var toStringMethod = typeof(object).GetMethod("ToString", new Type[] { });

            var publicProperties =
                typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                    .Where(p => p.PropertyType == typeof(string));
            Expression orExpressions = null;

            var searchKeyParts = exactMatch ? new[] { searchKey } : searchKey.Split(' ');
            foreach (var property in publicProperties)
            {
                Expression nameProperty = Expression.Property(parameter, property);
                foreach (var searchKeyPart in searchKeyParts)
                {
                    Expression searchKeyExpression = Expression.Constant(searchKeyPart);
                    Expression callContainsMethod = Expression.Call(nameProperty, containsMethod, searchKeyExpression);
                    orExpressions = orExpressions == null ? callContainsMethod : Expression.Or(orExpressions, callContainsMethod);
                }
            }

            MethodCallExpression whereCallExpression = Expression.Call(
                typeof(Queryable), 
                "Where", 
                new[] { queryable.ElementType }, 
                queryable.Expression, 
                Expression.Lambda<Func<T, bool>>(orExpressions, new[] { parameter }));

            return queryable.Provider.CreateQuery<T>(whereCallExpression);
        }

        #endregion
    }
}
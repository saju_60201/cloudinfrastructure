// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpandableQuery.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.LinqExt
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// An IQueryable wrapper that allows us to visit the query's expression tree just before LINQ to SQL gets to it.
    ///     This is based on the excellent work of Tomas Petricek: http://tomasp.net/blog/linq-expand.aspx
    /// </summary>
    /// <typeparam name="T">
    /// The object used as expandable
    /// </typeparam>
    public class ExpandableQuery<T> : IQueryable<T>, IOrderedQueryable<T>, IOrderedQueryable
    {
        #region Fields

        /// <summary>
        ///     The _inner.
        /// </summary>
        private readonly IQueryable<T> inner;

        /// <summary>
        ///     The _provider.
        /// </summary>
        private readonly ExpandableQueryProvider<T> provider;

        #endregion

        // Original query, that we're wrapping
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpandableQuery{T}"/> class.
        /// </summary>
        /// <param name="inner">
        /// The inner.
        /// </param>
        internal ExpandableQuery(IQueryable<T> inner)
        {
            this.inner = inner;
            this.provider = new ExpandableQueryProvider<T>(this);
        }

        #endregion

        #region Explicit Interface Properties

        /// <summary>
        ///     Gets the element type.
        /// </summary>
        Type IQueryable.ElementType
        {
            get
            {
                return typeof(T);
            }
        }

        /// <summary>
        ///     Gets the expression.
        /// </summary>
        Expression IQueryable.Expression
        {
            get
            {
                return this.inner.Expression;
            }
        }

        /// <summary>
        ///     Gets the provider.
        /// </summary>
        IQueryProvider IQueryable.Provider
        {
            get
            {
                return this.provider;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the inner query.
        /// </summary>
        internal IQueryable<T> InnerQuery
        {
            get
            {
                return this.inner;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {
            return this.inner.GetEnumerator();
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public override string ToString()
        {
            return this.inner.ToString();
        }

        #endregion

        #region Explicit Interface Methods

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.inner.GetEnumerator();
        }

        #endregion
    }

    /// <summary>
    /// The expandable query provider.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the provider
    /// </typeparam>
    internal class ExpandableQueryProvider<T> : IQueryProvider
    {
        #region Fields

        /// <summary>
        ///     The _query.
        /// </summary>
        private readonly ExpandableQuery<T> query;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpandableQueryProvider{T}"/> class.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        internal ExpandableQueryProvider(ExpandableQuery<T> query)
        {
            this.query = query;
        }

        #endregion

        // The following four methods first call ExpressionExpander to visit the expression tree, then call
        // upon the inner query to do the remaining work.
        #region Explicit Interface Methods

        /// <summary>
        /// The create query.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="TElement">
        /// The query will be created for this element
        /// </typeparam>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<TElement> IQueryProvider.CreateQuery<TElement>(Expression expression)
        {
            return new ExpandableQuery<TElement>(this.query.InnerQuery.Provider.CreateQuery<TElement>(expression.Expand()));
        }

        /// <summary>
        /// The create query.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            return this.query.InnerQuery.Provider.CreateQuery(expression.Expand());
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="TResult">
        /// The result type of the expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="TResult"/>.
        /// </returns>
        TResult IQueryProvider.Execute<TResult>(Expression expression)
        {
            return this.query.InnerQuery.Provider.Execute<TResult>(expression.Expand());
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object IQueryProvider.Execute(Expression expression)
        {
            return this.query.InnerQuery.Provider.Execute(expression.Expand());
        }

        #endregion
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Linq.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.LinqExt
{
    using System;
    using System.Linq.Expressions;

    /// <summary>
    ///     The linq.
    /// </summary>
    public static class Linq
    {
        // Returns the given anonymous method as a lambda expression
        #region Public Methods and Operators

        /// <summary>
        /// The expression returns the property or method as string
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <typeparam name="T">
        /// The object which property should be returned
        /// </typeparam>
        /// <typeparam name="TResult">
        /// the lambda
        /// </typeparam>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public static Expression<Func<T, TResult>> Expr<T, TResult>(Expression<Func<T, TResult>> expr)
        {
            return expr;
        }

        /// <summary>
        /// Returns the given anonymous function as a Func delegate
        /// </summary>
        /// <param name="expr">
        /// The expr.
        /// </param>
        /// <typeparam name="T">
        /// object which is used as part of the expression
        /// </typeparam>
        /// <typeparam name="TResult">
        /// result of the used method inside of the expression
        /// </typeparam>
        /// <returns>
        /// The <see cref="Func{T,TResult}"/>.
        /// </returns>
        public static Func<T, TResult> Func<T, TResult>(Func<T, TResult> expr)
        {
            return expr;
        }

        #endregion
    }
}
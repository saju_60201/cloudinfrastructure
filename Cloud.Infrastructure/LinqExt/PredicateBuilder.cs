// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PredicateBuilder.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.LinqExt
{
    using System;
    using System.Linq.Expressions;

    /// <summary>
    ///     The predicate builder.
    /// </summary>
    public static class PredicateBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        /// The and.
        /// </summary>
        /// <param name="expr1">
        /// The expr 1.
        /// </param>
        /// <param name="expr2">
        /// The expr 2.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters);
            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
        }

        /// <summary>
        ///     The false.
        /// </summary>
        /// <typeparam name="T">
        ///     The object this method is applied to
        /// </typeparam>
        /// <returns>
        ///     The <see cref="Expression" />.
        /// </returns>
        public static Expression<Func<T, bool>> False<T>()
        {
            return f => false;
        }

        /// <summary>
        /// The or.
        /// </summary>
        /// <param name="expr1">
        /// The expr 1.
        /// </param>
        /// <param name="expr2">
        /// The expr 2.
        /// </param>
        /// <typeparam name="T">
        /// The object this method is applied to
        /// </typeparam>
        /// <returns>
        /// The <see cref="Expression"/>.
        /// </returns>
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters);
            return Expression.Lambda<Func<T, bool>>(Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
        }

        /// <summary>
        ///     The true.
        /// </summary>
        /// <typeparam name="T">
        ///     The object this method is applied to
        /// </typeparam>
        /// <returns>
        ///     The <see cref="Expression" />.
        /// </returns>
        public static Expression<Func<T, bool>> True<T>()
        {
            return f => true;
        }

        #endregion
    }
}
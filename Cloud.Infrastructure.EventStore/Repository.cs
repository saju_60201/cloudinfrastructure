﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Repository.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using NES.Contracts;

namespace Cloud.Infrastructure.EventStore
{
    using ILogger = Cloud.Infrastructure.Logging.Contracts.ILogger;

    /// <summary>
    ///     The repository.
    /// </summary>
    public class Repository : IRepository
    {
        private readonly IRepository internalRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="pipesFactory">
        /// The pipes factory.
        /// </param>
        public Repository(ILogger logger, IRepositoryPipesFactory pipesFactory)
            : this(logger, pipesFactory, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="pipesFactory">
        /// The pipes factory.
        /// </param>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Logger and Factory must not be null
        /// </exception>
        protected Repository(ILogger logger, IRepositoryPipesFactory pipesFactory, IRepository repository)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (pipesFactory == null)
            {
                throw new ArgumentNullException("pipesFactory");
            }

            this.Logger = logger;
            this.PipesFactory = pipesFactory;
            this.internalRepository = repository ?? new NES.Repository();
        }

        private ILogger Logger { get; set; }

        private IRepositoryPipesFactory PipesFactory { get; set; }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="aggregate">
        /// The aggregate.
        /// </param>
        /// <typeparam name="T">
        /// The aggregate which should be added
        /// </typeparam>
        public void Add<T>(T aggregate) where T : class, IEventSourceBase
        {
            this.internalRepository.Add(aggregate);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="bucketId">
        /// The bucket id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <typeparam name="T"> a type
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T Get<T>(string bucketId, Guid id) where T : class, IEventSource<Guid>
        {
            var source = this.internalRepository.Get<T>(bucketId, id);
            foreach (var repPipe in this.PipesFactory.GetPipes())
            {
                IRepositoryPipe pipe = repPipe;
                this.Logger.Debug(() => string.Format("Calling After Read pipe {0}", pipe.GetType().FullName));
                repPipe.AfterRead(id, source);
            }

            return source;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="bucketId">
        /// The bucket id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <typeparam name="T"> a type
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T Get<T>(string bucketId, Guid id, int version) where T : class, IEventSource<Guid>
        {
            var source = this.internalRepository.Get<T>(bucketId, id, version);
            foreach (var repPipe in this.PipesFactory.GetPipes())
            {
                IRepositoryPipe pipe = repPipe;
                this.Logger.Debug(() => string.Format("Calling After Read pipe {0}", pipe.GetType().FullName));
                repPipe.AfterRead(id, source);
            }

            return source;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <typeparam name="T">
        /// The Type which should be returned from repository
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        ///     which should be returned from repository
        /// </returns>
        public T Get<T>(Guid id) where T : class, IEventSource<Guid>
        {
            var source = this.internalRepository.Get<T>(id);
            foreach (var repPipe in this.PipesFactory.GetPipes())
            {
                IRepositoryPipe pipe = repPipe;
                this.Logger.Debug(() => string.Format("Calling After Read pipe {0}", pipe.GetType().FullName));
                repPipe.AfterRead(id, source);
            }

            return source;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <typeparam name="T"> some type </typeparam>
        /// <returns> The <see cref="T"/>an instance of type </returns>
        public T Get<T>(Guid id, int version) where T : class, IEventSource<Guid>
        {
            var source = this.internalRepository.Get<T>(id, version);
            foreach (var repPipe in this.PipesFactory.GetPipes())
            {
                IRepositoryPipe pipe = repPipe;
                this.Logger.Debug(() => string.Format("Calling After Read pipe {0}", pipe.GetType().FullName));
                repPipe.AfterRead(id, source);
            }

            return source;
        }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepositoryPipe.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The RepositoryPipe interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using NES.Contracts;

namespace Cloud.Infrastructure.EventStore
{
    /// <summary>
    ///     The RepositoryPipe interface.
    /// </summary>
    public interface IRepositoryPipe
    {
        /// <summary>
        /// The after read.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <typeparam name="T">
        /// Type of the source
        /// </typeparam>
        void AfterRead<T>(Guid id, T source) where T : class, IEventSource<Guid>;
    }
}
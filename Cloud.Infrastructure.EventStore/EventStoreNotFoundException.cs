﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventStoreNotFoundException.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The event store not found exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Cloud.Infrastructure.EventStore
{
    /// <summary>
    ///     The event store not found exception.
    /// </summary>
    public class EventStoreNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventStoreNotFoundException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public EventStoreNotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventStoreNotFoundException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="innerException">
        /// The inner exception.
        /// </param>
        public EventStoreNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
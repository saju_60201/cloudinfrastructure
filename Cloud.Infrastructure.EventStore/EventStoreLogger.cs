﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventStoreLogger.cs" company="">
//   
// </copyright>
// <summary>
//   The event store logger.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.EventStore
{
    using System;
    using System.Diagnostics;

    using Cloud.Infrastructure.Logging.Contracts;

    using NEventStore.Logging;

    /// <summary>
    ///     The event store logger.
    /// </summary>
    public class EventStoreLogger : ILog
    {
        #region Fields

        /// <summary>
        ///     The logger name.
        /// </summary>
        private readonly string loggerName;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EventStoreLogger"/> class.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Logger must be not empty
        /// </exception>
        public EventStoreLogger(Type type, ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            this.Logger = logger;
            this.loggerName = type != null ? type.FullName : null;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the root logger name.
        /// </summary>
        public string RootLoggerName
        {
            get
            {
                return this.Logger.RootLoggerName;
            }

            set
            {
                this.Logger.RootLoggerName = value;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the logger.
        /// </summary>
        private ILogger Logger { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        public void Debug(string message, params object[] values)
        {
            this.Logger.Debug(() => string.Format(message, values), this.GetLoggerName());
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        public void Error(string message, params object[] values)
        {
            this.Logger.Error(() => string.Format(message, values), this.GetLoggerName());
        }

        /// <summary>
        /// The fatal.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        public void Fatal(string message, params object[] values)
        {
            this.Logger.Fatal(new Exception(string.Format(message, values)), this.GetLoggerName());
        }

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        public void Info(string message, params object[] values)
        {
            this.Logger.Info(() => string.Format(message, values), this.GetLoggerName());
        }

        /// <summary>
        /// The verbose.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        public void Verbose(string message, params object[] values)
        {
            this.Logger.Debug(() => string.Format(message, values), this.GetLoggerName());
        }

        /// <summary>
        /// The warn.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="values">
        /// The values.
        /// </param>
        public void Warn(string message, params object[] values)
        {
            this.Logger.Warning(() => string.Format(message, values), this.GetLoggerName());
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get logger name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetLoggerName()
        {
            if (!string.IsNullOrEmpty(this.loggerName))
            {
                return this.loggerName;
            }

            var stackFrame = new StackFrame(4, false);
            Type declaringType = stackFrame.GetMethod().DeclaringType;
            if (declaringType != null)
            {
                return declaringType.FullName;
            }

            return this.GetType().Name;
        }

        #endregion
    }
}
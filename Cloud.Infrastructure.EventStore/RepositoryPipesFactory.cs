﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryPipesFactory.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.EventStore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Cloud.Infrastructure.Interception;
    using Cloud.Infrastructure.Logging.Contracts;
    using System.Runtime;

    /// <summary>
    ///     The repository pipes factory.
    /// </summary>
    public class RepositoryPipesFactory : IRepositoryPipesFactory
    {
        #region Static Fields

        /// <summary>
        /// The sync root.
        /// </summary>
        private static readonly object SyncRoot = new object();

        /// <summary>
        /// The pipes cache.
        /// </summary>
        private static List<IRepositoryPipe> pipesCache;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryPipesFactory"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="serviceLocator">
        /// The service Locator.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Logger must not be null
        /// </exception>
        public RepositoryPipesFactory(ILogger logger, IServiceLocator serviceLocator)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            if (serviceLocator == null)
            {
                throw new ArgumentNullException("serviceLocator");
            }

            this.Logger = logger;
            this.ServiceLocator = serviceLocator;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        private ILogger Logger { get; set; }

        /// <summary>
        /// Gets or sets the service locator.
        /// </summary>
        private IServiceLocator ServiceLocator { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get pipes.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public IEnumerable<IRepositoryPipe> GetPipes()
        {
            if (pipesCache == null)
            {
                lock (SyncRoot)
                {
                    if (pipesCache != null)
                    {
                        return pipesCache;
                    }

                    this.Logger.Debug(() => "Pipes is null so perform a scan");

                    var pipeTypes = CurrentDomainTypes.GetTypesDerivingFrom<IRepositoryPipe>(false).ToList();

                    this.Logger.Debug(() => string.Format("Found pipes count is {0}", pipeTypes.Count));

                    pipesCache = new List<IRepositoryPipe>(pipeTypes.Count);

                    foreach (var pipeType in pipeTypes)
                    {
                        var type = pipeType;
                        this.Logger.Debug(() => string.Format("Pipe type is {0}", type.FullName));
                        var pipeItem = (IRepositoryPipe)this.ServiceLocator.Current.ResolveOrThrow(pipeType);
                        pipesCache.Add(pipeItem);
                    }
                }
            }

            return pipesCache;
        }

        #endregion
    }
}
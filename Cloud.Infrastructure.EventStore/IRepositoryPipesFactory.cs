﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepositoryPipesFactory.cs" company="Ruf Informatik AG">
//   Copyright © Ruf Informatik AG. All rights reserved.
// </copyright>
// <summary>
//   The RepositoryPipesFactory interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace Cloud.Infrastructure.EventStore
{
    /// <summary>
    ///     The RepositoryPipesFactory interface.
    /// </summary>
    public interface IRepositoryPipesFactory
    {
        /// <summary>
        ///     The get pipes.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        IEnumerable<IRepositoryPipe> GetPipes();
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// <summary>
//   The extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.EventStore
{
    using System;

    using NES;
    using NES.Contracts;

    using NEventStore;

    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The exists.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Exists(this IRepository repository, Guid id)
        {
            var storeEvents = DI.Current.Resolve<IStoreEvents>();
            IEventStream eventSteam = storeEvents.OpenStream(id, int.MinValue, int.MaxValue);
            return eventSteam != null && eventSteam.CommittedEvents.Count > 0;
        }

        /// <summary>
        /// The get from event store or throw exception.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <typeparam name="T">
        /// Type of the EventSource resp. Aggregate
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// record not found on database
        /// </exception>
        public static T GetOrThrowException<T>(this IRepository repository, Guid id) where T : AggregateBase<Guid>
        {
            var instance = repository.Get<T>(id);
            if (instance != null)
            {
                return instance;
            }

            throw new Exception(string.Format("Object not found on database {0}", typeof(T).FullName));
        }

        /// <summary>
        /// The get without pipe.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <typeparam name="T">
        /// Aggregate type
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T GetWithoutPipe<T>(this IRepository repository, Guid id) where T : class, IEventSource<Guid>
        {
            bool isDisabled = EventStorePipeSettings.Disable();
            try
            {
                return repository.Get<T>(id);
            }
            finally
            {
                if (isDisabled)
                {
                    EventStorePipeSettings.Enable();
                }
            }
        }

        /// <summary>
        /// The get without pipe.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="versionId">
        /// The version id.
        /// </param>
        /// <typeparam name="T">
        /// Aggregate type
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T GetWithoutPipe<T>(this IRepository repository, Guid id, int versionId) where T : class, IEventSource<Guid>
        {
            bool isDisabled = EventStorePipeSettings.Disable();
            try
            {
                return repository.Get<T>(id, versionId);
            }
            finally
            {
                if (isDisabled)
                {
                    EventStorePipeSettings.Enable();
                }
            }
        }

        #endregion
    }
}
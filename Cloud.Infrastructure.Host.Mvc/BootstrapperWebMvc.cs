﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BootstrapperWebMvc.cs" company="">
//   
// </copyright>
// <summary>
//   The bootstrapper web mvc.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Host.Mvc
{
    using System.Web.Mvc;

    using Microsoft.Practices.Unity;

    /// <summary>
    /// The bootstrapper web mvc.
    /// </summary>
    public class BootstrapperWebMvc : BootstrapperWebApi
    {
        #region Methods

        /// <summary>
        /// The register controllers.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        protected override void RegisterControllers(IUnityContainer container)
        {
            container.RegisterInstance<IControllerActivator>(new MvcControllerActivator(), new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// The setup container.
        /// </summary>
        protected override void SetupContainer()
        {
            this.BuildUnityContainer();
            DependencyResolver.SetResolver(new MvcLocatorUnityDependencyResolver(this.Config.Container));
        }

        #endregion
    }
}
﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Cloud.Infrastructure.Host.Mvc
{
    public class MvcControllerActivator : IControllerActivator
    {
        public IController Create(RequestContext requestContext, Type controllerType)
        {
            var controller = DependencyResolver.Current.GetService(controllerType) as IController;
            return controller;
        }
    }
}
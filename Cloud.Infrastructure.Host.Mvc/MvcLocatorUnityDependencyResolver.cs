﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MvcLocatorUnityDependencyResolver.cs" company="">
//   
// </copyright>
// <summary>
//   The mvc locator unity dependency resolver.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Cloud.Infrastructure.Host.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Microsoft.Practices.Unity;

    /// <summary>
    /// The mvc locator unity dependency resolver.
    /// </summary>
    public class MvcLocatorUnityDependencyResolver : IDependencyResolver
    {
        #region Fields

        /// <summary>
        /// The container.
        /// </summary>
        private readonly IUnityContainer container;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MvcLocatorUnityDependencyResolver"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MvcLocatorUnityDependencyResolver(IUnityContainer container)
        {
            this.container = container;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get service.
        /// </summary>
        /// <param name="serviceType">
        /// The service type.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetService(Type serviceType)
        {
            try
            {
                return this.container.Resolve(serviceType);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The get services.
        /// </summary>
        /// <param name="serviceType">
        /// The service type.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return this.container.ResolveAll(serviceType);
            }
            catch
            {
                return new List<object>();
            }
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceCode.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System;

    /// <summary>
    ///     The resource code.
    /// </summary>
    public class ResourceCode : ILocalizationResource
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceCode"/> class.
        /// </summary>
        /// <param name="resourceKey">
        /// The resource key.
        /// </param>
        /// <param name="resourceType">
        /// The resource type.
        /// </param>
        public ResourceCode(string resourceKey, Type resourceType)
        {
            this.Key = resourceKey;
            this.ResourceTypeFullName = resourceType.FullName;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the key.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        ///     Gets the resource type full name.
        /// </summary>
        public string ResourceTypeFullName { get; private set; }

        #endregion
    }
}
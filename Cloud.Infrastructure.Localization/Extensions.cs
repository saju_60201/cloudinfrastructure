﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Reflection;
    using System.Resources;
    using System.Threading;

    /// <summary>
    ///     The extensions.
    /// </summary>
    public static class Extensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get current culture info.
        /// </summary>
        /// <param name="languageSupport">
        /// The language Support.
        /// </param>
        /// <returns>
        /// The <see cref="CultureInfo"/>.
        /// </returns>
        public static CultureInfo GetCurrentCultureInfo(this ILanguageSupport languageSupport)
        {
            return languageSupport.GetCurrentLanguage().CultureInfo;
        }

        /// <summary>
        /// The get resource type.
        /// </summary>
        /// <param name="resource">
        /// The resource.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/>.
        /// </returns>
        public static Type GetResourceType(this ILocalizationResource resource)
        {
            return Type.GetType(resource.ResourceTypeFullName);
        }

        /// <summary>
        /// The gets resx value for given enum that is decorated with defined DisplayAttribute .
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture info.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetResxValue(this Enum value, CultureInfo cultureInfo)
        {
            cultureInfo = cultureInfo ?? Thread.CurrentThread.CurrentUICulture;

            FieldInfo field = value.GetType().GetField(value.ToString());
            var attribute = Attribute.GetCustomAttribute(field, typeof(DisplayAttribute)) as DisplayAttribute;

            if (attribute != null)
            {
                var resourceManager = new ResourceManager(attribute.ResourceType);

                return resourceManager.GetString(attribute.Name, cultureInfo);
            }

            return null;
        }

        /// <summary>
        /// Gets resx value for a given static enumeration value.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <param name="staticListType">
        /// The static list type.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture info.
        /// </param>
        /// <typeparam name="T">
        /// Enumeration value
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// if instance not set
        /// </exception>
        /// <exception cref="Exception">
        /// target property not found
        /// </exception>
        public static string GetResxValue<T>(this Enumeration<T> instance, Type staticListType, CultureInfo cultureInfo)
            where T : IComparable<T>, new()
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance");
            }

            cultureInfo = cultureInfo ?? Thread.CurrentThread.CurrentUICulture;

            var staticProps = staticListType.GetProperties(BindingFlags.GetProperty | BindingFlags.Static | BindingFlags.Public);

            PropertyInfo targetProperty = null;
            foreach (var propertyInfo in staticProps)
            {
                var propValue = propertyInfo.GetValue(null);
                if (propValue != null && (Enumeration<T>)propValue == instance)
                {
                    targetProperty = propertyInfo;
                    break;
                }
            }

            if (targetProperty == null)
            {
                throw new Exception("target property not found");
            }

            var attribute = Attribute.GetCustomAttribute(targetProperty, typeof(DisplayAttribute)) as DisplayAttribute;

            if (attribute != null)
            {
                var resourceManager = new ResourceManager(attribute.ResourceType);

                return resourceManager.GetString(attribute.Name, cultureInfo);
            }

            return null;
        }

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="resource">
        /// The resource.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetString(this ILocalizationResource resource, params object[] arguments)
        {
            if (resource == null)
            {
                return string.Empty;
            }

            return new ResourceTranslator().GetString(resource, arguments);
        }

        #endregion
    }
}
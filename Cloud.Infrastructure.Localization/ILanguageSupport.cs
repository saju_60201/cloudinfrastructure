﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILanguageSupport.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System.Collections.Generic;

    /// <summary>
    ///     The LanguageSupport interface.
    /// </summary>
    public interface ILanguageSupport
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The get all languages.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        IEnumerable<Language> GetAllLanguages();

        /// <summary>
        ///     The get current language.
        /// </summary>
        /// <returns>
        ///     The <see cref="Language" />.
        /// </returns>
        Language GetCurrentLanguage();

        /// <summary>
        ///     The get tenant languages.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        IEnumerable<Language> GetTenantLanguages();

        /// <summary>
        ///     The get user languages.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        IEnumerable<Language> GetUserLanguages();

        #endregion
    }
}
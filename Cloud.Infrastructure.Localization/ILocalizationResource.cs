﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILocalizationResource.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    /// <summary>
    ///     The LocalizationResource interface.
    /// </summary>
    public interface ILocalizationResource
    {
        #region Public Properties

        /// <summary>
        ///     Gets the key.
        /// </summary>
        string Key { get; }

        /// <summary>
        ///     Gets the resource type full name.
        /// </summary>
        string ResourceTypeFullName { get; }

        #endregion
    }
}
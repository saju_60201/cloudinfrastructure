﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguageValue.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    /// <summary>
    ///     The language value.
    /// </summary>
    public class LanguageValue
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the language.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value { get; set; }

        #endregion
    }
}
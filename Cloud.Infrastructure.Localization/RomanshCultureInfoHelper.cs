﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RomanshCultureInfoHelper.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System;
    using System.Globalization;

    /// <summary>
    ///     The romansh culture info helper.
    /// </summary>
    public static class RomanshCultureInfoHelper
    {
        #region Public Methods and Operators

        /// <summary>
        /// Registers custom romanch culture info.
        /// </summary>
        /// <param name="customCultureAndRegionInfoName">
        /// The custom culture and region info name: from "rm0-CH" to "rm4-CH"
        /// </param>
        public static void RegisterRomanchCultureInfo(string customCultureAndRegionInfoName)
        {
            CultureAndRegionInfoBuilder cib = null;
            try
            {
                // Create a CultureAndRegionInfoBuilder object named "customCultureAndRegionInfoName".
                cib = new CultureAndRegionInfoBuilder(customCultureAndRegionInfoName, CultureAndRegionModifiers.None);

                // Populate the new CultureAndRegionInfoBuilder object with culture information.
                var ci = new CultureInfo("rm-CH");
                cib.LoadDataFromCultureInfo(ci);

                // Populate the new CultureAndRegionInfoBuilder object with region information.
                var ri = new RegionInfo("CH");
                cib.LoadDataFromRegionInfo(ri);

                // Register the custom culture.
                cib.Register();
            }
            catch (Exception)
            {
            }
        }

        #endregion
    }
}
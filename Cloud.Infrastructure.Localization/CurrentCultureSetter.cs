﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CurrentCultureSetter.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System.Globalization;

    /// <summary>
    ///     The current culture setter.
    /// </summary>
    public class CurrentCultureSetter : ICurrentCultureSetter
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the culture.
        /// </summary>
        public CultureInfo Culture { get; set; }

        #endregion
    }
}
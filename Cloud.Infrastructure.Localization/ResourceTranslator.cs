﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceTranslator.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Resources;
    using System.Threading;

    /// <summary>
    ///     The resource translator.
    /// </summary>
    public class ResourceTranslator : IResourceTranslator
    {
        #region Fields

        /// <summary>
        /// The resource managers.
        /// </summary>
        private readonly ConcurrentDictionary<string, ResourceManager> resourceManagers =
            new ConcurrentDictionary<string, ResourceManager>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetString(ILocalizationResource key)
        {
            return this.GetString(key, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetString(ILocalizationResource key, object[] arguments)
        {
            return this.GetString(key, Thread.CurrentThread.CurrentCulture, arguments);
        }

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="targetCulture">
        /// The target culture.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetString(ILocalizationResource key, CultureInfo targetCulture, object[] arguments)
        {
            ResourceManager resourceManager;
            if (!this.resourceManagers.TryGetValue(key.ResourceTypeFullName, out resourceManager))
            {
                resourceManager = new ResourceManager(key.GetResourceType());
                this.resourceManagers.TryAdd(key.ResourceTypeFullName, resourceManager);
            }

            var stringToReturn = resourceManager.GetString(key.Key, targetCulture);
            if (arguments == null || string.IsNullOrEmpty(stringToReturn))
            {
                return stringToReturn;
            }

            return string.Format(stringToReturn, arguments);
        }

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="targetCulture">
        /// The target culture.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetString(ILocalizationResource key, CultureInfo targetCulture)
        {
            return this.GetString(key, targetCulture, null);
        }

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="targetCulture">
        /// The target culture.
        /// </param>
        /// <typeparam name="T">
        /// type of localizationresource
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetString<T>(CultureInfo targetCulture) where T : ILocalizationResource, new()
        {
            var res = new T();
            return this.GetString(res, targetCulture);
        }

        /// <summary>
        /// The get supported languages.
        /// </summary>
        /// <param name="languageSupport">
        /// The current culture provider.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="resourceType">
        /// The resource type.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<LanguageValue> GetSupportedLanguages(
            ILanguageSupport languageSupport, 
            string key, 
            Type resourceType, 
            object[] arguments)
        {
            var translations = new List<LanguageValue>();
            var resKey = new ResourceCode(key, resourceType);
            foreach (var supportedLanguage in languageSupport.GetAllLanguages())
            {
                var translatedString = this.GetString(resKey, supportedLanguage.CultureInfo, arguments);
                var languageValue = new LanguageValue() { Language = supportedLanguage, Value = translatedString };
                translations.Add(languageValue);
            }

            return translations;
        }

        /// <summary>
        /// Gets the supported languages.
        /// </summary>
        /// <param name="languageSupport">
        /// The current culture provider.
        /// </param>
        /// <param name="multilanguageParamValues">
        /// The multilanguage parameter values.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="resourceType">
        /// Type of the resource.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// List of LanguageValue
        /// </returns>
        public List<LanguageValue> GetSupportedLanguages(
            ILanguageSupport languageSupport, 
            List<LanguageValue> multilanguageParamValues, 
            string key, 
            Type resourceType, 
            object[] arguments)
        {
            var translations = new List<LanguageValue>();
            var resKey = new ResourceCode(key, resourceType);
            foreach (var supportedLanguage in languageSupport.GetAllLanguages())
            {
                var multiLanguageValue =
                    multilanguageParamValues.FirstOrDefault(
                        x => x.Language.CultureInfo.TwoLetterISOLanguageName == supportedLanguage.CultureInfo.TwoLetterISOLanguageName);
                object[] placeholderValues = arguments;
                if (multiLanguageValue != null)
                {
                    placeholderValues = arguments != null
                                            ? new object[] { multiLanguageValue.Value }.Concat(arguments).ToArray()
                                            : new object[] { multiLanguageValue.Value };
                }

                var translatedString = this.GetString(resKey, supportedLanguage.CultureInfo, placeholderValues);
                var languageValue = new LanguageValue() { Language = supportedLanguage, Value = translatedString };
                translations.Add(languageValue);
            }

            return translations;
        }

        #endregion
    }
}
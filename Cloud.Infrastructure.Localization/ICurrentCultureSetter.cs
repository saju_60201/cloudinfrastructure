﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICurrentCultureSetter.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System.Globalization;

    /// <summary>
    ///     The CurrentCultureSetter interface.
    /// </summary>
    public interface ICurrentCultureSetter
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the culture.
        /// </summary>
        CultureInfo Culture { get; set; }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguageSupport.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;

    /// <summary>
    ///     The current culture.
    /// </summary>
    public class LanguageSupport : ILanguageSupport
    {
        #region Static Fields

        /// <summary>
        /// The all support languages.
        /// </summary>
        private static readonly List<Language> AllSupportLanguages =
            Language.GetAll<Language, LanguageCodes>().Where(l => l.CultureInfo != null).ToList();

        #endregion

        #region Fields

        /// <summary>
        /// The current culture setter.
        /// </summary>
        private readonly ICurrentCultureSetter currentCultureSetter;

        /// <summary>
        /// The last found language.
        /// </summary>
        private Language lastFoundLanguage;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageSupport"/> class.
        /// </summary>
        /// <param name="iCurrentCultureSetter">
        /// The i current culture setter.
        /// </param>
        public LanguageSupport(ICurrentCultureSetter iCurrentCultureSetter)
        {
            this.currentCultureSetter = iCurrentCultureSetter;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Returns the supported languages
        /// </summary>
        /// <returns>all supported languages</returns>
        public IEnumerable<Language> GetAllLanguages()
        {
            return AllSupportLanguages;
        }

        /// <summary>Gets current culture info. </summary>
        /// <returns>
        ///     The <see cref="CultureInfo" />.
        /// </returns>
        public Language GetCurrentLanguage()
        {
            var currentCulture = this.currentCultureSetter.Culture ?? Thread.CurrentThread.CurrentCulture;

            if (this.lastFoundLanguage != null && object.Equals(this.lastFoundLanguage.CultureInfo, currentCulture))
            {
                return this.lastFoundLanguage;
            }

            this.lastFoundLanguage = AllSupportLanguages.FirstOrDefault(l => object.Equals(l.CultureInfo, currentCulture));
            return this.lastFoundLanguage ?? LanguageCodes.German;
        }

        /// <summary>
        ///     The get tenant languages.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public IEnumerable<Language> GetTenantLanguages()
        {
            //todo: check which languages are by tenant supported and return the list
            return this.GetAllLanguages();
        }

        /// <summary>
        ///     The get user languages.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public IEnumerable<Language> GetUserLanguages()
        {
            //todo: check which languages are by user supported and return the list
            return this.GetAllLanguages();
        }

        #endregion
    }
}
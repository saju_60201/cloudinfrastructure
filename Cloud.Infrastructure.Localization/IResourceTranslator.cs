// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IResourceTranslator.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.Localization.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    ///     The ResourceTranslator interface.
    /// </summary>
    public interface IResourceTranslator
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get string.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetString(ILocalizationResource key);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetString(ILocalizationResource key, object[] arguments);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="targetCulture">
        /// The target culture.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetString(ILocalizationResource key, CultureInfo targetCulture, object[] arguments);

        /// <summary>
        /// The get supported languages.
        /// </summary>
        /// <param name="languageSupport">
        /// The language support.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="resourceType">
        /// The resource type.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<LanguageValue> GetSupportedLanguages(ILanguageSupport languageSupport, string key, Type resourceType, object[] arguments);

        /// <summary>
        /// The get supported languages.
        /// </summary>
        /// <param name="languageSupport">
        /// The language support.
        /// </param>
        /// <param name="multilanguageParamValues">
        /// The multilanguage param values.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="resourceType">
        /// The resource type.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<LanguageValue> GetSupportedLanguages(
            ILanguageSupport languageSupport, 
            List<LanguageValue> multilanguageParamValues, 
            string key, 
            Type resourceType, 
            object[] arguments);

        #endregion
    }
}
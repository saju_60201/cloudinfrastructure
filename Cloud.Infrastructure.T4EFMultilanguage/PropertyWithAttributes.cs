﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PropertyWithAttributes.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.T4EFMultilanguage
{
    /// <summary>
    ///     The property with attributes.
    /// </summary>
    public class PropertyWithAttributes
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyWithAttributes"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="attributeAsString">
        /// The attribute as string.
        /// </param>
        public PropertyWithAttributes(string propertyName, string type, string attributeAsString)
        {
            this.PropertyName = propertyName;
            this.Type = type;
            this.AttributeAsString = attributeAsString;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PropertyWithAttributes" /> class.
        /// </summary>
        public PropertyWithAttributes()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the attribute as string.
        /// </summary>
        public string AttributeAsString { get; set; }

        /// <summary>
        ///     Gets or sets the property name.
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        #endregion
    }
}
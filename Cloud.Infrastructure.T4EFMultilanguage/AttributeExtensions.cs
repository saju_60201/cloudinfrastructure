﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeExtensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.T4EFMultilanguage
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    ///     The attribute extensions.
    /// </summary>
    public static class AttributeExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// The create attribute.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="Attribute"/>.
        /// </returns>
        public static Attribute CreateAttribute(this CustomAttributeData data)
        {
            var arguments = data.ConstructorArguments.GetConstructorValues().ToArray();
            var attribute = data.Constructor.Invoke(arguments) as Attribute;

            if (data.NamedArguments == null)
            {
                return attribute;
            }

            foreach (var namedArgument in data.NamedArguments)
            {
                var propertyInfo = namedArgument.MemberInfo as PropertyInfo;
                var value = namedArgument.TypedValue.GetArgumentValue();

                if (propertyInfo != null)
                {
                    propertyInfo.SetValue(attribute, value, null);
                }
                else
                {
                    var fieldInfo = namedArgument.MemberInfo as FieldInfo;
                    if (fieldInfo != null)
                    {
                        fieldInfo.SetValue(attribute, value);
                    }
                }
            }

            return attribute;
        }

        /// <summary>
        /// The create attributes.
        /// </summary>
        /// <param name="attributesData">
        /// The attributes data.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<Attribute> CreateAttributes(this IEnumerable<CustomAttributeData> attributesData)
        {
            return from attributeData in attributesData select attributeData.CreateAttribute();
        }

        /// <summary>
        /// The get custom attributes copy.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<Attribute> GetCustomAttributesCopy(this Type type)
        {
            return CustomAttributeData.GetCustomAttributes(type).CreateAttributes();
        }

        /// <summary>
        /// The get custom attributes copy.
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<Attribute> GetCustomAttributesCopy(this Assembly assembly)
        {
            return CustomAttributeData.GetCustomAttributes(assembly).CreateAttributes();
        }

        /// <summary>
        /// The get custom attributes copy.
        /// </summary>
        /// <param name="memberInfo">
        /// The member info.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<Attribute> GetCustomAttributesCopy(this MemberInfo memberInfo)
        {
            return CustomAttributeData.GetCustomAttributes(memberInfo).CreateAttributes();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The convert custom attribute typed argument array.
        /// </summary>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <param name="elementType">
        /// The element type.
        /// </param>
        /// <returns>
        /// The <see cref="Array"/>.
        /// </returns>
        private static Array ConvertCustomAttributeTypedArgumentArray(
            this IEnumerable<CustomAttributeTypedArgument> arguments, 
            Type elementType)
        {
            var valueArray = arguments.Select(x => x.Value).ToArray();
            var newArray = Array.CreateInstance(elementType, valueArray.Length);
            Array.Copy(valueArray, newArray, newArray.Length);
            return newArray;
        }

        /// <summary>
        /// The get argument value.
        /// </summary>
        /// <param name="argument">
        /// The argument.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private static object GetArgumentValue(this CustomAttributeTypedArgument argument)
        {
            var value = argument.Value;
            var collectionValue = value as ReadOnlyCollection<CustomAttributeTypedArgument>;
            return collectionValue != null
                       ? ConvertCustomAttributeTypedArgumentArray(collectionValue, argument.ArgumentType.GetElementType())
                       : value;
        }

        /// <summary>
        /// The get constructor values.
        /// </summary>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private static IEnumerable<object> GetConstructorValues(this IEnumerable<CustomAttributeTypedArgument> arguments)
        {
            return from argument in arguments select argument.GetArgumentValue();
        }

        #endregion
    }
}
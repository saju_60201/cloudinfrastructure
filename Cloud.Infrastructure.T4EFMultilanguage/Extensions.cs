﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.T4EFMultilanguage
{
    using System;
    using System.Reflection;

    using Cloud.Infrastructure.Localization.Contracts;

    /// <summary>
    ///     The extention for creation sql statments.
    /// </summary>
    public static class Extensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Creates full text index sql statement.
        /// </summary>
        /// <param name="assembly">
        /// The assembly in which domain entities should be inspected.
        /// </param>
        /// <param name="fullTextIndexCatalogName">
        /// The full text index catalog name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [Obsolete(
            "CreateFullTextIndexSqlStatement is deprecated. Please Use PubliWeb.Infrastructure.T4EFMultilanguage.CreateFullTextIndexSqlStatement(Type typeFromDbContext, string FullTextIndexCatalogName)"
            )]
        public static string CreateFullTextIndexSqlStatement(this Assembly assembly, string fullTextIndexCatalogName)
        {
            var fullTextIndexCreator = new FullTextIndexCreator();
            return fullTextIndexCreator.GenerateSqlStatement(assembly, fullTextIndexCatalogName);
        }

        /// <summary>
        /// Gets columns list.
        /// </summary>
        /// <param name="type">
        /// The entity type.
        /// </param>
        /// <param name="languageSupport">
        /// The culture provider.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetColumnsList(this Type type, ILanguageSupport languageSupport)
        {
            var fullTextCreator = new FullTextIndexCreator();
            return fullTextCreator.GetColumns(type, languageSupport);
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FullTextIndexCreator.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.T4EFMultilanguage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Cloud.Infrastructure.Localization.Contracts;
    using Cloud.Infrastructure.ODataAttributes;

    /// <summary>
    ///     The full text index creator.
    /// </summary>
    public class FullTextIndexCreator
    {
        #region Public Properties

        /// <summary>
        ///     Gets the column separator.
        /// </summary>
        public string ColumnSeparator
        {
            get
            {
                return ", " + Environment.NewLine;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Provides a sql statement for catalog creation.
        /// </summary>
        /// <param name="catalogName">
        /// The catalog name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string CreateCatalog(string catalogName)
        {
            const string TemplateText = @"CREATE FULLTEXT CATALOG [{0}] WITH ACCENT_SENSITIVITY = ON AUTHORIZATION [dbo]";
            return !string.IsNullOrEmpty(catalogName) ? string.Format(TemplateText, catalogName) : string.Empty;
        }

        /// <summary>
        /// The create full text index sql statement.
        /// </summary>
        /// <param name="contextType">
        /// The context type.
        /// </param>
        /// <param name="fullTextIndexCatalogName">
        /// The full text index catalog name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string CreateFullTextIndexSqlStatement(Type contextType, string fullTextIndexCatalogName)
        {
            return GenerateSqlStatement(contextType, fullTextIndexCatalogName);
        }

        /// <summary>
        /// Provides a sql statement for index creation.
        /// </summary>
        /// <param name="catalogName">
        /// The catalog name.
        /// </param>
        /// <param name="tableName">
        /// The table name.
        /// </param>
        /// <param name="keyIndexName">
        /// The key index name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>sql statment.
        /// </returns>
        public string CreateFulltextIndex(string catalogName, Type tableName, string keyIndexName)
        {
            const string TemplateText =
                @"CREATE FULLTEXT INDEX ON [dbo].[{4}]{0}({2}){0}KEY INDEX [{3}] ON ([{1}], FILEGROUP [PRIMARY]){0}WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)";

            string foundColumns = this.GetColumns(tableName);

            if (!string.IsNullOrEmpty(catalogName) && !string.IsNullOrEmpty(tableName.Name) && !string.IsNullOrEmpty(foundColumns))
            {
                return string.Format(TemplateText, Environment.NewLine, catalogName, foundColumns, keyIndexName, tableName.Name);
            }

            return null;
        }

        /// <summary>
        /// Provides a sql statement for droping existing catalog.
        /// </summary>
        /// <param name="catalogName">
        /// The catalog name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string DropCatalog(string catalogName)
        {
            const string TemplateText =
                @"IF  EXISTS (SELECT * FROM sysfulltextcatalogs ftc WHERE ftc.name = N'{0}') DROP FULLTEXT CATALOG [{1}]";
            return !string.IsNullOrEmpty(catalogName) ? string.Format(TemplateText, catalogName, catalogName) : string.Empty;
        }

        /// <summary>
        /// The drop fulltext index.
        /// </summary>
        /// <param name="tableName">
        /// The table name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string DropFulltextIndex(Type tableName)
        {
            const string TemplateText =
                @"IF  EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].[{0}]')) DROP FULLTEXT INDEX ON [dbo].[{1}]";
            return !string.IsNullOrEmpty(tableName.Name) ? string.Format(TemplateText, tableName.Name, tableName.Name) : string.Empty;
        }

        /// <summary>
        /// The find types which contain properties marked with FullTextIndexAttribute.
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        [Obsolete("FindTypes(Assembly) is deprecated. Please Use FindTypes(Type typeFromContext)")]
        public List<Type> FindTypes(Assembly assembly)
        {
            var classes = assembly.GetTypes().Where(t => t.IsClass && !t.IsAbstract && !t.IsGenericType && t.IsPublic);

            IEnumerable<Type> types = classes as IList<Type> ?? classes.ToList();
            var list = (from type in types
                        let properties = type.GetProperties()
                        where properties.Any(propertyInfo => propertyInfo.GetCustomAttributes(typeof(FullTextIndexAttribute), false).Any())
                        select type).ToList();

            return list.Count == 0 ? null : list;
        }

        /// <summary>
        /// The find types.
        /// </summary>
        /// <param name="contextType">
        /// The context type.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.List of Types
        /// </returns>
        public List<Type> FindTypes(Type contextType)
        {
            var classes =
                contextType.GetProperties()
                    .Where(p => p.PropertyType.Name == "DbSet`1" && p.PropertyType.IsGenericType)
                    .Select(p => p.PropertyType.GetGenericArguments().FirstOrDefault())
                    .Where(c => c.GetProperties().Any(a => a.GetCustomAttributes(typeof(FullTextIndexAttribute), false).Any()))
                    .ToList();
            return classes.Count == 0 ? null : classes;
        }

        /// <summary>
        /// Generate sql statement with CREATE FULLTEXT CATALOG \CREATE FULLTEXT INDEX .
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <param name="fullTextIndexCatalogName">
        /// The full text index catalog name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [Obsolete(
            "GenerateSqlStatement(Assembly,FullTextIndexCatalogName) is deprecated. Please Use GenerateSqlStatement(Type, FullTextIndexCatalogName)"
            )]
        public string GenerateSqlStatement(Assembly assembly, string fullTextIndexCatalogName)
        {
            string catalogCreationSqlStatement = this.DropCatalog(fullTextIndexCatalogName);
            catalogCreationSqlStatement += Environment.NewLine + this.CreateCatalog(fullTextIndexCatalogName);
            string indexesCreationSqlStatement = string.Empty;
            string dropSqlStatements = string.Empty;
            var classes = this.FindTypes(assembly);

            if (classes != null)
            {
                foreach (var @class in classes)
                {
                    dropSqlStatements += this.DropFulltextIndex(@class) + Environment.NewLine;

                    string result = this.CreateFulltextIndex(fullTextIndexCatalogName, @class, "PK_dbo." + @class.Name);

                    if (!string.IsNullOrEmpty(result) && string.IsNullOrEmpty(indexesCreationSqlStatement))
                    {
                        indexesCreationSqlStatement = result;
                    }
                    else if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(indexesCreationSqlStatement))
                    {
                        indexesCreationSqlStatement = indexesCreationSqlStatement + Environment.NewLine + Environment.NewLine + result;
                    }
                }

                return dropSqlStatements + Environment.NewLine + catalogCreationSqlStatement + Environment.NewLine + Environment.NewLine
                       + indexesCreationSqlStatement;
            }

            return catalogCreationSqlStatement;
        }

        /// <summary>
        /// The generate sql statement.
        /// </summary>
        /// <param name="contextType">
        /// The context type.
        /// </param>
        /// <param name="fullTextIndexCatalogName">
        /// The full text index catalog name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateSqlStatement(Type contextType, string fullTextIndexCatalogName)
        {
            string catalogCreationSqlStatement = this.DropCatalog(fullTextIndexCatalogName);
            catalogCreationSqlStatement += Environment.NewLine + this.CreateCatalog(fullTextIndexCatalogName);
            string indexesCreationSqlStatement = string.Empty;
            string dropSqlStatements = string.Empty;
            var classes = this.FindTypes(contextType);

            if (classes != null)
            {
                foreach (var @class in classes)
                {
                    dropSqlStatements += this.DropFulltextIndex(@class) + Environment.NewLine;

                    string result = this.CreateFulltextIndex(fullTextIndexCatalogName, @class, "PK_dbo." + @class.Name);

                    if (!string.IsNullOrEmpty(result) && string.IsNullOrEmpty(indexesCreationSqlStatement))
                    {
                        indexesCreationSqlStatement = result;
                    }
                    else if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(indexesCreationSqlStatement))
                    {
                        indexesCreationSqlStatement = indexesCreationSqlStatement + Environment.NewLine + Environment.NewLine + result;
                    }
                }

                return dropSqlStatements + Environment.NewLine + catalogCreationSqlStatement + Environment.NewLine + Environment.NewLine
                       + indexesCreationSqlStatement;
            }

            return catalogCreationSqlStatement;
        }

        /// <summary>
        /// Gets columns\ properties that decorated with FullTextIndexAttribute
        /// </summary>
        /// <param name="type">
        /// The type or class name (table name).
        /// </param>
        /// <param name="languageSupport">
        /// The culture Provider.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetColumns(Type type, ILanguageSupport languageSupport = null)
        {
            var fullTextIndexMarkedProperties =
                type.GetProperties()
                    .Select(propertyInfo => propertyInfo)
                    .Where(column => column.GetCustomAttributes(typeof(FullTextIndexAttribute), false).Any());
            // || column.GetCustomAttributes(typeof(FullTextIndexAttribute), false).Any());
            var list = new List<string>();

            foreach (var fullTextIndexMarkedProperty in fullTextIndexMarkedProperties)
            {
                string propertyName = fullTextIndexMarkedProperty.Name;
                var attribute =
                    fullTextIndexMarkedProperty.GetCustomAttributes(typeof(FullTextIndexAttribute), false).FirstOrDefault() as
                    FullTextIndexAttribute;
                if (attribute != null)
                {
                    string language = attribute.Language.ToString();

                    if (languageSupport == null)
                    {
                        string entry = string.Format("[{0}] LANGUAGE [{1}]", propertyName, language);
                        list.Add(entry);
                    }
                    else
                    {
                        var cultureInfo = languageSupport.GetCurrentLanguage().CultureInfo;
                        if (cultureInfo.EnglishName.ToLower().Contains(language.ToLower()))
                        {
                            list.Add(propertyName);
                        }
                    }
                }
            }

            string result = string.Join(this.ColumnSeparator, list);
            return result;
            //return list.Count == 0 ? null : list;
        }

        #endregion
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeHandler.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.T4EFMultilanguage
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    using Cloud.Infrastructure.ODataAttributes;

    /// <summary>
    ///     The class handler.
    /// </summary>
    public class AttributeHandler
    {
        #region Constants

        /// <summary>
        /// The culture separator.
        /// </summary>
        private const string CultureSeparator = @"_";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeHandler" /> class.
        /// </summary>
        public AttributeHandler()
        {
            this.MultilanguageAttributeParser = new MultilanguageAttributeParser();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the multilanguage attribute parser.
        /// </summary>
        public MultilanguageAttributeParser MultilanguageAttributeParser { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Decorates with full text index attribute.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="givenAttribute">
        /// The given attribute.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string DecorateWithFullTextIndexAttribute(string propertyName, string givenAttribute)
        {
            var last3Chars = propertyName.Substring(Math.Max(0, propertyName.Length - 3));

            if (last3Chars.StartsWith(CultureSeparator))
            {
                var extractedCulture = new CultureInfo(last3Chars.Replace(CultureSeparator, string.Empty), false);
                return this.MultilanguageAttributeParser.DecorateWithFullTextIndexAttribute(givenAttribute, extractedCulture);
            }

            return givenAttribute;
        }

        /// <summary>
        /// Extracts multilanguage attributes.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<PropertyWithAttributes> ExtractMultilanguageAttributes(Type type)
        {
            var allLanguageMarkedProperties =
                type.GetProperties()
                    .Select(propertyInfo => propertyInfo)
                    .Where(column => column.GetCustomAttributes(typeof(MultilanguageAttribute), false).Any());
            // || column.GetCustomAttributes(typeof(FullTextIndexAttribute), false).Any());
            var list = new List<PropertyWithAttributes>();

            foreach (var languageMarkedProperty in allLanguageMarkedProperties)
            {
                string propertyName;
                var attributeAsString = this.MultilanguageAttributeParser.GetMultilanguageAttributeAsString(
                    languageMarkedProperty, 
                    out propertyName);
                list.Add(new PropertyWithAttributes(propertyName, "string", attributeAsString));
            }

            return list.Count == 0 ? null : list;
        }

        /// <summary>
        /// Extract multilanguage attributes from
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Type> FindTypes(Assembly assembly)
        {
            var classes = assembly.GetTypes().Where(t => t.IsClass && !t.IsAbstract && !t.IsGenericType && t.IsPublic);

            IEnumerable<Type> types = classes as IList<Type> ?? classes.ToList();
            var list = (from type in types
                        let properties = type.GetProperties()
                        where properties.Any(propertyInfo => propertyInfo.GetCustomAttributes(typeof(MultilanguageAttribute), false).Any())
                        select type).ToList();

            return list.Count == 0 ? null : list;
        }

        /// <summary>Gets using namespaces. </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string GetUsingNamespaces()
        {
            return
                string.Format(
                    "{0}" + this.MultilanguageAttributeParser.Separator + "{1}" + this.MultilanguageAttributeParser.Separator + "{2}"
                    + this.MultilanguageAttributeParser.Separator + "{3}", 
                    "using System;", 
                    "using System.ComponentModel.DataAnnotations;", 
                    "using System.ComponentModel.DataAnnotations.Schema;", 
                    "using PubliWeb.Infrastructure.ODataAttributes;");
        }

        #endregion
    }
}
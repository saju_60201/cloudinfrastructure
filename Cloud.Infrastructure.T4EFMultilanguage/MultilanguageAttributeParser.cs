﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MultilanguageAttributeParser.cs" company="">
//   
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Cloud.Infrastructure.T4EFMultilanguage
{
    using System;
    using System.Globalization;
    using System.Reflection;

    using Cloud.Infrastructure.ODataAttributes;

    /// <summary>
    ///     The multilanguage attribute parser.
    /// </summary>
    public class MultilanguageAttributeParser
    {
        #region Constants

        /// <summary>
        /// The fulltext index language.
        /// </summary>
        private const string FulltextIndexLanguage = "[FullTextIndex(FullTextIndexLanguage.{0})]";

        /// <summary>
        /// The tab.
        /// </summary>
        private const string Tab = "\t";

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the separator.
        /// </summary>
        public string Separator
        {
            get
            {
                return Environment.NewLine + Tab + Tab;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The decorate with full text index attribute.
        /// </summary>
        /// <param name="givenStringAttribute">
        /// The given string attribute.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture info.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string DecorateWithFullTextIndexAttribute(string givenStringAttribute, CultureInfo cultureInfo)
        {
            string fullTextIndexAsstring = string.Format(
                FulltextIndexLanguage, 
                Enum.Parse(typeof(FullTextIndexLanguage), cultureInfo.EnglishName).ToString());

            return !string.IsNullOrEmpty(givenStringAttribute)
                       ? string.Join(this.Separator, givenStringAttribute, fullTextIndexAsstring)
                       : fullTextIndexAsstring;
        }

        /// <summary>
        /// Extracts max min length.
        /// </summary>
        /// <param name="multilanguageAttribute">
        /// The multilanguage attribute.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ExtractMaxMinLength(MultilanguageAttribute multilanguageAttribute)
        {
            // build string for MaxLength attribute like [MaxLength(10, ErrorMessage="BloggerName must be 10 characters or less"),MinLength(5)]
            var attributeAsString = string.Empty;
            if (multilanguageAttribute.MaxLength != 0 && string.IsNullOrEmpty(multilanguageAttribute.MaxLengthErrorMessage)
                && multilanguageAttribute.MinLength == 0)
            {
                //1000
                attributeAsString = string.Format("[MaxLength({0})]", multilanguageAttribute.MaxLength);
            }
            else if (multilanguageAttribute.MaxLength != 0 && !string.IsNullOrEmpty(multilanguageAttribute.MaxLengthErrorMessage)
                     && multilanguageAttribute.MinLength == 0)
            {
                // 1100
                attributeAsString = string.Format(
                    "[MaxLength({0}, ErrorMessage=\"{1}\")]", 
                    multilanguageAttribute.MaxLength, 
                    multilanguageAttribute.MaxLengthErrorMessage);
            }
            else if (multilanguageAttribute.MaxLength != 0 && string.IsNullOrEmpty(multilanguageAttribute.MaxLengthErrorMessage)
                     && multilanguageAttribute.MinLength != 0 && string.IsNullOrEmpty(multilanguageAttribute.MinLengthErrorMessage))
            {
                //1010
                attributeAsString = string.Format(
                    "[MaxLength({0}), MinLength({1})]", 
                    multilanguageAttribute.MaxLength, 
                    multilanguageAttribute.MinLength);
            }
            else if (multilanguageAttribute.MaxLength != 0 && string.IsNullOrEmpty(multilanguageAttribute.MaxLengthErrorMessage)
                     && multilanguageAttribute.MinLength != 0 && !string.IsNullOrEmpty(multilanguageAttribute.MinLengthErrorMessage))
            {
                //1011
                attributeAsString = string.Format(
                    "[MaxLength({0}), MinLength({1}, ErrorMessage=\"{2}\")]", 
                    multilanguageAttribute.MaxLength, 
                    multilanguageAttribute.MinLength, 
                    multilanguageAttribute.MinLengthErrorMessage);
            }
            else if (multilanguageAttribute.MaxLength != 0 && !string.IsNullOrEmpty(multilanguageAttribute.MaxLengthErrorMessage)
                     && multilanguageAttribute.MinLength != 0
                     && string.IsNullOrEmpty(multilanguageAttribute.MinLengthErrorMessage))
            {
                //1110
                attributeAsString = string.Format(
                    "[MaxLength({0}, ErrorMessage=\"{1}\"), MinLength({2})]", 
                    multilanguageAttribute.MaxLength, 
                    multilanguageAttribute.MaxLengthErrorMessage, 
                    multilanguageAttribute.MinLength);
            }
            else if (multilanguageAttribute.MaxLength != 0
                     && !string.IsNullOrEmpty(multilanguageAttribute.MaxLengthErrorMessage)
                     && multilanguageAttribute.MinLength != 0
                     && !string.IsNullOrEmpty(multilanguageAttribute.MinLengthErrorMessage))
            {
                //1111
                attributeAsString =
                    string.Format(
                        "[MaxLength({0}, ErrorMessage=\"{1}\"), MinLength({2}, ErrorMessage=\"{3}\")]", 
                        multilanguageAttribute.MaxLength, 
                        multilanguageAttribute.MaxLengthErrorMessage, 
                        multilanguageAttribute.MinLength, 
                        multilanguageAttribute.MinLengthErrorMessage);
            }

            if (multilanguageAttribute.MaxLength == 0 && multilanguageAttribute.MinLength != 0
                && string.IsNullOrEmpty(multilanguageAttribute.MinLengthErrorMessage))
            {
                //0010
                attributeAsString = string.Format("[MinLength({0})]", multilanguageAttribute.MinLength);
            }
            else if (multilanguageAttribute.MaxLength == 0 && multilanguageAttribute.MinLength != 0
                     && !string.IsNullOrEmpty(multilanguageAttribute.MinLengthErrorMessage))
            {
                //0011
                attributeAsString = string.Format(
                    "[MinLength({0}, ErrorMessage=\"{1}\")]", 
                    multilanguageAttribute.MinLength, 
                    multilanguageAttribute.MinLengthErrorMessage);
            }

            return attributeAsString;
        }

        /// <summary>
        /// Extraxts required.
        /// </summary>
        /// <param name="multilanguageAttribute">
        /// The multilanguage attribute.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ExtraxtRequired(MultilanguageAttribute multilanguageAttribute)
        {
            return multilanguageAttribute.Required ? string.Format("[Required]") : string.Empty;
        }

        /// <summary>
        /// Extraxts string length.
        /// </summary>
        /// <param name="multilanguageAttribute">
        /// The multilanguage attribute.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ExtraxtStringLength(MultilanguageAttribute multilanguageAttribute)
        {
            return multilanguageAttribute.StringLength != 0
                       ? string.Format("[StringLength({0})]", multilanguageAttribute.StringLength)
                       : string.Empty;
        }

        /// <summary>
        /// Gets multilanguage attribute as string.
        /// </summary>
        /// <param name="propertyInfo">
        /// property info
        /// </param>
        /// <param name="propertyName">
        /// property name
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetMultilanguageAttributeAsString(PropertyInfo propertyInfo, out string propertyName)
        {
            var multilanguageAttribute = propertyInfo.GetCustomAttribute<MultilanguageAttribute>();

            string attributeAsString = string.Empty;
            propertyName = string.Empty;

            if (multilanguageAttribute != null)
            {
                attributeAsString = this.ExtractMultilanguageAttribute(multilanguageAttribute);
                propertyName = multilanguageAttribute.PropertyName;
            }

            return attributeAsString;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Decorates with language attribute.
        /// </summary>
        /// <param name="givenStringAttribute">
        /// The given string attribute.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string DecorateWithLanguageAttribute(string givenStringAttribute)
        {
            const string LanguageAttribute = "[Language]";
            return !string.IsNullOrEmpty(givenStringAttribute)
                       ? string.Join(this.Separator, givenStringAttribute, LanguageAttribute)
                       : LanguageAttribute;
        }

        /// <summary>
        /// The extract multilanguage attribute.
        /// </summary>
        /// <param name="multilanguageAttribute">
        /// The multilanguage attribute.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ExtractMultilanguageAttribute(MultilanguageAttribute multilanguageAttribute)
        {
            var attributeAsString = this.ExtractMaxMinLength(multilanguageAttribute);

            if (!string.IsNullOrEmpty(this.ExtraxtRequired(multilanguageAttribute)) && !string.IsNullOrEmpty(attributeAsString))
            {
                attributeAsString = string.Join(this.Separator, attributeAsString, this.ExtraxtRequired(multilanguageAttribute));
            }
            else if (!string.IsNullOrEmpty(this.ExtraxtRequired(multilanguageAttribute)) && string.IsNullOrEmpty(attributeAsString))
            {
                attributeAsString = this.ExtraxtRequired(multilanguageAttribute);
            }

            if (!string.IsNullOrEmpty(this.ExtraxtStringLength(multilanguageAttribute)) && !string.IsNullOrEmpty(attributeAsString))
            {
                attributeAsString = string.Join(this.Separator, attributeAsString, this.ExtraxtStringLength(multilanguageAttribute));
            }
            else if (!string.IsNullOrEmpty(this.ExtraxtStringLength(multilanguageAttribute)) && string.IsNullOrEmpty(attributeAsString))
            {
                attributeAsString = this.ExtraxtStringLength(multilanguageAttribute);
            }

            attributeAsString = this.DecorateWithLanguageAttribute(attributeAsString);
            return attributeAsString;
        }

        #endregion
    }
}